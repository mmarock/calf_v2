

#include "../header/Requests/Echo.hh"
#include "../header/Requests/Time.hh"
#include "../header/Requests/Quit.hh"
#include "../header/Requests/ServerQuit.hh"
#include "../header/Requests/CircuitToJson.hh"
#include "../header/Requests/SchedulerToJson.hh"
#include "../header/Requests/Reload.hh"


#include <iostream>
#include <string>
#include <unistd.h>

#include <cerrno>
#include <cstdlib>
#include <cstring>

#include <sys/un.h>
#include <sys/socket.h>

#include <getopt.h>
#include <json/json.h>


using namespace CALF;

int main(int argc, char **argv)
{
	if(argc <= 1) {
		std::cout << "no args given" << std::endl;
		return -1;
	}

	int sock = socket(AF_INET6, SOCK_STREAM ,0);
	if (sock < 0) {
		perror("socket(): ");
		return -1;
	}

	struct sockaddr_un addr = {AF_UNIX, "/tmp/calf.socket"};

	if (connect(sock, (struct sockaddr *)&addr, sizeof(sockaddr_un)) < 0) {
		perror("connect(): ");
		return -1;
	}

	Json::Value root(Json::ValueType::objectValue);

	root["version"] = 1;
	root["requests"] = Json::Value(Json::ValueType::arrayValue);

	struct option opts[] = { { "echo", required_argument, NULL, 1 },
				 { "time", optional_argument, NULL, 2 },
				 { "quit", no_argument, NULL, 3 },
				 { "server_quit", no_argument, NULL, 4 },
				 { "serialise_circuit", no_argument, NULL, 5},
				 { "serialise_scheduler", no_argument, NULL, 6},
				 { "reload", no_argument, NULL, 7},
				 { nullptr, 0, 0, 0 } };

	int c = -1;
	
	std::vector<RequestPtr> requests;

	while ((c = getopt_long(argc, argv, "", opts, NULL) ) != -1) {
		switch (c) {
			case 1:
				requests.emplace_back(new Echo(optarg));
				break;
			case 2:
				requests.emplace_back(new Time());
				break;
			case 3:
				requests.emplace_back(new Quit());
				break;
			case 4:
				requests.emplace_back(new ServerQuit());
				break;
			case 5:
				requests.emplace_back(new CircuitToJson());
				break;
			case 6:
				requests.emplace_back(new SchedulerToJson());
				break;
			case 7:
				requests.emplace_back(new Reload());
				break;
			default:
				return EXIT_FAILURE;
		};
	}
	
	for (auto &r : requests) {
		root["requests"].append(r->toJson());	
	}

	Json::FastWriter writer;

	std::string payload = writer.write(root);
	payload += '\x04';

	std::cout << "write to server: " << payload << std::endl;

	if(write(sock, payload.c_str(), payload.size()) < 0) {
		perror("write(): ");
		return EXIT_FAILURE;
	}

	uint8_t buffer[15000];

	if(read(sock, buffer, 15000) < 0) {
		perror("read: ");
		return -1;
	}

	Json::Reader reader;

	reader.parse((char *)buffer, root);

	Json::StyledStreamWriter stream_writer;

	stream_writer.write(std::cout, root);

	close(sock);

	return EXIT_SUCCESS;
}




