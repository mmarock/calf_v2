
#include <iostream>

#include "../header/Client.hh"
#include "../header/logger.hh"

#include "../header/Requests/Echo.hh"
#include "../header/Requests/Reload.hh"
#include "../header/Requests/SchedulerRequest.hh"
#include "../header/Requests/ServerQuit.hh"

#include <algorithm>
#include <memory>

#include <cassert>
#include <cstring>

#include <unistd.h>

#include <getopt.h>
#include <json/json.h>

using namespace CALF;

////////////////////////////////////////////////////////////
//		static functions
////////////////////////////////////////////////////////////

static void unknown_opt()
{
	std::cout << __PRETTY_FUNCTION__ << ": unknown opt" << std::endl;
}

static void print_help(struct option *long_opts)
{
	assert(long_opts);
	std::cout << "calf-admin (--port=<port> || --host=<host> || --help) "
		     "[COMMAND]\n";
	while (long_opts->name) {
		std::cout << "\t--" << long_opts->name
			  << ": required_argument: " << std::boolalpha
			  << ((long_opts->has_arg == required_argument) ? true
									: false)
			  << "\n";
		long_opts++;
	}
	std::cout << std::endl;
}

////////////////////////////////////////////////////////////
//		possible Actions
////////////////////////////////////////////////////////////

class EchoAction : public Action
{
	std::string msg_;

public:
	explicit EchoAction(const char *optarg) : msg_(optarg) {}

	~EchoAction() {}

	std::vector<RequestPtr> query() const override
	{
		std::vector<RequestPtr> ptr;
		ptr.emplace_back(new Echo(msg_));

		return ptr;
	}

	bool evaluate(const std::vector<ResponsePtr> &resp) const override
	{
		auto it = std::find_if(
		    resp.begin(), resp.end(), [&](const ResponsePtr &ptr) {
			    return ptr->name() == Response::NAME::ECHO;
		    });

		if (it == resp.end() ||
		    it->get()->type() != Response::TYPE::TEXT) {
			std::cout << "calf-admin: echo: no response"
				  << std::endl;
			return false;
		}

		const auto &response = *it->get();

		std::cout << "calf-admin: echo: "
			  << dynamic_cast<const TextResponse &>(response).text()
			  << std::endl;
		return true;
	}
};

struct ReloadAction : public Action {
	explicit ReloadAction() {}
	~ReloadAction() {}

	std::vector<RequestPtr> query() const override
	{
		std::vector<RequestPtr> ptr;
		ptr.emplace_back(new Reload());
		return ptr;
	}

	bool evaluate(const std::vector<ResponsePtr> &resp) const override
	{
		auto it = std::find_if(
		    resp.begin(), resp.end(), [&](const ResponsePtr &ptr) {
			    return ptr->name() == Response::NAME::RELOAD;
		    });
		if (it == resp.end()) {
			std::cout << "calf-admin: relaod: no response"
				  << std::endl;
			return false;
		}
		const auto &response = *it->get();

		if (response.type() == Response::TYPE::EMPTY) {
			// empty - ok -
			std::cout << "calf-admin: reload: ok" << std::endl;
			return true;
		} else {
			// exception
			std::cout
			    << "calf-admin: reload: "
			    << dynamic_cast<const ExceptionResponse &>(response)
				   .text()
			    << std::endl;
			return false;
		}
	}
};

class SchedulerAction : public Action
{

	SchedulerRequest::COMMAND cmd_;

public:
	explicit SchedulerAction(const char *opt)
	    : cmd_(SchedulerRequest::isCmd(opt))
	{
	}
	~SchedulerAction() {}

	std::vector<RequestPtr> query() const override
	{
		std::vector<RequestPtr> ptr;
		ptr.emplace_back(new SchedulerRequest(cmd_));
		return ptr;
	}

	bool evaluate(const std::vector<ResponsePtr> &resp) const override
	{
		auto it = std::find_if(
		    resp.begin(), resp.end(), [&](const ResponsePtr &ptr) {
			    return ptr->name() ==
				   Response::NAME::SCHEDULER_REQUEST;
		    });
		if (it == resp.end()) {
			std::cout << "calf-admin: scheduler: no response"
				  << std::endl;
			return false;
		}
		const auto &response = *it->get();

		std::cout << "calf-admin: scheduler: state: "
			  << dynamic_cast<const TextResponse &>(response)
				 .text()
			  << std::endl;
		return true;
	}
};

struct ServerQuitAction : public Action {
	explicit ServerQuitAction() {}

	~ServerQuitAction() {}

	std::vector<RequestPtr> query() const override
	{
		std::vector<RequestPtr> ptr;
		ptr.emplace_back(new ServerQuit());

		return ptr;
	}

	bool evaluate(const std::vector<ResponsePtr> &resp) const override
	{
		auto it = std::find_if(
		    resp.begin(), resp.end(), [&](const ResponsePtr &ptr) {
			    return ptr->name() == Response::NAME::SERVER_QUIT;
		    });

		if (it == resp.end() ||
		    it->get()->type() != Response::TYPE::EMPTY) {
			return false;
		}

		std::cout << "calf-admin: server-quit: ok" << std::endl;

		// exit immediatly, do not cleanup
		// This is exceptional behaviour. The Client never should be
		// able
		// to close the server on its own.
		exit(EXIT_SUCCESS);

		// return (it != resp.end() &&
		//	it->get()->type() == Response::TYPE::EMPTY);
	}
};

////////////////////////////////////////////////////////////
//		main function
////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{

	logger::setLogLevel(LOG_LEVEL::DEBUG);

	struct option long_opts[]{
		{ "port", required_argument, nullptr, 1 },
		{ "help", no_argument, nullptr, 2 },
		{ "host", required_argument, nullptr, 3 },
		{ "echo", required_argument, nullptr, 4 },
		{ "reload", no_argument, nullptr, 5 },
		{ "scheduler", required_argument, nullptr, 6 },
		{ "server-quit", no_argument, nullptr, 7 },
		{ nullptr, no_argument, nullptr, 0 }
	};

	ActionPtr action(nullptr);

	const char *host = nullptr;
	const char *port = nullptr;
	int c = -1;
	while ((c = getopt_long(argc, argv, "", long_opts, nullptr)) != -1) {
		switch (c) {
		case 1:
			port = optarg;
			break;
		case 2:
			print_help(long_opts);
			return 0;
		case 3:
			host = optarg;
			break;
		case 4:
			action = ActionPtr(new EchoAction(optarg));
			break;
		case 5:
			action = ActionPtr(new ReloadAction());
			break;
		case 6:
			action = ActionPtr(new SchedulerAction(optarg));
			break;
		case 7:
			action = ActionPtr(new ServerQuitAction());
			break;
		default:
			unknown_opt();
			return -1;
		};
	}

	try {
		Client client(host, port);
		return (action) ? client.communicate(action) : -1;
	} catch (ClientError &ex) {
		std::cerr << "calf-admin: " << ex.what() << "\n";
	}
}
