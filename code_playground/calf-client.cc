

#include <iostream>

#include "../header/Client.hh"
#include "../header/Detector.hh"
#include "../header/FillGroup.hh"
#include "../header/FillTime.hh"
#include "../header/VentBox.hh"
#include "../header/logger.hh"
#include "../header/Request.hh"
#include "../header/Scheduler.hh"

#include "../header/Requests/CancelGroupFill.hh"
#include "../header/Requests/CircuitToJson.hh"
#include "../header/Requests/DetectorToJson.hh"
#include "../header/Requests/ForceFill.hh"
#include "../header/Requests/GetDetectorInletTemperature.hh"
#include "../header/Requests/GetDetectorOutletTemperature.hh"
#include "../header/Requests/SchedulerToJson.hh"
#include "../header/Requests/SetDetectorActivity.hh"

#include <algorithm>
#include <thread>
#include <vector>

#include <cassert>
#include <cstring>

#include <unistd.h>

#include <getopt.h>
#include <json/json.h>
#include <limits.h>

using namespace CALF;

////////////////////////////////////////////////////////////
//		static functions
////////////////////////////////////////////////////////////

static void print_help(struct option *long_opts)
{
	assert(long_opts);
	std::cout << "calf-admin (--port=<port> || --host=<host> || --help) "
		     "[COMMAND]\n";
	while (long_opts->name) {
		std::cout << "\t--" << long_opts->name
			  << ": required_argument: " << std::boolalpha
			  << ((long_opts->has_arg == required_argument) ? true
									: false)
			  << "\n";
		long_opts++;
	}
	std::cout << std::endl;
}

static void unknown_opt()
{
	std::cout << __PRETTY_FUNCTION__ << ": unknown opt" << std::endl;
}

static int str_to_int(const char *arg)
{
	assert(arg);
	char *endptr = nullptr;
	auto c = strtol(arg, &endptr, 10);

	if (c == LONG_MIN || c == LONG_MAX) {
		throw std::runtime_error(std::string("str_to_int: ") +
					 strerror(errno));
	} else if (*endptr != '\0') {
		throw std::runtime_error(
		    std::string("str_to_int: invalid string: ") + arg);
	}

	return c;
}

////////////////////////////////////////////////////////////
//		possible Actions
////////////////////////////////////////////////////////////

class AttachAction : public Action
{
	int id_;

public:
	explicit AttachAction(const char *optarg) : id_(str_to_int(optarg)) {}
	~AttachAction() {}

	std::vector<RequestPtr> query() const override
	{
		std::vector<RequestPtr> ptr;
		ptr.emplace_back(new SetDetectorActivity(id_, true));
		return ptr;
	}

	bool evaluate(const std::vector<ResponsePtr> &resp) const override
	{
		// returns either an empty response or an exception
		auto it = std::find_if(
		    resp.begin(), resp.end(), [&](const ResponsePtr &ptr) {
			    return ptr->name() ==
				   Response::NAME::SET_DETECTOR_ACTIVITY;
		    });

		if (it == resp.end()) {
			std::cout << "calf-client: attach: no response"
				  << std::endl;
			return false;
		}

		const auto &response = *it->get();

		if (response.type() == Response::TYPE::EXCEPTION) {
			std::cout << "calf-client: attach: "
				  << dynamic_cast<const ExceptionResponse &>(
					 response).text() << std::endl;
			return false;
		} else {
			std::cout << "calf-client: attach: id:" << id_
				  << ", done" << std::endl;
			return true;
		}
	}
};

class DetachAction : public Action
{
	int id_;

public:
	explicit DetachAction(const char *optarg) : id_(str_to_int(optarg)) {}
	~DetachAction() {}

	std::vector<RequestPtr> query() const override
	{
		std::vector<RequestPtr> ptr;
		ptr.emplace_back(new SetDetectorActivity(id_, false));
		return ptr;
	}

	bool evaluate(const std::vector<ResponsePtr> &resp) const override
	{
		// returns either an empty response or an exception
		auto it = std::find_if(
		    resp.begin(), resp.end(), [&](const ResponsePtr &ptr) {
			    return ptr->name() ==
				   Response::NAME::SET_DETECTOR_ACTIVITY;
		    });

		if (it == resp.end()) {
			std::cout << "calf-client: detach: no response"
				  << std::endl;
			return false;
		}

		const auto &response = *it->get();

		if (response.type() == Response::TYPE::EXCEPTION) {
			std::cout << "calf-client: detach: "
				  << dynamic_cast<const ExceptionResponse &>(
					 response).text() << std::endl;
			return false;
		} else {
			std::cout << "calf-client: detach: id:" << id_
				  << ", done" << std::endl;
			return true;
		}
	}
};

class ForceFillAction : public Action
{
	int id_;

public:
	explicit ForceFillAction(const char *optarg) : id_(str_to_int(optarg))
	{
	}
	~ForceFillAction() {}

	std::vector<RequestPtr> query() const override
	{
		std::vector<RequestPtr> ptr;
		ptr.emplace_back(new ForceFill(id_));
		return ptr;
	}

	bool evaluate(const std::vector<ResponsePtr> &resp) const override
	{
		auto it = std::find_if(
		    resp.begin(), resp.end(), [&](const ResponsePtr &ptr) {
			    return ptr->name() == Response::NAME::FORCE_FILL;
		    });
		assert((it != resp.end()) && "incomplete response");

		if ((*it)->type() == Response::TYPE::EMPTY) {
			std::cout << "force-fill: id: " << id_ << std::endl;
			return true;
		} else {

			std::cout << "force-fill: exception: "
				  << dynamic_cast<const ExceptionResponse &>(
					 **it).text() << std::endl;
			return false;
		}
	}
};

struct CancelFillAction : public Action {
	CancelFillAction() {}
	~CancelFillAction() {}

	std::vector<RequestPtr> query() const override {
		std::vector<RequestPtr> ptr;
		ptr.emplace_back(new CancelGroupFill());
		return ptr;
	}

	bool evaluate(const std::vector<ResponsePtr> &resp) const override
	{
		return (
		    find_if(resp.begin(), resp.end(),
			    [&](const ResponsePtr &resp) {
				    return resp->name() ==
					   Response::NAME::CANCEL_FILL_GROUP;
			    }) != resp.end());
	}
};

struct ShowGroupsAction : public Action {
	ShowGroupsAction() {}
	~ShowGroupsAction() {}
	std::vector<RequestPtr> query() const override
	{
		std::vector<RequestPtr> ptr;
		ptr.emplace_back(new SchedulerToJson());
		return ptr;
	}

	bool evaluate(const std::vector<ResponsePtr> &resp) const override
	{
		auto it = std::find_if(
		    resp.begin(), resp.end(), [&](const ResponsePtr &ptr) {
			    return ptr->name() ==
				   Response::NAME::SCHEDULER_TO_JSON;
		    });
		assert((it != resp.end()) && "incomplete response");

		const Response &response = **it;
		if (response.type() == Response::TYPE::EXCEPTION) {
			std::cout << "scheduler-to-json: exception: "
				  << dynamic_cast<const ExceptionResponse &>(
					 response).text() << std::endl;
			return false;
		}

		// get all FillGroups
		std::vector<FillGroup> fgs;
		for (auto &g : dynamic_cast<const JsonResponse &>(response)
				   .value()["groups"]) {
			fgs.push_back(FillGroup::fromJson(g));
		}
		// print all FillGroups
		for (auto &g : fgs) {
			std::cout << "group[id = " << g.getID()
				  << "] = {\n\tdetectors = [ ";
			std::for_each(g.getDetectors().begin(),
				      g.getDetectors().end(), [&](int c) {
					      std::cout << std::to_string(c)
							<< " ";
				      });
			std::cout << "];\n\tfill_times = [ ";
			std::for_each(g.getFillTimes().begin(),
				      g.getFillTimes().end(),
				      [&](const FillTime &time) {
					      std::cout << time.hour << ":"
							<< time.minute << " ";
				      });
			std::cout << "];\n}\n";
		}
		return true;
	}
};

class StatusAction : public Action
{
	int id_;

public:
	explicit StatusAction(const char *optarg) : id_(str_to_int(optarg)) {}
	~StatusAction() {}

	std::vector<RequestPtr> query() const override
	{
		std::vector<RequestPtr> ptr;
		ptr.emplace_back(new DetectorToJson(id_));
		ptr.emplace_back(new GetDetectorInletTemperature(id_));
		ptr.emplace_back(new GetDetectorOutletTemperature(id_));
		return ptr;
	}

	bool evaluate(const std::vector<ResponsePtr> &resp) const override
	{
		// returns either an empty response or an exception
		auto json_it = std::find_if(
		    resp.begin(), resp.end(), [&](const ResponsePtr &ptr) {
			    return ptr->name() ==
				   Response::NAME::DETECTOR_TO_JSON;
		    });
		auto inlet_it = std::find_if(
		    resp.begin(), resp.end(), [&](const ResponsePtr &ptr) {
			    return ptr->name() ==
				   Response::NAME::
				       GET_DETECTOR_TO_INLET_TEMPERATURE;
		    });
		auto outlet_it = std::find_if(
		    resp.begin(), resp.end(), [&](const ResponsePtr &ptr) {
			    return ptr->name() ==
				   Response::NAME::
				       GET_DETECTOR_TO_OUTLET_TEMPERATURE;
		    });

		if (json_it == resp.end() || inlet_it == resp.end() ||
		    outlet_it == resp.end()) {
			std::cout << "calf-client: status: incomplete response"
				  << std::endl;
			return false;
		}

		// all responses found
		if (json_it->get()->type() == Response::TYPE::EXCEPTION) {
			std::cout << "calf-client: detector_to_json: "
				  << dynamic_cast<const ExceptionResponse &>(
					 *json_it->get()).text() << std::endl;
			return false;
		}

		const auto &json =
		    dynamic_cast<const JsonResponse &>(*json_it->get()).value();

		assert(json.isObject() && "no json object");
		assert(json.isMember("inlet_sensor_id") &&
		       "inlet sensor id missing");
		assert(json.isMember("outlet_sensor_id") &&
		       "outlet sensor id missing");

		if (json["inlet_sensor_id"].asInt() >= 0 &&
		    !print_sensor_temperature(std::cout << "detector[" << id_
							<< "].inlet: ",
					      *inlet_it)) {
			return false;
		}
		if (json["outlet_sensor_id"].asInt() >= 0 &&
		    !print_sensor_temperature(std::cout << "detector[" << id_
							<< "].outlet: ",
					      *outlet_it)) {
			return false;
		}

		return true;
	}

private:
	static bool
	print_sensor_temperature(std::ostream &os,
				 const ResponsePtr &sensor_temp_response)
	{
		assert(sensor_temp_response && "unique_ptr owns no Response");
		assert(sensor_temp_response->name() ==
			   Response::NAME::GET_DETECTOR_TO_INLET_TEMPERATURE ||
		       sensor_temp_response->name() ==
			       Response::NAME::
				   GET_DETECTOR_TO_OUTLET_TEMPERATURE &&
			   "Response is not of the right type");
		if (sensor_temp_response->type() == Response::TYPE::EXCEPTION) {
			os << "exception: "
			   << dynamic_cast<const ExceptionResponse &>(
				  *sensor_temp_response).text() << "\n";
			return false;
		}
		os << dynamic_cast<const TemperatureResponse &>(
			  *sensor_temp_response)
			  .temperature()
			  .inCelsius() << " celsius\n";

		return true;
	}
};

struct ListAction : public Action {

	ListAction() {}
	~ListAction() {}

	std::vector<RequestPtr> query() const override
	{
		std::vector<RequestPtr> ptr;
		ptr.emplace_back(new CircuitToJson());
		return ptr;
	}

	bool evaluate(const std::vector<ResponsePtr> &resp) const override
	{
		auto it =
		    find_if(resp.begin(), resp.end(), [](const ResponsePtr &r) {
			    return r->name() == Response::NAME::CIRCUIT_TO_JSON;
		    });

		if (it == resp.end()) {
			std::cout << "calf-client: list: incomplete response"
				  << std::endl;
		}

		const auto &json =
		    dynamic_cast<const JsonResponse &>(**it).value();

		// get detectors
		std::vector<std::pair<int, Detector>> detectors;
		for (const auto &d : json["detectors"]) {
			detectors.emplace_back(Detector::fromJson(d));
		}

		// get vent boxes ( for showing the channels )
		std::vector<std::pair<int, VentBox>> boxes;
		for (const auto &b : json["vent_boxes"]) {
			boxes.emplace_back(VentBox::fromJson(b));
		}

		for (auto &d : detectors) {
			// find box which contains the vent_id
			auto box =
			    find_if(boxes.begin(), boxes.end(),
				    [&](const std::pair<int, VentBox> &b) {
					    return b.second.contains(
						d.second.getVentID());
				    });

			if (box == boxes.end()) {
				std::cout << "id: " << d.first
					  << " box: unknown, abort"
					  << std::endl;
				return false;
			}

			// TODO: split into smaller functions
			// get the associated channel
			try {
				VentBox::CHANNEL channel =
				    box->second.query(d.second.getVentID());

				std::cout
				    << "detector[id = " << d.first
				    << "] is_active: " << std::boolalpha
				    << d.second.isActive()
				    << "\n\tat vent_box[id = " << box->first
				    << "]["
				    << "channel = " << std::to_string(channel)
				    << "]";

				if (d.second.hasInletSensor()) {
					std::cout << "\n\tusing inlet[id = "
						  << d.second.getInletSensorID()
						  << "]";
				}
				if (d.second.hasOutletSensor()) {
					std::cout
					    << "\n\tusing outlet[id = "
					    << d.second.getOutletSensorID()
					    << "] ";
				}

			} catch (VentBoxError &ex) {
				std::cout << ex.what();
				return false;
			}
			std::cout << std::endl;
		}
		return true;
	}
};
/*
class WatchAction : public Action {
	mutable Client client_;
	const std::string optarg_;
public:
	WatchAction(const char *host, const char *port, const char *optarg)
	    : client_(host, port), optarg_(optarg)
	{
	}
	~WatchAction() {}

	std::vector<RequestPtr> query() const override
	{
		std::vector<RequestPtr> ptr;
		ptr.emplace_back(new CircuitToJson());
		return ptr;
	}

	bool evaluate(const std::vector<ResponsePtr> &resp) const override {
		(void)resp;
		std::cout << "watch: quit by pressing a button\n";
		volatile bool cancel = false;
		// start thread
		std::thread thr(WatchAction::watchThread, std::ref(client_),
				optarg_.c_str(), std::ref(cancel));

		getchar();
		cancel = true;

		thr.join();
		std::cout << "watch: finished\n";
		return true;
	}
private:
	static void watchThread(Client &client_, const char *optarg,
				volatile bool &cancel)
	{
		ActionPtr action( new StatusAction(optarg));
		while(!cancel) {
			client_.communicate(action);
			sleep(1);
		}
	}
};
*/
////////////////////////////////////////////////////////////
//		main function
////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
	logger::setLogLevel(LOG_LEVEL::DEBUG);

	struct option long_opts[]{
		{ "port", required_argument, nullptr, 'p' },
		{ "host", required_argument, nullptr, 'o' },
		{ "attach", required_argument, nullptr, 'a' },
		{ "detach", required_argument, nullptr, 'd' },
		{ "force-fill-group", required_argument, nullptr, 'f' },
		{ "cancel-filling", no_argument, nullptr, 'c'},
		{ "show-groups", no_argument, nullptr, 'g' },
		{ "status", required_argument, nullptr, 's' },
		{ "list", no_argument, nullptr, 'l' },
		//	{ "watch", required_argument, nullptr, 'w' },
		{ "help", no_argument, nullptr, 'h' },
		{ nullptr, no_argument, nullptr, 0 }
	};

	(void)str_to_int;

	ActionPtr action(nullptr);

	const char *host = nullptr;
	const char *port = nullptr;
	int c = -1;
	while ((c = getopt_long(argc, argv, "", long_opts, nullptr)) != -1) {
		switch (c) {
		case 'p':
			port = optarg;
			break;
		case 'h':
			print_help(long_opts);
			return 0;
		case 'o':
			host = optarg;
			break;
		case 'a':
			action = ActionPtr(new AttachAction(optarg));
			break;
		case 'd':
			action = ActionPtr(new DetachAction(optarg));
			break;
		case 'f':
			action = ActionPtr(new ForceFillAction(optarg));
			break;
		case 'c':
			action = ActionPtr(new CancelFillAction());
			break;
		case 'g':
			action = ActionPtr(new ShowGroupsAction());
			break;
		case 's':
			action = ActionPtr(new StatusAction(optarg));
			break;
		case 'l':
			action = ActionPtr(new ListAction());
			break;
		/*
		case 'w':
			action = ActionPtr(
			    new WatchAction(host, port, optarg));
			break;
		*/
		default:
			unknown_opt();
			return -1;
		};
	}

	try {
		Client client(host, port);
		return (action) ? client.communicate(action) : -1;
	} catch (ClientError &ex) {
		std::cerr << "calf-client: " << ex.what() << "\n";
	}
}
