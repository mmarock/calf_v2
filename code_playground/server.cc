

#include "../header/Calf.hh"

#include "../config_files/config.h"

#include <algorithm>
#include <iostream>

#include <cstdlib>
#include <climits>

#include <getopt.h>

using namespace CALF;

static int get_unsigned_number(const char *str) {
	if (!str) {
		std::cout << "get_unsigned_number: not string given";
		return -1;
	}

	char *endptr = NULL;
	auto result = strtol(str, &endptr, 10);
	if (errno == ERANGE && ( result == LONG_MIN || result == LONG_MAX)) {
		perror("get_unsigned_number: strol():");
		return -1;
	}

	if (str == endptr) {
		std::cerr << "get_unsigned_number: no digits found"
			  << std::endl;
		return -1;
	}

	return result;
}

static int get_circuit_id(const char *optarg) {
	return get_unsigned_number(optarg);
}

static void print_help(const char *const help[] ) {
	const char *const *it = help;
	while(*it) {
		std::cout << *it << std::endl;
		it++;
	}
}


int main(int argc, char ** argv)
{
	struct option long_options[] = {
		{"circuit-id", required_argument, nullptr, 'c'},
		{"clear-setup", no_argument, nullptr, 's'},
		{"debug", no_argument, nullptr, 'd'},
		{"help", no_argument, nullptr, 'h'},
		{"no-daemonize", no_argument, nullptr, 'n'},
		{"verbose", no_argument, nullptr, 'v'},
		{"version", no_argument, nullptr, 'w'},
		{0,	0,	nullptr,	0}
	};

	const char *help_string[] = {
		"\t  --circuit-id=id\tload the given circuit id",
		"\t[ --clear-setup ]\tstart with a clear setup",
		"\t[ --debug ]\tset the verbosity to DEBUG",
		"\t[ --help ]\tprint this help",
		"\t[ --no-daemonize ]\tdo not fork the process",
		"\t[ --verbose ]\tincrease the verbosity by one",
		"\t[ --version ]\tprint the latest version string and exit",
		NULL
	};

	int circuit_id = -1;
	bool daemonize = true;
	bool from_config = true;

	int c = -1;
	while ((c = getopt_long(argc, argv, "", long_options, nullptr)) != -1) {
		switch(c) {
			case 'c':
				if ((circuit_id = get_circuit_id(optarg)) < 0) {
					return -1;
				}
				break;
			case 's':
				from_config = false;
				break;
			case 'd':
				Calf::debugVerbosity();
				break;
			case 'h':
				print_help(help_string);
				return 0;
			case 'n':
				daemonize = false;
				break;
			case 'v':
				Calf::increaseVerbosity();
				break;
			case 'w':
				std::cout << "Calf: " << CALF_VERSION_STRING
					  << std::endl;
				return 0;
			default:
				std::cerr << "Calf: unknown option" << std::endl;
				print_help(help_string);
				return -1;
		};
	}

	if (circuit_id < 0) {
		std::cout << "insert a valid circuit-id"<< std::endl;
		return -1;
	}

	Calf::run(circuit_id, daemonize, from_config);

	return 0;
}
