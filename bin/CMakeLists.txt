include_directories(${CMAKE_CURRENT_BINARY_DIR})

include_directories(${LIBUSB-1.0_INCLUDE_DIRS})

include_directories(config_files)

pkg_check_modules(JSONCPP jsoncpp)
include_directories(${JSONCPP_INCLUDE_DIRS})

set (SERVER_DEPS ../src/logger.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/Circuit.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/DescriptorGuard.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/USBEnvironment.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/ObjectSerialisation.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/Requests.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/PIDLock.cc ${SERVER_DEPS})
#set (SERVER_DEPS ../src/RTDConverter.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/RTDCalculator.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/Scheduler.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/FillGroup.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/FillTime.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/FillReport.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/MainVent.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/ValveVent.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/Detector.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/USBTemp.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/NotificationSystem.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/ThreadedConnection.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/RRDManager.cc ${SERVER_DEPS})
set (SERVER_DEPS ../src/RoundRobinArchive.cc ${SERVER_DEPS})

message("TODO: Create object lib to reduce compile time...")

add_executable(calfd ${SERVER_DEPS} ../src/server.cc)
target_link_libraries(calfd ${JSONCPP_LDFLAGS} ${LIBUSB-1.0_LDFLAGS} rrd esmtp pthread)

add_executable(testclient ${SERVER_DEPS} ../src/client.cc)
target_link_libraries(testclient ${JSONCPP_LDFLAGS} ${LIBUSB-1.0_LDFLAGS} rrd esmtp pthread)

add_executable(calf-cli ${SERVER_DEPS} ../src/calf-cli.cc)
target_link_libraries(calf-cli ${JSONCPP_LDFLAGS} ${LIBUSB-1.0_LDFLAGS} rrd pthread esmtp)
