#include "../header/Scheduler.hh"

#include "../config_files/config.h"
#include "../header/Circuit.hh"
#include "../header/Detector.hh"
#include "../header/FillReport.hh"
#include "../header/logger.hh"
#include "../header/NotificationSystem.hh"

#include <algorithm>
#include <condition_variable>
#include <fstream>
#include <mutex>
#include <queue>
#include <stdexcept>
#include <thread>

#include <cassert>
#include <ctime>


#include <json/json.h>

namespace CALF
{

using namespace std;
using namespace std::chrono;
using time_point = system_clock::time_point;

SchedulerError::SchedulerError(string msg)
    : msg_(string("SchedulerError: ") + move(msg))
{
}
SchedulerError::SchedulerError(const char *msg)
    : msg_(string("SchedulerError: ") + msg)
{
}

const char *SchedulerError::what() const noexcept { return msg_.c_str(); }

struct NoScheduledGroup : public SchedulerError
{
	explicit NoScheduledGroup(int group_id)
	    : SchedulerError(string("NoScheduledGroup: group ") +
			     to_string(group_id) + " not found")
	{
	}
};

struct SchedulerOverlapError : public SchedulerError
{
	explicit SchedulerOverlapError()
	    : SchedulerError(string("SchedulerOverlapError"))
	{
	}
};

struct SchedulerDetectorNotFound : public SchedulerError
{
	SchedulerDetectorNotFound(int group_id, int detector_id)
	    : SchedulerError(string("DetectorNotFound: group[") +
			     to_string(group_id) + "].detector[" +
			     to_string(detector_id) + "]")
	{
	}
};

struct SchedulerNoConfigFound : public SchedulerError
{
	explicit SchedulerNoConfigFound(const char *path)
	    : SchedulerError(string("SchedulerNoConfigFound: ") + path)
	{}
};

struct SchedulerConfigParserError : public SchedulerError
{
	SchedulerConfigParserError(const char *path, string err)
	    : SchedulerError(string("SchedulerConfigParserError: file: ") + path +
			     ", error: " + err)
	{
	}
};

struct SchedulerCircuitNotFound : public SchedulerError
{
	explicit SchedulerCircuitNotFound(int circuit_id)
	    : SchedulerError(string("SchedulerCircuitNotFound: id:") +
			     to_string(circuit_id))
	{
	}
};

struct SchedulerMissingObject : public SchedulerError
{
	explicit SchedulerMissingObject(const char *obj)
	    : SchedulerError(string("SchedulerMissingObject: ") + obj)
	{
	}
};

////////////////////////////////////////////////////////////
//		FillTime member and helper functions
////////////////////////////////////////////////////////////

FillTime::FillTime(int h, int m) : hour(h), minute(m)
{
	if (hour < 0 || hour > 23) {
		throw out_of_range("FillTime: hour out of range (hour=" +
				   to_string(hour) + ")");
	} else if (minute < 0 || minute > 59) {
		throw out_of_range("FillTime: minute out of range (minute=" +
				   to_string(minute) + ")");
	}
}

FillTime::FillTime(const FillTime &rhs) noexcept : hour(rhs.hour),
						   minute(rhs.minute)
{
}

FillTime &FillTime::operator=(const FillTime &rhs) noexcept
{
	hour = rhs.hour;
	minute = rhs.minute;
	return *this;
}

FillTime::FillTime(FillTime &&rhs) noexcept : hour(std::move(rhs.hour)),
					      minute(std::move(rhs.minute))
{
	rhs.hour = 0;
	rhs.minute = 0;
}

FillTime &FillTime::operator=(FillTime &&rhs) noexcept
{
	hour = std::move(rhs.hour);
	minute = std::move(rhs.minute);

	rhs.hour = 0;
	rhs.minute = 0;

	return *this;
}

system_clock::time_point FillTime::toTimepoint() const
{
	time_t t = time(NULL);

	struct tm tt;
	localtime_r(&t, &tt);
	tt.tm_hour = hour;
	tt.tm_min = minute;

	return system_clock::from_time_t(mktime(&tt));
}

bool FillTime::isNow() const
{
	const auto now = system_clock::now();
	return (duration_cast<minutes>(toTimepoint().time_since_epoch() -
				       now.time_since_epoch()).count() == 0);
}

////////////////////////////////////////////////////////////
//		FillGroup definitions
////////////////////////////////////////////////////////////

FillGroup::FillGroup(int id, const vector<int> &ids,
		     const vector<FillTime> &tps, const minutes &timeout)
    : id_(id), detector_ids_(ids), time_points_(tps), timeout_(timeout)
{
}

FillGroup::FillGroup(int id, vector<int> &&ids, vector<FillTime> &&tps,
		     const minutes &timeout)
    : id_(id), detector_ids_(move(ids)), time_points_(move(tps)),
      timeout_(timeout)
{
}

bool FillGroup::hasDetector(int id) const
{
	return (findDetectorID(id) != detector_ids_.end());
}

bool FillGroup::needsFilling() const
{
	return (find_if(time_points_.begin(), time_points_.end(),
			mem_fn(&FillTime::isNow)) != time_points_.end());
}

vector<int>::iterator FillGroup::findDetectorID(int id)
{
	return find(detector_ids_.begin(), detector_ids_.end(), id);
}

vector<int>::const_iterator FillGroup::findDetectorID(int id) const
{
	return find(detector_ids_.begin(), detector_ids_.end(), id);
}

FillGroup FillGroup::fromJson(const Json::Value &val)
{
	vector<int> detector_ids;
	for (auto &detector : val["detectors"]) {
		detector_ids.push_back(detector.asInt());
	}

	vector<FillTime> tps;
	for (auto &tp : val["timepoints"]) {
		tps.emplace_back(tp["hour"].asInt(), tp["minute"].asInt());
	}

	return FillGroup(
			val["fill_group_id"].asInt(),
			move(detector_ids),
			move(tps),
			chrono::minutes(val["timeout"].asUInt()));
}

Json::Value FillGroup::toJson() const
{
	Json::Value fgr(Json::ValueType::objectValue);

	fgr["fill_group_id"] = id_;

	fgr["timeout"] = (uint16_t)timeout_.count();

	fgr["timepoints"] = Json::Value(Json::ValueType::arrayValue);
	for (auto &time : time_points_) {
		Json::Value entry(Json::ValueType::objectValue);

		entry["hour"] = time.hour;
		entry["minute"] = time.minute;

		fgr["timepoints"].append(entry);
	}

	fgr["detectors"] = Json::Value(Json::ValueType::arrayValue);
	for (int detector : detector_ids_) {
		fgr["detectors"].append(Json::Value(detector));
	}

	return fgr;
}

////////////////////////////////////////////////////////////
//		Scheduler::Impl declaration
////////////////////////////////////////////////////////////

class Scheduler::Impl
{
	Circuit &circuit_;

	mutex mtx_;
	vector<FillGroup> groups_;

	bool cancel_all_threads_;
	thread scheduler_thread_;

	volatile bool cancel_fill_;

	mutex is_filling_mtx_;
	condition_variable is_filling_cv_;
	volatile bool is_filling_;


public:
	explicit Impl(Circuit &circuit);
	~Impl();

	void clear();

	void schedulerThread();
	void groupThread(const FillGroup &group);

	void setFillingState(bool is_filling);

	FillGroup &findGroup(int group_id);
	const FillGroup &findGroup(int group_id) const;

	bool hasGroup(int group_id) const;

	bool hasOverlap(const vector<FillTime> &tp_vec,
			const minutes &timeout) const;
	bool hasOverlap(const FillTime &fill_time,
			const minutes &timeout) const;

	static bool tpInRange(const time_point &tp, const time_point &begin,
			      const time_point &end);

	int addGroup(const vector<int> &detector_ids,
		      const vector<FillTime> &tp_vector);
	void removeGroup(int group_id);

	bool hasDetector(int group_id, int detector_id) const;
	void addDetector(int group_id, int detector_id);
	void removeDetector(int group_id, int detector_id);

	void rmDetectorFromAllGroups(int detector_id);

	void cancelGroupFill();

	void forceFillGroup(int group_id);

	vector<FillGroup>::const_iterator cbegin() const;
	vector<FillGroup>::const_iterator cend() const;

	static Scheduler fromJson(const Json::Value &val, Circuit &circ);
	void reload(const Json::Value &val);
	Json::Value toJson() const;

	static Scheduler fromConfigFile(Circuit &c);
	void reloadConfigFile();
	void toConfigFile() const;

	static const char *defaultConfigFile();
};

////////////////////////////////////////////////////////////
//		Scheduler::Impl definitions
////////////////////////////////////////////////////////////

Scheduler::Impl::Impl(Circuit &c)
    : circuit_(c), cancel_all_threads_(false),
      scheduler_thread_(&Scheduler::Impl::schedulerThread, this),
      cancel_fill_(false), is_filling_(false)
{
}

Scheduler::Impl::~Impl()
{
	cancel_all_threads_ = true;
	scheduler_thread_.join();
}

void Scheduler::Impl::clear() {
	lock_guard<mutex> lk(mtx_);

	groups_.clear();
}

void Scheduler::Impl::schedulerThread()
{
	queue<int> deferred_groups;

	while (!cancel_all_threads_) {
		auto it = find_if(groups_.begin(), groups_.end(),
				  mem_fn(&FillGroup::needsFilling));
		if (it != groups_.end()) {
			deferred_groups.push(it->id_);
			this_thread::sleep_for(minutes(1));
		}
		// TODO fix possible race-condition
		if (!is_filling_ && !deferred_groups.empty()) {
			thread group_thread =
			    thread(&Scheduler::Impl::groupThread, this,
				   ref(findGroup(deferred_groups.front())));
			group_thread.detach();
			deferred_groups.pop();
		}
		this_thread::sleep_for(seconds(1));
	}
}

void Scheduler::Impl::groupThread(const FillGroup &group)
{
	setFillingState(true);

	circuit_.fill(cancel_fill_, group);

	setFillingState(false);
}

void Scheduler::Impl::setFillingState(bool is_filling)
{
	{
		lock_guard<mutex> lk(is_filling_mtx_);
		is_filling_ = is_filling;
	}
	is_filling_cv_.notify_one();
}

FillGroup &Scheduler::Impl::findGroup(int group_id)
{
	auto group =
	    find_if(groups_.begin(), groups_.end(),
		    [&](const FillGroup &g) { return (g.id_ == group_id); });
	if (group == groups_.end()) {
		throw out_of_range("Scheduler::FindGroup: id " +
				   to_string(group_id) + " not found");
	}
	return *group;
}

const FillGroup &Scheduler::Impl::findGroup(int group_id) const
{
	auto group =
	    find_if(groups_.begin(), groups_.end(),
		    [&](const FillGroup &g) { return (g.id_ == group_id); });
	if (group == groups_.end()) {
		throw out_of_range("Scheduler::FindGroup: id " +
				   to_string(group_id) + " not found");
	}
	return *group;
}

bool Scheduler::Impl::hasGroup(int group_id) const
{
	return (
	    find_if(groups_.begin(), groups_.end(), [&](const FillGroup &g) {
		    return (g.id_ == group_id);
	    }) != groups_.end());
}

bool Scheduler::Impl::hasOverlap(const vector<FillTime> &tp_vec,
				 const minutes &timeout) const
{
	for (const auto &tp : tp_vec) {
		if (hasOverlap(tp, timeout)) {
			return true;
		}
	}
	return false;
}

bool Scheduler::Impl::hasOverlap(const FillTime &new_start,
				 const minutes &new_timeout) const
{
	const time_point new_begin(new_start.toTimepoint());
	const time_point new_end(new_begin + new_timeout);
	for (const FillGroup &group : groups_) {
		for (const FillTime red_tp_ : group.time_points_) {
			const time_point ex_begin(red_tp_.toTimepoint());
			const time_point ex_end(ex_begin + group.timeout_);

			if (tpInRange(ex_begin, new_begin, new_end) ||
			    tpInRange(new_begin, ex_begin, ex_end) ||
			    tpInRange(new_end, ex_begin, ex_end) ||
			    tpInRange(ex_begin, new_begin, new_end)) {
				return true;
			}
		}
	}
	return false;
}

bool Scheduler::Impl::tpInRange(const time_point &tp, const time_point &begin,
				const time_point &end)
{
	return ((tp >= begin) && (tp <= end));
}

int Scheduler::Impl::addGroup(const vector<int> &detector_ids,
			       const vector<FillTime> &tp_vector)
{
	auto timeout = circuit_.fillTimeout(detector_ids);

	for (const auto &tp : tp_vector) {
		if (hasOverlap(tp, timeout)) {
			throw SchedulerOverlapError();
		}
	}
	lock_guard<mutex> lk(mtx_);
	groups_.emplace_back(groups_.size(), detector_ids, tp_vector, timeout);
	return groups_.back().id_;
}

void Scheduler::Impl::removeGroup(int group_id)
{
	lock_guard<mutex> lk(mtx_);
	auto it = find_if(groups_.begin(), groups_.end(),
			  [&](const FillGroup &g)
			      -> bool { return (g.id_ == group_id); });
	if (it != groups_.end()) {
		it = groups_.erase(it);
		while (it != groups_.end()) {
			it->id_--;
			it++;
		}
	} else {
		throw runtime_error(
		    "Scheduler::removeGroup: group_id not found");
	}
}

bool Scheduler::Impl::hasDetector(int group_id, int detector_id) const
{
	return findGroup(group_id).hasDetector(detector_id);
}

void Scheduler::Impl::addDetector(int group_id, int detector_id)
{
	lock_guard<mutex> lk(mtx_);

	auto timeout = circuit_.fillTimeout(detector_id);

	auto &grp = findGroup(group_id);
	if (!grp.hasDetector(detector_id)) {
		if (grp.timeout_ >= timeout ||
		    (!hasOverlap(grp.time_points_, timeout))) {
			grp.detector_ids_.push_back(detector_id);
		} else {
			throw SchedulerOverlapError();
		}
	}
}

void Scheduler::Impl::removeDetector(int group_id, int detector_id)
{
	lock_guard<mutex> lk(mtx_);

	auto grp = findGroup(group_id);
	auto it = grp.findDetectorID(detector_id);

	if (it == grp.detector_ids_.end()) {
		throw SchedulerDetectorNotFound(group_id, detector_id);
	}
	grp.detector_ids_.erase(it);
	grp.timeout_ = circuit_.fillTimeout(grp.detector_ids_);
}

void Scheduler::Impl::rmDetectorFromAllGroups(int detector_id)
{
	lock_guard<mutex> lk(mtx_);
	auto it = groups_.begin();
	while ((it = std::find_if(it, groups_.end(), [&](const FillGroup &grp) {
		return grp.hasDetector(detector_id);
	})) != groups_.end()) {
		it->detector_ids_.erase(it->findDetectorID(detector_id));
	}
}

void Scheduler::Impl::cancelGroupFill()
{
	unique_lock<mutex> lk(is_filling_mtx_);
	cancel_fill_ = true;
	while (is_filling_) {
		is_filling_cv_.wait(lk);
	}
	cancel_fill_ = false;
}

void Scheduler::Impl::forceFillGroup(int group_id)
{
	if (is_filling_) {
		throw runtime_error(
		    "Scheduler::forceFillGroup: failed: already filling");
	}
	thread t(&Scheduler::Impl::groupThread, this, ref(findGroup(group_id)));

	t.detach();
}

vector<FillGroup>::const_iterator Scheduler::Impl::cbegin() const
{
	return groups_.cbegin();
}

vector<FillGroup>::const_iterator Scheduler::Impl::cend() const
{
	return groups_.cend();
}

Scheduler Scheduler::Impl::fromJson(const Json::Value &val, Circuit &circ)
{
	auto &groups = val["groups"];
	if (!groups.isArray()) {
		throw SchedulerMissingObject("groups");
	}

	Scheduler scheduler(circ);

	for (auto &grp : groups) {
		scheduler.impl_->groups_.push_back(FillGroup::fromJson(grp));
	}

	return move(scheduler);
}

void Scheduler::Impl::reload(const Json::Value &val)
{
	// reload groups
	vector<FillGroup> groups;
	for(auto &grp : val["scheduler"]["groups"]) {
		groups.push_back(FillGroup::fromJson(grp));
	}

	// everything worked, now swap the groups
	lock_guard<mutex> lk(mtx_);
	swap(groups_, groups);
}

Json::Value Scheduler::Impl::toJson() const
{
	Json::Value scheduler(Json::ValueType::objectValue);

	auto &groups =
	    (scheduler["groups"] = Json::Value(Json::ValueType::arrayValue));

	for (auto &grp : groups_) {
		groups.append(grp.toJson());
	}

	return scheduler;
}

/**
 * @Synopsis Create a Scheduler from the defaultConfigFile()
 *
 * @Returns The memorized scheduler
 */
Scheduler Scheduler::Impl::fromConfigFile(Circuit &c)
{
	ifstream file(defaultConfigFile(), std::ios::in);
	if (!file.is_open()) {
		// file error
		throw SchedulerNoConfigFound(defaultConfigFile());
	}

	Json::Reader reader;
	Json::Value root = Json::Value(Json::ValueType::objectValue);
	if (!reader.parse(file, root)) {
		// parse error
		throw SchedulerConfigParserError(defaultConfigFile(),
					reader.getFormattedErrorMessages());
	}
	if (!root.isObject()) {
		throw SchedulerError("fromConfigFile: need object");
	}
	if (!root["schedulers"].isArray()) {
		throw SchedulerError(
		    "fromConfigFile: 'schedulers' = array not found");
	}

	auto it = find_if(root["schedulers"].begin(), root["schedulers"].end(),
			  [&](Json::Value &v) {
				  return v["circuit_id"].isInt() &&
					 (v["circuit_id"] == c.getID());
			  });

	if (it == root["schedulers"].end()) {
		throw SchedulerCircuitNotFound(c.getID());
	}

	return fromJson(*it, c);
}

void Scheduler::Impl::reloadConfigFile()
{
	ifstream file(defaultConfigFile(), std::ios::in);
	if (!file.is_open()) {
		// file error
		throw SchedulerNoConfigFound(defaultConfigFile());
	}

	Json::Reader reader;
	Json::Value root = Json::Value(Json::ValueType::objectValue);
	if (!reader.parse(file, root)) {
		// parse error
		file.close();
		throw SchedulerConfigParserError(defaultConfigFile(),
					reader.getFormattedErrorMessages());
	}
	file.close();

	reload(root);
}

void Scheduler::Impl::toConfigFile() const
{
	const char *path = defaultConfigFile();
	std::fstream file(path, std::ios::in);
	Json::Reader reader;
	Json::Value root = Json::Value(Json::ValueType::objectValue);

	if (!file.is_open()) {
		throw SchedulerNoConfigFound(path);
	}

	if (!reader.parse(file, root)) {
		throw SchedulerConfigParserError(
		    path, reader.getFormattedErrorMessages());
	}
	file.close();

	Json::Value &schedulers = root["schedulers"];

	auto it = find_if(schedulers.begin(), schedulers.end(),
			  [&](Json::Value &scheduler) {
				  return (scheduler["circuit_id"].asInt() ==
					  circuit_.getID());
			  });
	if (it != schedulers.end()) {
		Json::Value val;
		schedulers.removeIndex(std::distance(schedulers.begin(), it),
				       &val);
	}

	schedulers.append(toJson());

	file.open(path, std::ios::out);
	Json::StyledStreamWriter writer;

	writer.write(file, root);
}

const char *Scheduler::Impl::defaultConfigFile()
{
	return SCHEDULER_CONFIG_DIR"/scheduler.config";
}

////////////////////////////////////////////////////////////
//		Scheduler definitions
////////////////////////////////////////////////////////////

Scheduler::Scheduler(Circuit &circuit)
    : impl_(new Impl(circuit))
{
}

Scheduler::~Scheduler() {}

Scheduler::Scheduler(Scheduler &&rhs) : impl_(move(rhs.impl_)) {}

Scheduler &Scheduler::operator=(Scheduler &&rhs) {
	impl_ = move(rhs.impl_);
	return *this;
}

void Scheduler::clear() {
	impl_->clear();
}

bool Scheduler::hasGroup(int group_id) const
{
	return impl_->hasGroup(group_id);
}

int Scheduler::addGroup(const vector<int> &detector_ids,
			 const vector<FillTime> &tp_vector)
{
	return impl_->addGroup(detector_ids, tp_vector);
}

void Scheduler::removeGroup(int group_id) { impl_->removeGroup(group_id); }

void Scheduler::addDetector(int group_id, int detector_id)
{
	impl_->addDetector(group_id, detector_id);
}

void Scheduler::removeDetector(int group_id, int detector_id)
{
	impl_->removeDetector(group_id, detector_id);
}

void Scheduler::rmDetectorFromAllGroups(int detector_id)
{
	impl_->rmDetectorFromAllGroups(detector_id);
}

void Scheduler::cancelGroupFill() { impl_->cancelGroupFill(); }

void Scheduler::forceFillGroup(int group_id)
{
	impl_->forceFillGroup(group_id);
}

vector<FillGroup>::const_iterator Scheduler::cbegin() const
{
	return impl_->cbegin();
}

vector<FillGroup>::const_iterator Scheduler::cend() const
{
	return impl_->cend();
}

Scheduler Scheduler::fromJson(const Json::Value &val, Circuit &circ)
{
	return Scheduler::Impl::fromJson(val, circ);
}

void Scheduler::reload(const Json::Value &val)
{
	impl_->reload(val);
}

Json::Value Scheduler::toJson() const
{
	return impl_->toJson();
}

Scheduler Scheduler::fromConfigFile(Circuit &c)
{
	return Scheduler::Impl::fromConfigFile(c);
}

void Scheduler::reloadConfigFile()
{
	impl_->reloadConfigFile();
}

void Scheduler::toConfigFile() const
{
	impl_->toConfigFile();
}

const char *Scheduler::defaultConfigFile()
{
	return Scheduler::Impl::defaultConfigFile();
}

} // namespace CALF
