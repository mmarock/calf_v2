#ifndef SHEDULER_HH_INCLUDED
#define SHEDULER_HH_INCLUDED

#include <chrono>
#include <memory>
#include <vector>

namespace Json
{
class Value;
}

namespace CALF
{

class SchedulerError : public std::exception
{
	std::string msg_;
public:
	explicit SchedulerError(std::string msg);
	explicit SchedulerError(const char *msg);

	const char *what() const noexcept;
};

class Circuit;
class NotificationSystem;

struct FillTime {
	int hour;
	int minute;

	FillTime(int hour, int minute);

	FillTime(const FillTime &rhs) noexcept;
	FillTime &operator=(const FillTime &rhs) noexcept;

	FillTime(FillTime &&rhs) noexcept;
	FillTime &operator=(FillTime &&rhs) noexcept;

	std::chrono::system_clock::time_point toTimepoint() const;

	bool isNow() const;
};

struct FillGroup {
	int id_;
	std::vector<int> detector_ids_;
	std::vector<FillTime> time_points_;
	std::chrono::minutes timeout_;

	FillGroup(int id, const std::vector<int> &ids,
		  const std::vector<FillTime> &tps,
		  const std::chrono::minutes &min);

	FillGroup(int id, std::vector<int> &&ids, std::vector<FillTime> &&tps,
		  const std::chrono::minutes &min);

	bool hasDetector(int id) const;

	bool needsFilling() const;

	std::vector<int>::iterator findDetectorID(int id);
	std::vector<int>::const_iterator findDetectorID(int id) const;

	static FillGroup fromJson(const Json::Value &val);
	Json::Value toJson() const;
};

enum class FILL_JOB_STATE {
	OK,
	FILL_TIMEOUT,
	VENTILATION_TIMEOUT,
	WARM,
	CANCELLED,
	UNKNOWN
};

class Scheduler
{
	class Impl;
	std::unique_ptr<Impl> impl_;

public:
	explicit Scheduler(Circuit &circuit);
	~Scheduler();

	Scheduler(const Scheduler &) = delete;
	Scheduler &operator=(const Scheduler &) = delete;

	Scheduler(Scheduler &&);
	Scheduler &operator=(Scheduler &&);

	void clear();

	bool hasGroup(int group_id) const;

	int addGroup(const std::vector<int> &detector_ids,
		      const std::vector<FillTime> &tp_vector);

	void removeGroup(int group_id);

	void addDetector(int group_id, int detector_id);
	void removeDetector(int group_id, int detector_id);

	void rmDetectorFromAllGroups(int detector_id);

	void cancelGroupFill();
	void forceFillGroup(int group_id);

	std::vector<FillGroup>::const_iterator cbegin() const;
	std::vector<FillGroup>::const_iterator cend() const;

	static Scheduler fromJson(const Json::Value &val, Circuit &circ);
	void reload(const Json::Value &val);
	Json::Value toJson() const;

	static Scheduler fromConfigFile(Circuit &c);
	void reloadConfigFile();
	void toConfigFile() const;

private:
	static const char *defaultConfigFile();
};

} // namespace CALF

#endif // SHEDULER_HH_INCLUDED
