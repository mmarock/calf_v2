
#!/usr/bin/python

import rrdtool
import os,sys
import stat

from time import time
import syslog

RRD_DIR="/tmp/rrd_directory";
RRD_IMAGE_DIR=""

def dir_exists(dir):
    return stat.S_ISDIR(os.stat(dir))

# Create a new directory /tmp/rrd_pictures
def create_image_dir(path):


if __name__ == '__main__':

    if not dir_exists(RRD_DIR):
        syslog.syslog(syslog.LOG_WARNING, "no rrd directory: " + RRD_DIR)
        return -1

    if not dir_exists(RRD_IMAGE_DIR):
        syslog.syslog(syslog.LOG_INFO, "create new image dir: " + RRD_IMAGE_DIR)
        create_image_dir(RRD_IMAGE_DIR)

    now = time.time()

    for f in os.listdir(RRD_DIR):
        rrd_file = os.path.join(RRD_DIR, f)
        image_file = os.path.join(RRD_IMAGE_DIR, os.path.basename(f) + ".png")

        rrdtool.graph(image_file,
                '--imgformat', 'PNG',
                '--width', '600',
                '--height', '120',
                '--title', os.path.basename(f),
                '--start', now - 3600,
                '--end', now,
                'DEF:inlet=' + rrd_file + ':inlet:AVERAGE:start=end-1h',
                'DEF:outlet=' + rrd_file + ':outlet:AVERAGE:start=end-1h',
                'LINE1:inlet#00CCFF:"inlet"',
                'LINE2:outlet#FF00FF:"outlet"'
                )

