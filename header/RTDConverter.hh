

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RTDCONVERTER_HH_INCLUDED
#define RTDCONVERTER_HH_INCLUDED

#include "Temperature.hh"

#include <memory>
#include <vector>

namespace CALF
{
class RTDConverterError : public std::exception
{
    std::string msg_;

    public:
    explicit RTDConverterError(const char *msg);
    explicit RTDConverterError(const std::string &msg);

    const char *what() const noexcept;
};

class RTDImpl
{
    public:
    struct data_point {
	double resistance;
	double temperature_in_K;

	data_point(double resistance, double temperature);
    };

    private:
    std::string name_;
    std::vector<data_point> data_points_;

    public:
    const std::string &getName() const;

    RTDImpl(const std::string &name, const std::vector<data_point> &d_points);

    Temperature getTemperature(double resistance) const;
};

class RTDCalculator
{
    std::shared_ptr<RTDImpl> impl_;

    public:
    explicit RTDCalculator(const std::shared_ptr<RTDImpl> &impl);
    const std::string &getName() const;
    Temperature operator()(double resistance) const;
};

class RTDConverter
{
    public:
    RTDConverter() = delete;

    static void fromConfigFile();

    static std::shared_ptr<RTDImpl>
    makeRTD(const std::string &name,
	    const std::vector<RTDImpl::data_point> &data_points);

    static RTDCalculator getCalculator(const std::string &name);

    static double getDefaultTemperatureStep();
    static const char *getDefaultConfigFile();

    private:
    static std::vector<std::shared_ptr<RTDImpl>>::iterator
    findByName(const std::string &name);
    static std::vector<std::shared_ptr<RTDImpl>> &getRTDImpls();
};

} // namespace CALF

#endif // RTDCONVERTER_HH_INCLUDED
