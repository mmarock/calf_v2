

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAKE_UNIQUE_HPP_INCLUDED
#define MAKE_UNIQUE_HPP_INCLUDED

#include <memory>
#include <type_traits>

namespace custom
{
/**
 * @brief see std::make_shared<>()
 *
 * Use make_unique to get a new object confined in an unique pointer.
 * This is needed because std::make_unique did not make it in c++11, instead
 * they introduce it in c++14.
 * This implimentation is similar to an example in cppreference, please consider
 * the warning.
 *
 * @warning Do not use this with arrays
 * @note there is no new keyword needed, this allocates memory if needed
 * @param args These args are directly passed to the Constructor T(args)
 *
 * @return unique_ptr<T>
 */
template <class T, typename... Args>
std::unique_ptr<T> make_unique(Args &&... args)
{
    static_assert(!std::is_array<T>(), "T must not be an array");
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

} // namespace custom

#endif // MAKE_UNIQUE_HPP_INCLUDED
