

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DESCRIPTORGUARD_HH_INCLUDED
#define DESCRIPTORGUARD_HH_INCLUDED

namespace CALF
{
/**
 * @brief Lightweight class which releases the given descriptor with close()
 *
 * @note This class does nothing else than cleanup.
 * 		There is no verification that the given fd is valid!
 *
 * @note NEVER introduce an int operator
 * 	because this opens up to a lot of nasty casts!
 */
class DescriptorGuard
{
    int fd_;

    public:
    DescriptorGuard(int fd);
    ~DescriptorGuard();

    int fd() const noexcept;
};

} // namespace CALF

#endif // DESCRIPTORGUARD_HH_INCLUDED
