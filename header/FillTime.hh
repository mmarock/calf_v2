

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FILLTIME_HH_INCLUDED
#define FILLTIME_HH_INCLUDED

#include <chrono>

namespace Json
{
class Value;
}

namespace CALF
{
struct FillTime {
    int hour;
    int minute;

    FillTime(int hour, int minute);

    FillTime(const FillTime &ft);
    FillTime &operator=(const FillTime &ft);

    // minutes till the next action, throws runtime_error if localtime
    // failed
    std::chrono::minutes offset() const;

    Json::Value toJson() const;
    static FillTime fromJson(const Json::Value &val);
};

bool operator<=(const FillTime &lhs, const FillTime &rhs) noexcept;
bool operator==(const FillTime &lhs, const FillTime &rhs) noexcept;

} // namespace CALF

#endif // FILLTIME_HH_INCLUDED
