

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINVENT_HH_INCLUDED
#define MAINVENT_HH_INCLUDED

#include <string>

namespace Json
{
class Value;
}

namespace CALF
{
class MainVentError : public std::exception
{
    std::string msg_;

    public:
    explicit MainVentError(const std::string &msg);

    const char *what() const noexcept override;
};

class MainVent
{
    int id_;
    int vent_usb_;
    uint8_t vent_channel_;

    public:
    explicit MainVent(int id, int vent_usb, uint8_t vent_channel);

    int id() const noexcept;
    int getVentUSB() const noexcept;
    uint8_t getVentChannel() const noexcept;

    Json::Value toJson() const;
    static MainVent fromJson(const Json::Value &val);
};

} // namespace CALF

#endif // MAINVENT_HH_INCLUDED
