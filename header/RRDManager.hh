

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RRDMANAGER_HH_INCLUDED
#define RRDMANAGER_HH_INCLUDED

#include <memory>

namespace CALF
{
class Circuit;

/**
 * @brief This class handles a RRD file.
 *
 * @todo Im Moment muss bei jedem Move von Circuit der Circuit Pointer
 * aktualisiert werden.
 * 		Lösung wäre, dem Circuit allgemein den move zu verbieten.
 * 		Allerdings macht fromConfigFile ein move wesentlich einfacher...
 * 		Gibt es eine Möglichkeit zwei kompl. Probleme einfach zu machen?
 *
 *
 * @todo Verbessere die Fehlermeldungen dringend!
 *
 */
class RRDManager
{
    class Impl;
    std::unique_ptr<Impl> impl_;

    public:
    explicit RRDManager(Circuit *c);
    void updateCircuitPtr(Circuit *c);

    ~RRDManager();

    bool isSubscribed(int detector_id);

    void subscribe(int detector_id_, bool has_inlet, bool has_outlet);
    void unsubscribe(int id);
};
}

#endif // RRDMANAGER_HH_INCLUDED
