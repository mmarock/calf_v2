

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USBENVIRONMENT_HH_INCLUDED
#define USBENVIRONMENT_HH_INCLUDED

#include <string>
#include <vector>

namespace Json
{
class Value;
}

namespace CALF
{
class Temperature;

class USBEnvironmentError : public std::exception
{
    std::string msg_;

    public:
    explicit USBEnvironmentError(const char *msg);
    explicit USBEnvironmentError(const std::string &msg);

    const char *what() const noexcept override;
};

/**
 * @brief Encapsulate USBTemp devices.
 *
 * The USBEnvironment does not belong to any other object
 * but is global available. The user checks, if there is an
 * USB device connected to an id and the does stuff to the associated
 * dios or adcs.
 *
 * id -> USBDevice
 * 	(channel, id) -> dio
 * 	(channel, id) -> adc
 *
 * In principle the USBTemp devices are encapsulated to make them replaceable.
 * I do not like these Meilhaus boxes because they are fragile and expensive.
 * If someone wants to use arduinos + usb controller or something else, change
 * the USBEnvironment Impl class.
 *
 * @todo scrap this Meilhaus trash
 *
 */
class USBEnvironment
{
    class Impl;
    static Impl &getImpl();

    public:
    static bool haveUSB(int id) noexcept;

    static void usePT100Sensor();  // SensorError
    static void usePT1000Sensor(); // SensorError -> RTDError

    static void associate(const std::string &serial, int usb_id);

    static void open(int usb, uint8_t channel);		      // USBError
    static void close(int usb, uint8_t channel);	      // USBError
    static Temperature temperature(int usb, uint8_t channel); // USBError

    static void clear();  // Reset the environment, wirft Json Error, UBSError
    static void reload(); // wirft eine genaue Beschreibung
    static void loadUSBEnvironment(); // wirft -"-

    static Json::Value toJson(); // wirft
};				 // USBEnvironment

}; // namespace CALF

#endif // USBENVIRONMENT_HH_INCLUDED
