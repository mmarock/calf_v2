

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RTDCALCULATOR_HH_INCLUDED
#define RTDCALCULATOR_HH_INCLUDED

#include <memory>
#include <string>
#include <vector>

namespace Json
{
class Value;
}

namespace CALF
{
class Temperature;

class RTDCalculatorError : public std::exception
{
    std::string msg_;

    public:
    explicit RTDCalculatorError(const char *msg);
    explicit RTDCalculatorError(const std::string &msg);

    const char *what() const noexcept override;
};

struct FunctionVal {
    double resistance;
    double temperature;
};

struct RTDData {
    std::string name;
    std::vector<FunctionVal> vals;
};

class RTDCalculator
{
    class Impl;
    std::unique_ptr<Impl> impl_;

    friend class RTDCalculatorFactory;

    public:
    RTDCalculator();
    RTDCalculator(const std::vector<RTDData> &origin, const std::string &name);
    ~RTDCalculator();

    RTDCalculator(RTDCalculator &&);
    RTDCalculator &operator=(RTDCalculator &&);

    bool isIinitialised() const noexcept;

    const std::string &name() const noexcept;
    Temperature operator()(double resistance) const;
};

class RTDCalculatorFactory
{
    class Impl;
    static Impl &getImpl();

    public:
    RTDCalculatorFactory() = delete;

    static void loadRTDCalculator(); // no file, json error

    static void clear();
    static void reload(); // file not found

    static RTDCalculator getRTDCalc(const char *name); // name unbekannt

    static Json::Value toJson(); // json fehler

    static const char *default_path() noexcept;
};

} // namespace CALF

#endif // RTDCALCULATOR_HH_INCLUDED
