

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOGGER_HH_INCLUDED
#define LOGGER_HH_INCLUDED

#include <string>

namespace CALF
{
enum class LOG_LEVEL { EMERG, ALERT, CRIT, ERR, WARNING, NOTICE, INFO, DEBUG };

namespace log
{
}

/**
 * @brief Wrapper for sylog() call
 */
class logger
{
    static LOG_LEVEL log_level_;
    static bool logger_initialized_;

    public:
    static void log(LOG_LEVEL lvl, const char *fmt, ...);

    static void setLogLevel(LOG_LEVEL new_log_level);
    static LOG_LEVEL getLogLevel();

    private:
    static void initLogger();
    static int getSyslogLevel(LOG_LEVEL lvl);
};

} // namespace CALF

#endif // LOGGER_HH_INCLUDED
