

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OBJECTSERIALISATION_HH_INCLUDED
#define OBJECTSERIALISATION_HH_INCLUDED

#include <memory>
#include <vector>

#include <cstdint>

namespace CALF
{
class Circuit;
class Request;

using Bytes = std::vector<uint8_t>;

class Archive
{
    class Impl;
    std::unique_ptr<Impl> impl_;

    friend struct ObjectSerialisation;

    public:
    enum class OBJECT { NONE, REQUEST, CIRCUIT, MAIN_VENT, USBENVIRONMENT };

    Archive();

    ~Archive();

    void clear();

    static bool importable(const Bytes &external_buffer) noexcept;
    void import(Bytes &external_buffer);
    void importByFD(int fd);

    Bytes exportBytes() noexcept;

    // gebe Objekt Typ des nächsten Elements an
    OBJECT extractable() noexcept;

    int exportToFD(int fd);
};

struct ObjectSerialisation {
    using Request_Ptr = std::unique_ptr<Request>;

    ObjectSerialisation() = delete;

    static int serialise(const Request_Ptr &r, Archive &ar) noexcept;
    static Request_Ptr constructRequest(Archive &ar);

    static int serialise(const Circuit &c, Archive &ar) noexcept;

    enum class STATIC { USBENVIRONMENT, RTDCALCULATOR };
    static int serialise(STATIC s, Archive &ar) noexcept;
};

} // namespace CALF

#endif // OBJECTSERIALISATION_HH_INCLUDED
