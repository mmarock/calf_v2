

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REQUESTS_HH_INCLUDED
#define REQUESTS_HH_INCLUDED

#include <memory>

namespace Json
{
class Value;
};

namespace CALF
{
/**
 * @brief Abstract base class of every Server Request
 *
 * Every Request which is sent to the server originates from
 * this class.
 */
class Request
{
    public:
    using Ptr = std::unique_ptr<Request>;

    enum class TYPE { ECHO, QUIT, NETDUMP, ENABLE, SYSTEM };

    Request();
    virtual ~Request() = 0;

    virtual TYPE type() const noexcept = 0;

    virtual Json::Value toJson() const;

    static Request::Ptr fromJson(const Json::Value &val);

    static const char *type(TYPE t) noexcept;
};

/**
 * @brief
 */
class EchoRequest : public Request
{
    std::string msg_;

    public:
    EchoRequest(std::string msg);
    ~EchoRequest();

    const std::string &msg() const noexcept;

    TYPE type() const noexcept override;
    Json::Value toJson() const override;
};

class QuitRequest : public Request
{
    public:
    QuitRequest();
    ~QuitRequest();
    TYPE type() const noexcept override;
    Json::Value toJson() const override;
};

class NetDumpRequest : public Request
{
    public:
    NetDumpRequest(bool circuit, bool scheduler, bool main_vents,
		   bool detectors, bool valve_vents, bool temperatures);

    ~NetDumpRequest();

    bool circuit() const noexcept;
    bool scheduler() const noexcept;
    bool main_vents() const noexcept;
    bool detectors() const noexcept;
    bool valve_vents() const noexcept;
    bool temperatures() const noexcept;

    TYPE type() const noexcept override;
    Json::Value toJson() const override;

    private:
    bool circuit_;
    bool scheduler_;
    bool main_vents_;
    bool detectors_;
    bool valve_vents_;
    bool temperatures_;
};

class DetectorEnableRequest : public Request
{
    int detector_id_;
    bool enable_;

    public:
    DetectorEnableRequest(int detector_id, bool enable);
    ~DetectorEnableRequest();

    int detector() const noexcept;
    bool enable() const noexcept;

    TYPE type() const noexcept override;
    Json::Value toJson() const override;
};

class SystemRequest : public Request
{
    public:
    enum class CMD { DUMP_TO_SYSLOG, RELOAD, STOP };

    private:
    CMD cmd_;

    public:
    explicit SystemRequest(CMD cmd);

    CMD cmd() const noexcept;

    TYPE type() const noexcept override;
    Json::Value toJson() const override;

    static const char *cmd(CMD c) noexcept;
};

} // namespace CALF

#endif // REQUESTS_HH_INCLUDED
