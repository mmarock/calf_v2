

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef THREADEDCONNECTION_HH_INCLUDED
#define THREADEDCONNECTION_HH_INCLUDED

#include <memory>

namespace CALF
{
class Circuit;
class Scheduler;

class ThreadedConnection
{
    class Impl;
    std::unique_ptr<Impl> impl_;

    public:
    ThreadedConnection(int fd, Circuit &circuit, Scheduler &scheduler);
    ~ThreadedConnection();

    ThreadedConnection(const ThreadedConnection &rhs) = delete;
    ThreadedConnection &operator=(const ThreadedConnection &rhs) = delete;

    ThreadedConnection(ThreadedConnection &&rhs);
    ThreadedConnection &operator=(ThreadedConnection &&rhs);

    static bool globalStopRequested() noexcept;

    // frage ob die Verbindung geschlossen werden kann,
    // nicht nötig bei einem Fehler oä
    bool closeRequested() const noexcept;
    int fd() const noexcept;
    int readFromFd(); // read new data
    void closeFd();   // close the socket, do this to ensure correct behaviour
};

} // namespace CALF

#endif // THREADEDCONNECTION_HH_INCLUDED
