
/**
    This file is part of Calf.

    Calf is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Calf is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

    @File Scheduler.hh
    @Brief Provide a scheduler for detectors, arranged in FillGroups.

    @See FillGroup.hh
    @See FillTime.hh
*/

#ifndef SCHEDULER_HH_INCLUDED
#define SCHEDULER_HH_INCLUDED

#include <chrono>
#include <memory>
#include <vector>

namespace Json
{
class Value;
}

namespace CALF
{
class SchedulerError : public std::exception
{
    std::string msg_;

    public:
    explicit SchedulerError(const char *msg);
    explicit SchedulerError(const std::string &msg);

    const char *what() const noexcept;
};

class Circuit;
class FillGroup;

/**
 * @Synopsis Do planned fillings
 *
 *  Scheduler manages its FillGroups by spawning a thread.
 *
 *  This thread is designed to sleep until the next FillTime.
 *  Then the thread wakes up and it is checked if a timeout happened.
 *  If true, the Circuit is advised to fill the FillGroup.
 *  Else the wakeup is done by a notification which hints of a configuration
 *  change.
 *
 *  Then the next sleep-time is calculated. If the Scheduler is paused
 *  or there is no FillGroup added, the thread sleeps till the next user
 *  notification (config change or resume() call).

 */
class Scheduler
{
    class Impl;
    std::unique_ptr<Impl> impl_;

    public:
    using minutes = std::chrono::minutes;

    explicit Scheduler(int circuit_id);
    ~Scheduler();

    Scheduler(const Scheduler &) = delete;
    Scheduler &operator=(const Scheduler &) = delete;

    Scheduler(Scheduler &&);
    Scheduler &operator=(Scheduler &&);

    bool haveGroup(int group_id) const noexcept;
    int addGroup();
    void rmGroup(int id);
    const std::vector<FillGroup> &groups() const noexcept;

    void addFillTime(int grp, int hour, int minute);

    void addDetector(int grp, int detector, minutes timeout);
    void updateDetector(int detector, minutes timeout);
    void rmDetector(int detector);	  // rm from all grps
    void rmDetector(int grp, int detector); // rm from one grp

    bool cancelFilling() noexcept;
    void fillGroup(Circuit &c, int grp);

    struct NextGroupData {
	int group_id;
	int timeout_in_milliseconds;
    };
    struct NextGroupData next() const noexcept;

    void reload();
    void clear() noexcept;
    void dump() noexcept;

    static Scheduler fromJson(const Json::Value &val);
    Json::Value toJson() const;

    static Scheduler fromConfigFile(int circuit_id);
    void toConfigFile() const;
};
}

#endif // SCHEDULER_HH_INCLUDED
