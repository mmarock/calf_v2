

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ROUNDROBINARCHIVE_HH_INCLUDED
#define ROUNDROBINARCHIVE_HH_INCLUDED

#include <ctime>
#include <string>

namespace CALF
{
class RoundRobinArchive
{
    std::string file_path_;

    public:
    RoundRobinArchive(int detector_id, const std::string &file_path);

    RoundRobinArchive(RoundRobinArchive &&rhs);
    RoundRobinArchive &operator=(RoundRobinArchive &&rhs);

    void updateInlet(double temp);
    void updateOutlet(double temp);
    void updateBoth(double inlet_temp, double outlet_temp);

    void timedUpdateBoth(time_t t, double inlet_temp, double outlet_temp);

    private:
    void create();
};
} // namespace CALF

#endif // ROUNDROBINARCHIVE_HH_INCLUDED
