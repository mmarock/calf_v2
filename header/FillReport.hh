

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FILLREPORT_HH_INCLUDED
#define FILLREPORT_HH_INCLUDED

#include <ctime>

#include <mutex>
#include <string>
#include <vector>

namespace CALF
{
enum class EVENT {

    VENTILATION_BEGIN,
    VENTILATION_TIMEOUT,

    DETECTOR_BEGIN,
    DETECTOR_INACTIVE,
    DETECTOR_WARMUP,
    DETECTOR_TIMEOUT,

    GROUP_BEGIN,
    GROUP_SUCESSFULL,
    GROUP_FAILED,
    GROUP_NO_ACTIVE_DETECTOR,

    USER_FILL_CANCEL,

    USER_REQUESTED_FORCE_FILL,
    UNKNOWN
};

struct FillReportLine {
    int id;
    time_t time;
    EVENT event;

    FillReportLine(int id, time_t time, EVENT event);

    std::string getString() const;
    bool isPositive() const;
};

class FillReport
{
    mutable std::mutex mtx_;
    std::vector<FillReportLine> lines_;

    public:
    void addLine(int id, EVENT e);
    void clear();

    std::string createLongFillReport();

    std::string createShortFillReport() const;

    private:
    bool isPositive() const;
    const FillReportLine &getGroupBeginLine() const;
    const FillReportLine &getFirstNegativeLine() const;
    std::string getSubject() const;
};

} // namespace CALF

#endif // FILLREPORT_HH_INCLUDED
