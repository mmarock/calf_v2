

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PIDLOCK_HH_INCLUDED
#define PIDLOCK_HH_INCLUDED

/**
 * @brief Aqquire an unique file in /tmp
 *
 * After you called PIDLock::aqquire() and it returned
 * true, it is guaranteed that there is only one calfd running!
 *
 */
class PIDLock
{
    class Impl;
    static Impl &getImpl() noexcept;

    public:
    PIDLock() = delete;

    static bool aqquire() noexcept;
    static void release() noexcept;
};

#endif // PIDLOCK_HH_INCLUDED
