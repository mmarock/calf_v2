

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEMPERATURE_HH_INCLUDED
#define TEMPERATURE_HH_INCLUDED

#include <string>

namespace CALF
{
class Temperature
{
    double value_;

    public:
    Temperature() : value_(0.0) {}
    explicit Temperature(const double value) : value_(value) {}
    Temperature(Temperature &&rhs) : value_(std::move(rhs.value_))
    {
	rhs.value_ = 0.0;
    }

    Temperature &operator=(Temperature &&rhs)
    {
	value_ = std::move(rhs.value_);
	rhs.value_ = 0.0;
	return *this;
    }

    Temperature(const Temperature &rhs) : value_(rhs.value_) {}
    Temperature &operator=(const Temperature &rhs)
    {
	value_ = rhs.value_;
	return *this;
    }

    double inKelvin() const { return value_; }
    double inCelsius() const { return (value_ - 273.15); }
    double inFahrenheit() const { return ((value_ * 1.8) - 459.67); }
    static Temperature fromCelsius(const double value)
    {
	return Temperature(value + 273.15);
    }

    static Temperature fromFahrenheit(const double value)
    {
	return Temperature((value + 459.67) * 5.0 / 9.0);
    }
};

inline bool operator<(const Temperature &lhs, const Temperature &rhs)
{
    return (lhs.inKelvin() < rhs.inKelvin());
}

inline bool operator>(const Temperature &lhs, const Temperature &rhs)
{
    return (lhs.inKelvin() > rhs.inKelvin());
}

} // namespace CALF

#endif // TEMPERATURE_HH_INCLUDED
