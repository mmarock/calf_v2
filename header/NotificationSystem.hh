

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NOTIFICATIONSYSTEM_HH_INCLUDED
#define NOTIFICATIONSYSTEM_HH_INCLUDED

#include <stdexcept>
#include <string>
#include <vector>

// TODO: remove all Json!
namespace Json
{
class Value;
}

namespace CALF
{
class NotificationSystemError : public std::exception
{
    std::string msg_;

    public:
    explicit NotificationSystemError(const char *msg);
    explicit NotificationSystemError(const std::string &msg);

    const char *what() const noexcept;
};

class FillReport;

// TODO: This system shall not be bound to one object, make it static
class NotificationSystem
{
    public:
    typedef std::pair<std::string, bool> Recipient;
    typedef std::vector<Recipient>::iterator iterator;
    typedef std::vector<Recipient>::const_iterator const_iterator;
    typedef std::vector<Recipient>::reverse_iterator reverse_iterator;
    typedef std::vector<Recipient>::const_reverse_iterator
	const_reverse_iterator;

    private:
    // TODO make this static!!!!
    // class Impl;
    // static const Impl &getImpl();

    std::string server_;
    std::string reverse_path_;
    std::vector<Recipient> recipients_;

    public:
    static NotificationSystem fromConfigFile();

    explicit NotificationSystem(
	const std::string &server, std::vector<Recipient> &&recipients,
	const std::string &reverse_path = getDefaultReversePath());

    void addRecipient(const std::string &mail, bool verbose_msg = true);
    void removeRecipient(const std::string &mail);

    iterator begin();
    iterator end();

    const_iterator cbegin() const;
    const_iterator cend() const;

    reverse_iterator rbegin();
    reverse_iterator rend();

    const_reverse_iterator crbegin() const;
    const_reverse_iterator crend() const;

    bool sendNotifications(FillReport &notification);

    void reloadConfigFile();

    static NotificationSystem fromJson(const Json::Value &val);
    // Json::Value reload(const Json::Value &val);
    Json::Value toJson() const;
    void reload(const Json::Value &val);
    static const char *getDefaultConfigPath();

    private:
    static const std::string &getDefaultReversePath();
    bool needsVerboseEMail() const;
    bool needsNonVerboseEMail() const;
    void sendEMail(const std::string &msg, bool send_verbose) const;
};

} // namespace CALF

#endif // NOTIFICATIONSYSTEM_HH_INCLUDED
