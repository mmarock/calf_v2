

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VALVEVENT_HH_INCLUDED
#define VALVEVENT_HH_INCLUDED

#include "Temperature.hh"

#include <chrono>

namespace Json
{
class Value;
}

namespace CALF
{
class ValveVentError : public std::exception
{
    std::string msg_;

    public:
    explicit ValveVentError(const std::string &msg);

    const char *what() const noexcept override;
};

/**
 * @brief Equals a Valve Vent with an optional sensor and an allowed time range
 */
class ValveVent
{
    using seconds = std::chrono::seconds;
    typedef std::pair<seconds, seconds> duration_range;

    int id_;

    int vent_usb_;
    uint8_t vent_channel_;

    int sensor_usb_;
    uint8_t sensor_channel_;
    Temperature trigger_temp_;
    duration_range allowed_time_duration_;

    public:
    ValveVent(int id, int vent_usb, uint8_t vent_channel);

    int id() const noexcept;

    int getVentUSB() const noexcept;
    uint8_t getVentChannel() const noexcept;

    void setSensor(int usb, uint8_t ch, const Temperature &T) noexcept;
    bool haveSensor() const noexcept;
    int getSensorUSB() const noexcept;
    uint8_t getSensorChannel() const noexcept;

    const Temperature &getTrigger() const noexcept;
    void setTrigger(const Temperature &T) noexcept;

    const duration_range getAllowedTimeDuration() const noexcept;
    void setAllowedTimeDuration(seconds min, seconds max) noexcept;

    Json::Value toJson() const;
    static ValveVent fromJson(const Json::Value &val);
};

} // namespace CALF;

#endif // VALVEVENT_HH_INCLUDED
