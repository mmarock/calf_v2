#ifndef CALF_USBTEMP_HH
#define CALF_USBTEMP_HH

#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

struct hiddev_devinfo;

namespace Json
{
class Value;
}

namespace CALF
{
class USBTempError : public std::runtime_error
{
    public:
    explicit USBTempError(const std::string &msg) : std::runtime_error(msg) {}
    explicit USBTempError(const char *msg) : std::runtime_error(msg) {}
};

struct USBTempInfo {
    std::string device;
    std::unique_ptr<hiddev_devinfo> info;
};

USBTempInfo makeUSBTempInfo(std::string device, const hiddev_devinfo *info);

const size_t USBTEMP_WRITE_MEMORY_MAX = 0xFF;
const uint32_t USBTEMP_FLASH_START = 0x00000000;
const uint32_t USBTEMP_FLASH_END = 0x000075FF;
const uint32_t USBTEMP_ID_MEMORY_START = 0x00200000;
const uint32_t USBTEMP_ID_MEMORY_END = 0x00200007;
const uint32_t USBTEMP_CONFIG_MEMORY_START = 0x00300000;
const uint32_t USBTEMP_CONFIG_MEMORY_END = 0x0030000F;
const uint32_t USBTEMP_EEPROM_START = 0x00F00000;
const uint32_t USBTEMP_EEPROM_END = 0x00F03FFF;

class USBTemp
{
    public:
    enum class DIO_DIRECTION : uint8_t { OUT, IN };

    enum CHANNEL : uint8_t {
	CHANNEL_0,
	CHANNEL_1,
	CHANNEL_2,
	CHANNEL_3,
	CHANNEL_4,
	CHANNEL_5,
	CHANNEL_6,
	CHANNEL_7,
    };

    enum ADC : uint8_t { ADC_0, ADC_1, ADC_2, ADC_3 };

    enum class SENSOR_TYPE : uint8_t {
	RTD,
	THERMISTOR,
	THERMOCOUPLE,
	SEMICONDUCTOR,
	DISABLED
    };

    enum class THERMOCOUPLE_TYPE : uint8_t { J, K, T, E, R, S, B, N };

    enum class SEMICONDUCTOR_TYPE : uint8_t { SINGLE_ENDED, DIFFERENTIAL };

    enum class CONNECTION : uint8_t {
	TWO_WIRE_ONE_SENSOR,
	TWO_WIRE_TWO_SENSOR,
	THREE_WIRE,
	FOUR_WIRE
    };

    enum class FILTER_FREQUENCY : uint8_t {
	HZ_500 = 1,
	HZ_250,
	HZ_125,
	HZ_62_5,
	HZ_50,
	HZ_39_2,
	HZ_33_3,
	HZ_19_6,
	HZ_16_7,
	// Unknown filter frequency value for 0x0a,
	HZ_12_5 = 0x0b,
	HZ_10,
	HZ_8_33,
	HZ_6_25,
	HZ_4_17
    };

    enum class GAIN : uint8_t { X1, X2, X4, X8, X16, X32, X64, X128 };

    enum class EXCITATION : uint8_t {
	OFF,
	UA_10,
	UA_210,
    };

    enum class UNIT : uint8_t { CALIBRATED, RAW };

    enum class MEMORY : uint8_t { MAIN_UC = 0, ISOLATED_UC };

    private:
    class Impl;
    std::unique_ptr<Impl> impl_;

    public:
    explicit USBTemp(std::string devname);
    ~USBTemp();

    USBTemp(USBTemp &&rhs);
    USBTemp &operator=(USBTemp &&rhs);

    USBTemp(const USBTemp &) = delete;
    USBTemp &operator=(const USBTemp &) = delete;

    static std::vector<USBTempInfo> findDevices();

    const std::string &getDevice() const;
    const std::string &getSerialNumber() const;

    void dioConfig(DIO_DIRECTION direction);
    uint8_t dioRead();
    void dioSet(const uint8_t value);

    void dioBitConfig(const uint8_t bit, DIO_DIRECTION direction);
    bool dioBitRead(const uint8_t bit);
    void dioBitSet(const uint8_t bit, const bool value);

    SENSOR_TYPE getSensorType(CHANNEL channel);
    void setSensorType(CHANNEL channel, SENSOR_TYPE type);

    enum THERMOCOUPLE_TYPE getThermocoupleType(CHANNEL channel);
    void setThermocoupleType(CHANNEL channel, THERMOCOUPLE_TYPE type);

    CONNECTION getSensorConnection(CHANNEL channel);
    void setSensorConnection(CHANNEL channel, CONNECTION connection);

    enum SEMICONDUCTOR_TYPE getSemiconductorType(CHANNEL channel);
    void setSemiconductorType(CHANNEL channel, SEMICONDUCTOR_TYPE type);

    GAIN getGain(CHANNEL channel);
    void setGain(CHANNEL channel, GAIN gain);

    FILTER_FREQUENCY getFilterFrequency(CHANNEL channel);
    void setFilterFrequency(CHANNEL channel, FILTER_FREQUENCY frequency);

    EXCITATION getExcitation(CHANNEL channel);
    void setExcitation(CHANNEL channel, EXCITATION exitation);

    float getVRef(CHANNEL channel);
    void setVRef(CHANNEL channel, const float value);

    float getI_10uA(CHANNEL channel);
    void setI_10uA(CHANNEL channel, const float value);

    float getI_210uA(CHANNEL channel);
    void setI_210uA(CHANNEL channel, const float value);

    float getI_10uA_3Wire(CHANNEL channel);
    void setI_10uA_3Wire(CHANNEL channel, const float value);

    float getV_10uA(CHANNEL channel);
    void setV_10uA(CHANNEL channel, const float value);

    float getV_210uA(CHANNEL channel);
    void setV_210uA(CHANNEL channel, const float value);

    float getV_10uA_3Wire(CHANNEL channel);
    void setV_10uA_3Wire(CHANNEL channel, const float value);

    void getCoefficients(CHANNEL channel, float *coeffs);
    void setCoefficients(CHANNEL channel, const float *coeffs);

    float getTemperature(CHANNEL channel, UNIT unit);
    void scanTemperatures(CHANNEL first, CHANNEL last, UNIT unit,
			  float *values);

    size_t readMemory(MEMORY mem, const uint16_t base, void *data,
		      const size_t n);
    size_t writeMemory(MEMORY type, const uint16_t base, const void *data,
		       const size_t n);

    void blinkLED();

    void resetUSB();

    uint8_t getStatus();

    void calibrate();

    uint8_t getBurnoutStatus();

    void prepareDownload(MEMORY mem);

    void writeCode(uint32_t base, void *data, size_t n);
    void readCode(uint32_t base, void *data, size_t n);
    void writeSerial(char *data, size_t n);
};

} // namespace CALF

#endif /* CALF_USBTEMP_HH */
