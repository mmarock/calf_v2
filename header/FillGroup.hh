

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FILLGROUP_HH_INCLUDED
#define FILLGROUP_HH_INCLUDED

#include <chrono>
#include <string>
#include <vector>

namespace Json
{
class Value;
}

namespace CALF
{
class FillGroupError : public std::exception
{
    std::string msg_;

    public:
    explicit FillGroupError(const char *msg);
    explicit FillGroupError(const std::string &msg);

    const char *what() const noexcept;
};

struct FillTime;

/**
 * @Synopsis A group of detectors
 *
 * @Todo time_frame_ only grows, change that behaviour
 */
class FillGroup
{
    int id_;

    std::chrono::minutes time_frame_;

    std::vector<int> detectors_;
    std::vector<FillTime> fill_times_;

    public:
    explicit FillGroup(int id);

    FillGroup(const FillGroup &grp);
    FillGroup &operator=(const FillGroup &grp);

    // throws InvalidFillTime, OverlappingFillTime
    void addFillTime(int hour, int minute);

    // throws InvalidFillTime
    void removeFillTime(int hour, int minute);

    std::chrono::minutes nextOffset() const noexcept;

    bool haveDetector(int detector) const noexcept;
    // both member throw DetectorNotFound
    void addDetector(int detector, const std::chrono::minutes &time_frame);
    void removeDetector(int detector);

    // getter
    int getID() const noexcept;
    const std::vector<int> &getDetectors() const noexcept;
    const std::vector<FillTime> &getFillTimes() const noexcept;

    // cmp member, checks overlapping
    bool interferesWith(const FillGroup &rhs) const noexcept;

    Json::Value toJson() const;
    static FillGroup fromJson(const Json::Value &val);

    private:
    static bool overlaps(const FillTime &ft1, const FillTime &ft2,
			 const std::chrono::minutes &time_frame);
};

} // namespace CALF

#endif // FILLGROUP_HH_INCLUDED
