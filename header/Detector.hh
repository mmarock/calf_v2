

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DETECTOR_HH_INCLUDED
#define DETECTOR_HH_INCLUDED

#include "Temperature.hh"

#include <chrono>

namespace Json
{
class Value;
}

namespace CALF
{
class DetectorError : public std::exception
{
    std::string msg_;

    public:
    explicit DetectorError(const std::string &msg);

    const char *what() const noexcept override;
};

class Detector
{
    using seconds = std::chrono::seconds;

    private:
    int id_;

    int vent_usb_;
    uint8_t vent_channel_;

    int inlet_usb_;
    uint8_t inlet_ch_;
    Temperature inlet_trigger_temp_;

    int outlet_usb_;
    uint8_t outlet_ch_;
    Temperature outlet_trigger_temp_;

    bool is_active_;
    std::pair<seconds, seconds> allowed_time_duration_;
    std::chrono::time_point<std::chrono::system_clock> last_changed_;

    public:
    explicit Detector(int id, int vent_usb, uint8_t vent_channel);

    int id() const noexcept;

    int getVentUSB() const;
    uint8_t getVentChannel() const;

    void setInlet(int usb, uint8_t ch, const Temperature &t) noexcept;
    bool hasInlet() const;
    int getInletUSB() const;
    uint8_t getInletChannel() const;

    void setOutlet(int usb, uint8_t ch, const Temperature &t) noexcept;
    bool hasOutlet() const;
    int getOutletUSB() const;
    uint8_t getOutletChannel() const;

    const Temperature &getInletTrigger() const;
    void setInletTrigger(const Temperature &T);

    const Temperature &getOutletTrigger() const;
    void setOutletTrigger(const Temperature &T);

    bool isActive() const;
    bool setActive(bool is_active);

    const std::pair<seconds, seconds> getAllowedTimeDuration() const;
    void setAllowedTimeDuration(seconds min, seconds max);

    std::chrono::time_point<std::chrono::system_clock> touch();

    Json::Value toJson() const;
    static Detector fromJson(const Json::Value &val);
};

} // nmaespace CALF

#endif // DETECTOR_HH_INCLUDED
