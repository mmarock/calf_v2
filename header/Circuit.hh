

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CIRCUIT_HH_INCLUDED
#define CIRCUIT_HH_INCLUDED

#include <chrono>
#include <memory>
#include <vector>

#include "Temperature.hh"

namespace Json
{
class Value;
}

namespace CALF
{
class Detector;
class FillGroup;
class MainVent;
class NotificationSystem;
class ValveVent;

class CircuitError : public std::exception
{
    std::string msg_;

    public:
    explicit CircuitError(const char *msg);
    explicit CircuitError(const std::string &msg);

    const char *what() const noexcept;
};

/**
 * @brief  Circuit class equals calf workspace
 * @todo NotificationSystem ist nutzlos innerhalb Circuit,
 * 		ein System für alles ist logischer
 *
 */
class Circuit
{
    class Impl;
    std::unique_ptr<Impl> impl_;

    using seconds = std::chrono::seconds;
    typedef std::pair<seconds, seconds> duration_range;

    public:
    explicit Circuit(int id);
    ~Circuit();

    Circuit(const Circuit &) = delete;
    Circuit &operator=(const Circuit &) = delete;

    Circuit(Circuit &&rhs);
    Circuit &operator=(Circuit &&rhs);

    static Circuit fromConfigFile(int id);
    Json::Value toJson() const;

    void reload();
    void clear();
    void dump();

    int id() const noexcept;
    const std::vector<MainVent> &getMainVents() const noexcept;
    const std::vector<ValveVent> &getValveVents() const noexcept;
    const std::vector<Detector> &getDetectors() const noexcept;
    const NotificationSystem &getNotificationSystem() const noexcept;

    int pairMainVent(int vent_usb, uint8_t vent_ch);
    void splitMainVent(int mvent_id);

    int pairValveVent(int vent_usb, uint8_t vent_ch);
    void addSensor(int vvent_id, int sensor_usb, uint8_t sensor_ch);
    void splitValveVent(int vvent_id);

    int pairDetector(int vent_usb, uint8_t vent_ch);
    void addInlet(int detector, int sensor_usb, uint8_t sensor_ch);
    void addOutlet(int detector, int sensor_usb, uint8_t sensor_ch);
    void splitDetector(int d_id);

    void setDetectorActivity(int detector_id, bool is_active);
    Temperature getDetectorInletTemperature(int detector_id);
    Temperature getDetectorOutletTemperature(int detector_id);
    bool fill(const volatile bool &cancel, const FillGroup &grp);

    using minutes = std::chrono::minutes;
    minutes fillTimeout(const std::vector<int> &ids) const;
    minutes fillTimeout(int detector_id) const;

    static const char *circuit_config_path() noexcept;
};

} // namespace CALF

#endif /* CIRCUIT_HH_INCLUDED */
