
<!doctype html>
<html lang="de">
	<?php error_reporting(E_STRICT); ?>
	<head>
		<meta charset="utf8"/>
		<title>CalfServer Control</title>
                <link rel="stylesheet" href="public/css/style.css"/>

	<body>
	<header>
		<h1>CalfServer Control</h1>
	</header>
	<div id="Kontrolle">
		<nav>
			<ul>
				<li><a href="#">Raspberry</a></li>
				<li><a href="#">Configuration</a></li>
				<li><a href="#">TemperatureMonitor</a></li>
				<li><a href="#">Enable/Disable Cryostats</a></li>
			</ul>
		</nav>
	</div>
	<section id="content">
		<p><big>Welcome to CalfServer Control Service</big></p>
		<p>
		<table style:"100%">
			<tr>
				<th>Process</th>
				<th>Return Value</th>
			</tr>
			<tr>
				<th>uptime</th>
				<th><?php echo exec('uptime -p');?></th>
			</tr>
			<tr>
				<th>date</th>
				<th><?php echo exec('date');?></th>
			</tr>
		</table>
		</p>
		<p>
		<?php
			include 'CircuitCommunicator.php';

			try {

			$comm = new CircuitCommunicator(1);

			$c = $comm->getConfiguration();

			echo $c;

			} catch (Exception $ex) {
				syslog(LOG_ALERT, $ex->getMessage());
			}
		?>
		</p>
		<!--
			/*
			include 'CircuitCommunicator.php';
			include 'StatusViewer.php';
			
			// aus der url
			$id = 1;
			$verbosity = 0;


			try {

				$comm = new CircuitCommunicator($id);

				$config = new array(comm.getConfiguration());

				foreach (config["cryostats"] as $cryostat) {
					// lade Informationen aus der Config sowie das rrd file, falls der Kryostat aktiv ist
					StatusViewer viewer(cryostat["id"]);
					viewer.print($cryostat, $verbosity);
				
			}

			} catch (StatusViewerError ex) {

			} catch (CircuitCommunicatorError ex) {
			
			} catch (Exception ex) {
				
			}
			 */
		-->
	</section>
	</body>

</html>
