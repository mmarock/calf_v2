<?php

class CircuitCommunicatorError extends Exception {
	
	function __construct(string $msg) {
		Exception::__construct("CircuitCommunicatorError: $msg");
	}
}

class CircuitCommunicator {

	public static $SERVER_SOCKET = null;

	private $json_buffer_ = null;	

	private $socket_ = null;
	function __construct() {

		if (!$SERVER_SOCKET) {
			$SERVER_SOCKET = "unix:///tmp/calf.socket";
		}

		$socket_ = fsockopen($SERVER_SOCKET, -1, $errno, $errstr, 30);
		if (!$socket_) {
			// log message
			syslog(LOG_ALERT, $errstr);
			throw new CircuitCommunicatorError($errstr);
		}
		echo "$errstr ($errno)";
		syslog(LOG_INFO, "CircuitCommunicator: connected to /tmp/calf.socket");
	}

	function __destruct() {
		if ($socket_) {
			fclose($socket_);
			$socket_ = null;
		}
	}

	function getConfiguration() {
		syslog(LOG_INFO, "CircuitCommunicator: getConfiguration() called");

		$payload = '{ request_type : "ECHO", echo_message: "Hallo Welt"}\x04';

		echo $payload;
		$len = fwrite($socket_, $payload);
		echo "Len=$len";
		return fread($socket_, 128);

		// create request
		// send, wait for response
		// wait till \x04
		// parse json data and return the associative array
	}

	function enableCryostat(int $id) {
		syslog(LOG_INFO, "CircuitCommunicator: enableCryostat(id:$id) called");
		// create request
		// send, wait for response
	}

	function disableCryostat(int $id) {
		syslog(LOG_INFO, "CircuitCommunicator: disableCryostat(id:$id) called");
	}

}

?>
