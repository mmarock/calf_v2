

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../header/RoundRobinArchive.hh"

#include <cstdio>

#include <stdexcept>

#include <sys/stat.h>

#include <rrd.h>

namespace CALF
{
bool exists(const std::string &path_to_file)
{
    struct stat buf;
    return (!stat(path_to_file.c_str(), &buf));
}

RoundRobinArchive::RoundRobinArchive(int id, const std::string &file_path)
    : file_path_(file_path + "/detector_" + std::to_string(id) + ".rrd")
{
    if (!exists(file_path_)) {
	create();
    }
}

RoundRobinArchive::RoundRobinArchive(RoundRobinArchive &&rhs)
    : file_path_(std::move(rhs.file_path_))
{
}

void RoundRobinArchive::create()
{
    const char *args[] = {"DS:inlet:GAUGE:120:-273.15:50",
			  "DS:outlet:GAUGE:120:-273.15:50",
			  "RRA:MIN:0.5:1:2880",
			  "RRA:MAX:0.5:1:2880",
			  "RRA:AVERAGE:0.5:30:2880",
			  "RRA:MIN:0.5:30:672",
			  "RRA:MAX:0.5:30:672",
			  "RRA:AVERAGE:0.5:30:672",
			  "RRA:MIN:0.5:120:768",
			  "RRA:MAX:0.5:120:768",
			  "RRA:AVERAGE:0.5:120:768",
			  "RRA:MIN:0.5:1440:731",
			  "RRA:MAX:0.5:1440:731",
			  "RRA:AVERAGE:0.5:1440:731"};
    constexpr size_t args_size = sizeof(args) / sizeof(args[0]);

    int rc =
	rrd_create_r(file_path_.c_str(), 330, time(NULL) - 10, args_size, args);
    if (rc == -1) {
	throw std::runtime_error(std::string("RoundRobinArchive::create(): ") +
				 rrd_get_error());
    }
}

void RoundRobinArchive::updateInlet(double temp)
{
    char update_str[128];
    snprintf(update_str, 128, "N:%f:U", temp);
    const char *list[] = {update_str, NULL};
    int rc = rrd_update_r(file_path_.c_str(), NULL, 1, list);
    if (rc == -1) {
	throw std::runtime_error(
	    std::string("RoundRobinArchive::updateInlet(): ") +
	    rrd_get_error());
    }
}

void RoundRobinArchive::updateOutlet(double temp)
{
    char update_str[128];
    snprintf(update_str, 128, "N:U:%f", temp);
    const char *list[] = {update_str};
    int rc = rrd_update_r(file_path_.c_str(), NULL, 1, list);
    if (rc == -1) {
	throw std::runtime_error(
	    std::string("RoundRobinArchive::updateOutlet(): ") +
	    rrd_get_error());
    }
}

void RoundRobinArchive::updateBoth(double inlet_temp, double outlet_temp)
{
    char update_str[128];
    snprintf(update_str, 128, "N:%f:%f", inlet_temp, outlet_temp);
    const char *list[] = {update_str};
    int rc = rrd_update_r(file_path_.c_str(), NULL, 1, list);
    if (rc == -1) {
	throw std::runtime_error(
	    std::string("RoundRobinArchive::updateBoth(): ") + rrd_get_error());
    }
}

void RoundRobinArchive::timedUpdateBoth(time_t t, double inlet_temp,
					double outlet_temp)
{
    char update_str[128];
    snprintf(update_str, 128, "%ld:%f:%f", t, inlet_temp, outlet_temp);
    const char *list[] = {update_str};
    int rc = rrd_update_r(file_path_.c_str(), NULL, 1, list);
    if (rc == -1) {
	throw std::runtime_error(
	    std::string("RoundRobinArchive::updateBoth(): ") + rrd_get_error());
    }
}

} // namespace CALF
