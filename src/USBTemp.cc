#include "../header/USBTemp.hh"

#include <algorithm>
#include <iomanip>

#include <cassert>
#include <cerrno>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <ctime>

#include <fcntl.h>
#include <linux/hiddev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>

#include <libusb.h>

namespace CALF
{
const uint16_t HID_DEV_MAX = 16;

////////////////////////////////////////////////////////////////////////////////
// USBTempInfo                                                                //
////////////////////////////////////////////////////////////////////////////////

USBTempInfo makeUSBTempInfo(std::string device,
			    const struct hiddev_devinfo *info)
{
    assert(info);

    USBTempInfo dev;
    dev.device = std::move(device);
    dev.info = std::unique_ptr<hiddev_devinfo>(new hiddev_devinfo);
    memcpy(dev.info.get(), info, sizeof(hiddev_devinfo));
    return dev;
}

////////////////////////////////////////////////////////////////////////////////
// USBTempImpl                                                                //
////////////////////////////////////////////////////////////////////////////////

class USBTemp::Impl
{
    static const uint16_t VENDOR_ID;
    static const uint16_t PRODUCT_ID;
    static const uint8_t UNLOCK_CODE;
    static const uint32_t USAGE_CODE;
    static const size_t MEMORY_BYTES_PER_OP;

    enum class COMMAND : uint8_t {
	DPORT_CONFIG = 0x01,     // Configure digital port
	DPORT_CONFIG_BIT = 0x02, // Configure digital port bits
	READ_DPORT = 0x03,       // Read digital port
	SET_DPORT = 0x04,	// Write digital port
	READ_DPORT_BIT = 0x05,   // Read digital port bit
	SET_DPORT_BIT = 0x06,    // Write digital port bit
	READ_TEMP = 0x18,	// Read input channel
	SCAN_TEMP = 0x19,	// Read multiple input channels
	MEMORY_READ = 0x30,      // Read Memory
	MEMORY_WRITE = 0x31,     // Write Memory
	BLINK_LED = 0x40,	// Causes LED to blink
	RESET_USB = 0x41,	// Reset USB interface
	GET_STATUS = 0x44,       // Get device status
	SET_CONFIG_ITEM = 0x49,  // Set a configuration item
	GET_CONFIG_ITEM = 0x4A,  // Get a configuration item
	CALIBRATE = 0x4B,	// Perform a channel calibration
	BURNOUT_STATUS = 0x4C,   // Get thermocouple burnout detection
	// status
	PREPARE_DOWNLOAD = 0x50, // Prepare for program memory download
	WRITE_CODE = 0x51,       // Write program memory
	WRITE_SERIAL = 0x53,     // Write a new serial number to device
	READ_CODE = 0x55	 // Read program memory
    };

    enum class ITEM : uint8_t {
	SENSOR_TYPE = 0,
	CONNECTION_TYPE,
	FILTER_RATE,
	EXCITATION,
	VREF,
	I_10_UA,
	I_210_UA,
	I_10_UA_3WIRE,
	V_10_UA,
	V_210_UA,
	V_10_UA_3WIRE,
	CH0_TC_TYPE,
	CH1_TC_TYPE,
	CH0_GAIN,
	CH1_GAIN,
	CH0_COEFF_0,
	CH1_COEFF_0,
	CH0_COEFF_1,
	CH1_COEFF_1,
	CH0_COEFF_2,
	CH1_COEFF_2,
	CH0_COEFF_3,
	CH1_COEFF_3,
    };

    int fd_;
    float temperatures_[8];
    time_t last_update_[8];
    time_t update_interval_;
    std::string device_;
    struct hiddev_devinfo info_;
    useconds_t sync_interval_;
    std::string serial_;

    public:
    Impl(std::string device);
    ~Impl();

    static std::vector<USBTempInfo> findDevices();

    const std::string &getDevice() const;
    const std::string &getSerialNumber() const;

    void dioConfig(DIO_DIRECTION direction);
    uint8_t dioRead();
    void dioSet(const uint8_t value);

    void dioBitConfig(const uint8_t bit, DIO_DIRECTION direction);
    bool dioBitRead(const uint8_t bit);
    void dioBitSet(const uint8_t bit, const bool value);

    SENSOR_TYPE getSensorType(CHANNEL channel);
    void setSensorType(CHANNEL channel, SENSOR_TYPE type);

    THERMOCOUPLE_TYPE getThermocoupleType(CHANNEL channel);
    void setThermocoupleType(CHANNEL channel, THERMOCOUPLE_TYPE type);

    CONNECTION getSensorConnection(CHANNEL channel);
    void setSensorConnection(CHANNEL channel, CONNECTION connection);

    SEMICONDUCTOR_TYPE getSemiconductorType(CHANNEL channel);
    void setSemiconductorType(CHANNEL channel, SEMICONDUCTOR_TYPE type);

    GAIN getGain(CHANNEL channel);
    void setGain(CHANNEL channel, GAIN gain);

    FILTER_FREQUENCY getFilterFrequency(CHANNEL channel);
    void setFilterFrequency(CHANNEL channel, FILTER_FREQUENCY frequency);

    EXCITATION getExcitation(CHANNEL channel);
    void setExcitation(CHANNEL channel, EXCITATION exitation);

    float getVRef(CHANNEL channel);
    void setVRef(CHANNEL channel, const float value);

    float getI_10uA(CHANNEL channel);
    void setI_10uA(CHANNEL channel, const float value);

    float getI_210uA(CHANNEL channel);
    void setI_210uA(CHANNEL channel, const float value);

    float getI_10uA_3Wire(CHANNEL channel);
    void setI_10uA_3Wire(CHANNEL channel, const float value);

    float getV_10uA(CHANNEL channel);
    void setV_10uA(CHANNEL channel, const float value);

    float getV_210uA(CHANNEL channel);
    void setV_210uA(CHANNEL channel, const float value);

    float getV_10uA_3Wire(CHANNEL channel);
    void setV_10uA_3Wire(CHANNEL channel, const float value);

    void getCoefficients(CHANNEL channel, float *coeffs);
    void setCoefficients(CHANNEL channel, const float *coeffs);

    float getTemperature(CHANNEL channel, UNIT unit);
    void scanTemperatures(CHANNEL first, CHANNEL last, UNIT unit,
			  float *values);

    size_t readMemory(MEMORY mem, const uint16_t base, void *data, size_t n);
    size_t writeMemory(MEMORY mem, const uint16_t base, const void *data,
		       const size_t n);

    void blinkLED();

    void resetUSB();

    uint8_t getStatus();

    void calibrate();

    uint8_t getBurnoutStatus();

    void prepareDownload(MEMORY mem);

    void writeCode(uint32_t base, void *data, size_t n);
    void readCode(uint32_t base, void *data, size_t n);
    void writeSerial(char *data, size_t n);

    private:
    useconds_t getSyncInterval() const;
    std::string readSerialNumber() const;

    void sendOutputReport(COMMAND cmd, const uint8_t data = 0);
    void sendOutputReport(COMMAND cmd, const void *data, const uint32_t n);

    uint8_t getInputReport(COMMAND cmd);
    void getInputReport(COMMAND cmd, void *data, const uint32_t n);

    void setItem(CHANNEL channel, ITEM item, const uint8_t value);
    void setItem(CHANNEL channel, ITEM item, const void *value, const size_t n);

    uint8_t getItem(CHANNEL channel, ITEM item);
    void getItem(CHANNEL channel, ITEM item, void *value, const size_t n);

    static bool itemHasSingleByteValue(ITEM item);
    static std::string itemDescription(ITEM item);
};

const uint16_t USBTemp::Impl::VENDOR_ID = 0x09DB;
const uint16_t USBTemp::Impl::PRODUCT_ID = 0x008D;
const uint8_t USBTemp::Impl::UNLOCK_CODE = 0xAD;
const uint32_t USBTemp::Impl::USAGE_CODE = 0xFF000001;
const size_t USBTemp::Impl::MEMORY_BYTES_PER_OP = 0x3B;

USBTemp::Impl::Impl(std::string device)
    : fd_(-1), temperatures_{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
      last_update_{0, 0, 0, 0, 0, 0, 0, 0}, update_interval_(1),
      device_(std::move(device)), sync_interval_(20000)
{
    errno = 0;
    fd_ = open(device_.c_str(), O_RDONLY);
    if (fd_ == -1) {
	throw USBTempError(strerror(errno));
    }

    int rc = ioctl(fd_, HIDIOCGDEVINFO, &info_);
    if (rc < 0) {
	while (close(fd_) == -1 && errno == EINTR)
	    ;
	throw USBTempError(device_ + ": failed to get device info (" +
			   strerror(-rc) + ")");
    }

    if (info_.vendor != VENDOR_ID || info_.product != PRODUCT_ID) {
	while (close(fd_) == -1 && errno == EINTR)
	    ;
	throw USBTempError(device_ + " is not a USB-TEMP device");
    }

    sync_interval_ = getSyncInterval();

    try {
	serial_ = readSerialNumber();
    } catch (std::exception &e) {
	while (close(fd_) == -1 && errno == EINTR)
	    ;
	throw;
    }
}

USBTemp::Impl::~Impl()
{
    if (fd_ != -1) {
	while (close(fd_) == -1 && errno == EINTR)
	    ;
    }
}

std::vector<USBTempInfo> USBTemp::Impl::findDevices()
{
    std::vector<USBTempInfo> devices;
    char buf[64];
    for (uint32_t i = 0; i < HID_DEV_MAX; ++i) {
	snprintf(buf, sizeof(buf), "/dev/usb/hiddev%u", i);
	int fd = open(buf, O_RDONLY);
	if (fd == -1) {
	    continue;
	}

	struct hiddev_devinfo info;
	int rc = ioctl(fd, HIDIOCGDEVINFO, &info);
	if (rc < 0) {
	    close(fd);
	    continue;
	}

	if (info.vendor == VENDOR_ID && info.product == PRODUCT_ID) {
	    devices.emplace_back(makeUSBTempInfo(std::string(buf), &info));
	}

	close(fd);
    }

    return devices;
}

const std::string &USBTemp::Impl::getDevice() const { return device_; }
const std::string &USBTemp::Impl::getSerialNumber() const { return serial_; }
////////////////////////////////////////////////////////////////////////////////

void USBTemp::Impl::dioConfig(DIO_DIRECTION direction)
{
    sendOutputReport(COMMAND::DPORT_CONFIG, (uint8_t)direction);
}

uint8_t USBTemp::Impl::dioRead()
{
    sendOutputReport(COMMAND::READ_DPORT, 0);
    return getInputReport(COMMAND::READ_DPORT);
}

void USBTemp::Impl::dioSet(uint8_t value)
{
    sendOutputReport(COMMAND::SET_DPORT, value);
}

void USBTemp::Impl::dioBitConfig(uint8_t bit, DIO_DIRECTION direction)
{
    uint8_t data[2] = {bit, (uint8_t)direction};
    sendOutputReport(COMMAND::DPORT_CONFIG_BIT, data, 2);
}

bool USBTemp::Impl::dioBitRead(uint8_t bit)
{
    sendOutputReport(COMMAND::READ_DPORT_BIT, bit);
    return (getInputReport(COMMAND::READ_DPORT_BIT) & 0x01);
}

void USBTemp::Impl::dioBitSet(uint8_t bit, bool value)
{
    uint8_t data[2] = {bit, value ? (uint8_t)0x01 : (uint8_t)0x00};
    sendOutputReport(COMMAND::SET_DPORT_BIT, data, 2);
}

USBTemp::SENSOR_TYPE USBTemp::Impl::getSensorType(CHANNEL channel)
{
    return (SENSOR_TYPE)getItem(channel, ITEM::SENSOR_TYPE);
}

void USBTemp::Impl::setSensorType(CHANNEL channel, SENSOR_TYPE type)
{
    setItem(channel, ITEM::SENSOR_TYPE, (uint8_t)type);
}

USBTemp::THERMOCOUPLE_TYPE USBTemp::Impl::getThermocoupleType(CHANNEL channel)
{
    ITEM item = ITEM::CH0_TC_TYPE;
    if ((uint8_t)channel % 2) {
	item = ITEM::CH1_TC_TYPE;
    }
    return (THERMOCOUPLE_TYPE)getItem(channel, item);
}

void USBTemp::Impl::setThermocoupleType(CHANNEL channel, THERMOCOUPLE_TYPE type)
{
    ITEM item = ITEM::CH0_TC_TYPE;
    if ((uint8_t)channel % 2) {
	item = ITEM::CH1_TC_TYPE;
    }
    setItem(channel, item, (uint8_t)type);
}

USBTemp::CONNECTION USBTemp::Impl::getSensorConnection(CHANNEL channel)
{
    return (CONNECTION)getItem(channel, ITEM::CONNECTION_TYPE);
}

void USBTemp::Impl::setSensorConnection(CHANNEL channel, CONNECTION connection)
{
    setItem(channel, ITEM::CONNECTION_TYPE, (uint8_t)connection);
}

USBTemp::SEMICONDUCTOR_TYPE USBTemp::Impl::getSemiconductorType(CHANNEL channel)
{
    return (SEMICONDUCTOR_TYPE)getItem(channel, ITEM::CONNECTION_TYPE);
}

void USBTemp::Impl::setSemiconductorType(CHANNEL channel,
					 SEMICONDUCTOR_TYPE type)
{
    setItem(channel, ITEM::CONNECTION_TYPE, (uint8_t)type);
}

USBTemp::GAIN USBTemp::Impl::getGain(CHANNEL channel)
{
    ITEM item = ITEM::CH0_GAIN;
    if ((uint8_t)channel % 2) {
	item = ITEM::CH1_GAIN;
    }

    return (GAIN)getItem(channel, item);
}

void USBTemp::Impl::setGain(CHANNEL channel, GAIN gain)
{
    ITEM item = ITEM::CH0_GAIN;
    if ((uint8_t)channel % 2) {
	item = ITEM::CH1_GAIN;
    }

    setItem(channel, item, (uint8_t)gain);
}

USBTemp::FILTER_FREQUENCY USBTemp::Impl::getFilterFrequency(CHANNEL channel)
{
    return (FILTER_FREQUENCY)getItem(channel, ITEM::FILTER_RATE);
}

void USBTemp::Impl::setFilterFrequency(CHANNEL channel,
				       FILTER_FREQUENCY frequency)
{
    setItem(channel, ITEM::FILTER_RATE, (uint8_t)frequency);
}

USBTemp::EXCITATION USBTemp::Impl::getExcitation(CHANNEL channel)
{
    return (EXCITATION)getItem(channel, ITEM::EXCITATION);
}

void USBTemp::Impl::setExcitation(CHANNEL channel, EXCITATION excitation)
{
    setItem(channel, ITEM::EXCITATION, (uint8_t)excitation);
}

float USBTemp::Impl::getVRef(CHANNEL channel)
{
    float value;
    getItem(channel, ITEM::VREF, &value, sizeof(value));
    return value;
}

void USBTemp::Impl::setVRef(CHANNEL channel, float value)
{
    setItem(channel, ITEM::VREF, &value, sizeof(value));
}

float USBTemp::Impl::getI_10uA(CHANNEL channel)
{
    float value;
    getItem(channel, ITEM::I_10_UA, &value, sizeof(value));
    return value;
}

void USBTemp::Impl::setI_10uA(CHANNEL channel, float value)
{
    setItem(channel, ITEM::I_10_UA, &value, sizeof(value));
}

float USBTemp::Impl::getI_210uA(CHANNEL channel)
{
    float value;
    getItem(channel, ITEM::I_210_UA, &value, sizeof(value));
    return value;
}

void USBTemp::Impl::setI_210uA(CHANNEL channel, float value)
{
    setItem(channel, ITEM::I_210_UA, &value, sizeof(value));
}

float USBTemp::Impl::getI_10uA_3Wire(CHANNEL channel)
{
    float value;
    getItem(channel, ITEM::I_10_UA_3WIRE, &value, sizeof(value));
    return value;
}

void USBTemp::Impl::setI_10uA_3Wire(CHANNEL channel, float value)
{
    setItem(channel, ITEM::I_10_UA_3WIRE, &value, sizeof(value));
}

float USBTemp::Impl::getV_10uA(CHANNEL channel)
{
    float value;
    getItem(channel, ITEM::V_10_UA, &value, sizeof(value));
    return value;
}

void USBTemp::Impl::setV_10uA(CHANNEL channel, float value)
{
    setItem(channel, ITEM::V_10_UA, &value, sizeof(value));
}

float USBTemp::Impl::getV_210uA(CHANNEL channel)
{
    float value;
    getItem(channel, ITEM::V_210_UA, &value, sizeof(value));
    return value;
}

void USBTemp::Impl::setV_210uA(CHANNEL channel, float value)
{
    setItem(channel, ITEM::V_210_UA, &value, sizeof(value));
}

float USBTemp::Impl::getV_10uA_3Wire(CHANNEL channel)
{
    float value;
    getItem(channel, ITEM::V_10_UA_3WIRE, &value, sizeof(value));
    return value;
}

void USBTemp::Impl::setV_10uA_3Wire(CHANNEL channel, float value)
{
    setItem(channel, ITEM::V_10_UA_3WIRE, &value, sizeof(value));
}

void USBTemp::Impl::getCoefficients(CHANNEL channel, float *coeffs)
{
    ITEM item = ITEM::CH0_COEFF_0;
    if ((uint8_t)channel % 2) {
	item = ITEM::CH1_COEFF_0;
    }

    for (size_t i = 0; i < 3; ++i) {
	getItem(channel, (ITEM)((uint8_t)item + (2 * i)), coeffs + i,
		sizeof(*coeffs));
    }
}

void USBTemp::Impl::setCoefficients(CHANNEL channel, const float *coeffs)
{
    ITEM item = ITEM::CH0_COEFF_0;
    if ((uint8_t)channel % 2) {
	item = ITEM::CH1_COEFF_0;
    }

    for (size_t i = 0; i < 3; ++i) {
	setItem(channel, (ITEM)((uint8_t)item + (2 * i)), coeffs + i,
		sizeof(*coeffs));
    }
}

float USBTemp::Impl::getTemperature(CHANNEL channel, UNIT unit)
{
    static volatile bool in_flight = false;

    float *value = temperatures_ + (size_t)channel;
    time_t now = time(nullptr);
    if (now - last_update_[(size_t)channel] >= update_interval_) {
	last_update_[(size_t)channel] = now;
	const uint8_t data[2] = {(uint8_t)channel, (uint8_t)unit};
	assert(!in_flight);
	in_flight = true;
	sendOutputReport(COMMAND::READ_TEMP, data, 2);
	getInputReport(COMMAND::READ_TEMP, value, sizeof(float));
	in_flight = false;
	// usleep(10000);
	if (!std::isnormal(*value)) {
	    *value = nanf("");
	}
    }
    return *value;
}

void USBTemp::Impl::scanTemperatures(CHANNEL first, CHANNEL last, UNIT unit,
				     float *values)
{
    const time_t now = time(NULL);
    const size_t num_channels = (size_t)((last - first) + 1);
    bool update = false;
    for (size_t i = 0; i < num_channels; ++i) {
	if (now - last_update_[(size_t)first + i] >= update_interval_) {
	    update = true;
	    break;
	}
    }
    if (update) {
	const uint8_t data[3] = {(uint8_t)first, (uint8_t)last, (uint8_t)unit};
	sendOutputReport(COMMAND::SCAN_TEMP, data, 3);
	getInputReport(COMMAND::SCAN_TEMP, temperatures_ + (size_t)first,
		       num_channels * sizeof(float));
	for (size_t i = first; i <= (size_t)last; ++i) {
	    last_update_[i] = now;
	    if (!std::isnormal(temperatures_[i])) {
		temperatures_[i] = nanf("");
	    }
	}
    }
    memcpy(values, temperatures_ + (size_t)first, num_channels * sizeof(float));
}

/*
size_t USBTemp::Impl::readMemory(MEMORY mem, uint16_t base, void *data,
				 size_t n)
{
}

size_t USBTemp::Impl::writeMemory(MEMORY mem, uint16_t base, void *data,
				  size_t n)
{
}
*/

void USBTemp::Impl::blinkLED() { sendOutputReport(COMMAND::BLINK_LED); }
void USBTemp::Impl::resetUSB() { sendOutputReport(COMMAND::RESET_USB); }
uint8_t USBTemp::Impl::getStatus()
{
    sendOutputReport(COMMAND::GET_STATUS);
    return getInputReport(COMMAND::GET_STATUS);
}

void USBTemp::Impl::calibrate()
{
    sendOutputReport(COMMAND::CALIBRATE);
    do {
	sleep(1);
    } while (getStatus() & 0x01);
}

uint8_t USBTemp::Impl::getBurnoutStatus()
{
    sendOutputReport(COMMAND::BURNOUT_STATUS);
    return getInputReport(COMMAND::BURNOUT_STATUS);
}

/*

void USBTemp::Impl::prepareDownload(MEMORY mem)
{
}

void USBTemp::Impl::writeCode(uint32_t base, void *data, size_t n)
{
}

void USBTemp::Impl::readCode(uint32_t base, void *data, size_t n)
{
}

void USBTemp::Impl::writeSerial(char *data, size_t n)
{
}

*/

////////////////////////////////////////////////////////////////////////////////

std::string USBTemp::Impl::readSerialNumber() const
{
    struct hiddev_string_descriptor desc;
    desc.index = 3;
    int rc = ioctl(fd_, HIDIOCGSTRING, &desc);
    if (rc < 0) {
	throw USBTempError("failed to get serial number string");
    }
    return std::string(desc.value, desc.value + rc);
}

useconds_t USBTemp::Impl::getSyncInterval() const
{
    useconds_t sync_interval = 10000;

    libusb_context *ctxt;
    int rc = libusb_init(&ctxt);
    if (rc < 0) {
	return sync_interval;
    }

    libusb_set_debug(ctxt, LIBUSB_LOG_LEVEL_NONE);

    libusb_device **devices;
    ssize_t num_devs = libusb_get_device_list(ctxt, &devices);
    if (num_devs < 0) {
	libusb_exit(ctxt);
	return sync_interval;
    }

    for (ssize_t i = 0; i < num_devs; ++i) {
	libusb_device_descriptor desc;
	libusb_get_device_descriptor(devices[i], &desc);

	if (desc.idVendor != VENDOR_ID || desc.idProduct != PRODUCT_ID) {
	    continue;
	}

	const uint8_t bus = libusb_get_bus_number(devices[i]);
	const uint8_t addr = libusb_get_device_address(devices[i]);

	if (bus != info_.busnum || addr != info_.devnum) {
	    continue;
	}

	int speed = libusb_get_device_speed(devices[i]);
	switch (speed) {
	case LIBUSB_SPEED_UNKNOWN:
	case LIBUSB_SPEED_LOW:
	    sync_interval = 10000;
	    break;
	case LIBUSB_SPEED_FULL:
	    sync_interval = 1000;
	    break;
	case LIBUSB_SPEED_HIGH:
	case LIBUSB_SPEED_SUPER:
	    sync_interval = 125;
	    break;
	}

	break;
    }
    libusb_free_device_list(devices, 1);
    libusb_exit(ctxt);
    return sync_interval;
}

void USBTemp::Impl::sendOutputReport(COMMAND cmd, const uint8_t data)
{
    struct hiddev_usage_ref uref;
    uref.report_type = HID_REPORT_TYPE_OUTPUT;
    uref.report_id = (uint32_t)cmd;
    uref.field_index = 0;
    uref.usage_index = 0;
    uref.usage_code = USAGE_CODE;
    uref.value = data;

    int rc = ioctl(fd_, HIDIOCSUSAGE, &uref);
    if (rc < 0) {
	throw USBTempError(strerror(-rc));
    }

    struct hiddev_report_info rinfo;
    rinfo.report_type = HID_REPORT_TYPE_OUTPUT;
    rinfo.report_id = (uint32_t)cmd;
    rinfo.num_fields = 1;

    if ((rc = ioctl(fd_, HIDIOCSREPORT, &rinfo)) < 0) {
	throw USBTempError(strerror(-rc));
    }

    usleep(sync_interval_);
}

void USBTemp::Impl::sendOutputReport(COMMAND cmd, const void *data,
				     const uint32_t n)
{
    const uint8_t *values = (const uint8_t *)data;

    // std::cout << "[D] Sending Ouput Report: (id=0x" << std::hex
    //	  << std::setfill('0') << std::setw(2) << (uint32_t)cmd
    //	  << ", num_values=0x" << n << "):\n\tData:";
    // for (size_t i = 0; i < n; ++i) {
    //	std::cout << " 0x" << std::setw(2) << (uint32_t)values[i];
    //}
    // std::cout << std::dec << std::setfill(' ') << std::endl;

    struct hiddev_usage_ref_multi uref_multi;

    memset(&uref_multi, 0, sizeof(uref_multi));

    uref_multi.uref.report_type = HID_REPORT_TYPE_OUTPUT;
    uref_multi.uref.report_id = (uint32_t)cmd;
    uref_multi.uref.field_index = 0;
    uref_multi.uref.usage_index = 0;
    uref_multi.uref.usage_code = USAGE_CODE;
    uref_multi.num_values = n;
    for (size_t i = 0; i < n; ++i) {
	uref_multi.values[i] = values[i];
    }

    struct hiddev_report_info rinfo;
    rinfo.report_type = HID_REPORT_TYPE_OUTPUT;
    rinfo.report_id = (uint32_t)cmd;
    rinfo.num_fields = n;

    int rc = ioctl(fd_, HIDIOCSUSAGES, &uref_multi);
    if (rc < 0) {
	throw USBTempError(strerror(-rc));
    }

    if ((rc = ioctl(fd_, HIDIOCSREPORT, &rinfo)) < 0) {
	throw USBTempError(strerror(-rc));
    }

    usleep(sync_interval_);
}

uint8_t USBTemp::Impl::getInputReport(COMMAND cmd)
{
    struct hiddev_usage_ref uref;
    uref.report_type = HID_REPORT_TYPE_INPUT;
    uref.report_id = (uint32_t)cmd;
    uref.field_index = 0;
    uref.usage_index = 0;
    uref.usage_code = USAGE_CODE;
    uref.value = 0;

    int rc = ioctl(fd_, HIDIOCGUSAGE, &uref);
    if (rc < 0) {
	throw USBTempError(strerror(-rc));
    }

    // std::cout << "[D] Got Input Report (cmd=0x" << std::hex
    //	  << std::setfill('0') << std::setw(2) << (uint32_t)cmd
    //	  << ", value=0x" << uref.value << std::setfill(' ') << std::dec
    //	  << ")\n";
    return (uint8_t)(uref.value & 0xFF);
}

void USBTemp::Impl::getInputReport(COMMAND cmd, void *data, const uint32_t n)
{
    struct hiddev_usage_ref_multi uref_multi;

    memset(&uref_multi, 0, sizeof(uref_multi));

    uref_multi.uref.report_type = HID_REPORT_TYPE_INPUT;
    uref_multi.uref.report_id = (uint32_t)cmd;
    uref_multi.uref.field_index = 0;
    uref_multi.uref.usage_index = 0;
    uref_multi.uref.usage_code = USAGE_CODE;
    uref_multi.num_values = n;

    int rc = ioctl(fd_, HIDIOCGUSAGES, &uref_multi);
    if (rc < 0) {
	throw USBTempError(strerror(-rc));
    }

    // std::cout << "[D] Got Input Report (cmd=0x" << std::hex
    //	  << std::setfill('0') << std::setw(2) << (uint32_t)cmd
    //	  << ", values=";

    uint8_t *values = (uint8_t *)data;
    for (size_t i = 0; i < n; ++i) {
	values[i] = (uint8_t)(uref_multi.values[i] & 0x000000FF);
	//	std::cout << " 0x" << std::setw(2) <<
	//(uint32_t)values[i];
    }
    // std::cout << ")\n" << std::setfill(' ') << std::dec;
}

void USBTemp::Impl::setItem(CHANNEL channel, ITEM item, const uint8_t value)
{
    if (!itemHasSingleByteValue(item)) {
	throw USBTempError(itemDescription(item) +
			   " is a multi-byte value item");
    }

    uint8_t data[3] = {(uint8_t)(channel / 2), (uint8_t)item, value};
    sendOutputReport(COMMAND::SET_CONFIG_ITEM, data, 3);
}

void USBTemp::Impl::setItem(CHANNEL channel, ITEM item, const void *value,
			    const size_t n)
{
    assert(n == 4);

    if (itemHasSingleByteValue(item)) {
	throw USBTempError(itemDescription(item) +
			   " is a single-byte value item");
    }

    uint8_t data[6];
    data[0] = (uint8_t)channel / 2;
    data[1] = (uint8_t)item;
    memcpy(data + 2, value, n);
    sendOutputReport(COMMAND::SET_CONFIG_ITEM, data, 6);
}

uint8_t USBTemp::Impl::getItem(CHANNEL channel, ITEM item)
{
    if (!itemHasSingleByteValue(item)) {
	throw USBTempError(itemDescription(item) +
			   " is a multi-byte value item");
    }

    uint8_t data[2] = {(uint8_t)(channel / 2), (uint8_t)item};

    sendOutputReport(COMMAND::GET_CONFIG_ITEM, data, 2);
    return getInputReport(COMMAND::GET_CONFIG_ITEM);
}

void USBTemp::Impl::getItem(CHANNEL channel, ITEM item, void *value,
			    const size_t n)
{
    assert(n == 4);

    if (itemHasSingleByteValue(item)) {
	throw USBTempError(itemDescription(item) + " is a 8-bit value item");
    }

    uint8_t data[6];
    data[0] = (uint8_t)(channel / 2);
    data[1] = (uint8_t)item;

    sendOutputReport(COMMAND::GET_CONFIG_ITEM, data, 2);
    getInputReport(COMMAND::GET_CONFIG_ITEM, data, 6);

    memcpy(value, data + 2, 4);
}

bool USBTemp::Impl::itemHasSingleByteValue(ITEM item)
{
    switch (item) {
    case ITEM::SENSOR_TYPE:
    case ITEM::CONNECTION_TYPE:
    case ITEM::FILTER_RATE:
    case ITEM::EXCITATION:
    case ITEM::CH0_TC_TYPE:
    case ITEM::CH1_TC_TYPE:
    case ITEM::CH0_GAIN:
    case ITEM::CH1_GAIN:
	return true;
    case ITEM::VREF:
    case ITEM::I_10_UA:
    case ITEM::I_210_UA:
    case ITEM::I_10_UA_3WIRE:
    case ITEM::V_10_UA:
    case ITEM::V_210_UA:
    case ITEM::V_10_UA_3WIRE:
    case ITEM::CH0_COEFF_0:
    case ITEM::CH1_COEFF_0:
    case ITEM::CH0_COEFF_1:
    case ITEM::CH1_COEFF_1:
    case ITEM::CH0_COEFF_2:
    case ITEM::CH1_COEFF_2:
    case ITEM::CH0_COEFF_3:
    case ITEM::CH1_COEFF_3:
	return false;
    }

    assert(false);
}

std::string USBTemp::Impl::itemDescription(ITEM item)
{
    std::string result;
    switch (item) {
    case ITEM::SENSOR_TYPE:
	result = "Sensor type";
	break;
    case ITEM::CONNECTION_TYPE:
	result = "Connection type";
	break;
    case ITEM::FILTER_RATE:
	result = "Filter frequency";
	break;
    case ITEM::EXCITATION:
	result = "Exication current";
	break;
    case ITEM::VREF:
	result = "Reference voltage";
	break;
    case ITEM::I_10_UA:
	result = "Measured current@10uA";
	break;
    case ITEM::I_210_UA:
	result = "Measured current@210uA";
	break;
    case ITEM::I_10_UA_3WIRE:
	result = "Measured current@10uA (3 wire)";
	break;
    case ITEM::V_10_UA:
	result = "Measured voltage@10uA";
	break;
    case ITEM::V_210_UA:
	result = "Measured voltage@210uA";
	break;
    case ITEM::V_10_UA_3WIRE:
	result = "Measured voltage@10uA (3 wire)";
	break;
    case ITEM::CH0_TC_TYPE:
	result = "Channel 0 thermocouple type";
	break;
    case ITEM::CH1_TC_TYPE:
	result = "Channel 1 thermocouple type";
	break;
    case ITEM::CH0_GAIN:
	result = "Channel 0 gain";
	break;
    case ITEM::CH1_GAIN:
	result = "Channel 1 gain";
	break;
    case ITEM::CH0_COEFF_0:
	result = "Channel 0 coefficient 0";
	break;
    case ITEM::CH1_COEFF_0:
	result = "Channel 1 coefficient 0";
	break;
    case ITEM::CH0_COEFF_1:
	result = "Channel 0 coefficient 1";
	break;
    case ITEM::CH1_COEFF_1:
	result = "Channel 1 coefficient 1";
	break;
    case ITEM::CH0_COEFF_2:
	result = "Channel 0 coefficient 2";
	break;
    case ITEM::CH1_COEFF_2:
	result = "Channel 1 coefficient 2";
	break;
    case ITEM::CH0_COEFF_3:
	result = "Channel 0 coefficient 3";
	break;
    case ITEM::CH1_COEFF_3:
	result = "Channel 1 coefficient 3";
	break;
    }
    return result;
}

////////////////////////////////////////////////////////////////////////////////
// USBTemp                                                                    //
////////////////////////////////////////////////////////////////////////////////

USBTemp::USBTemp(std::string device)
    : impl_(new USBTemp::Impl(std::move(device)))
{
}

USBTemp::~USBTemp() {}
USBTemp::USBTemp(USBTemp &&rhs) : impl_(std::move(rhs.impl_)) {}
USBTemp &USBTemp::operator=(USBTemp &&rhs)
{
    impl_ = std::move(rhs.impl_);
    return *this;
}

std::vector<USBTempInfo> USBTemp::findDevices()
{
    return USBTemp::Impl::findDevices();
}

const std::string &USBTemp::getDevice() const { return impl_->getDevice(); }
const std::string &USBTemp::getSerialNumber() const
{
    return impl_->getSerialNumber();
}

void USBTemp::dioConfig(DIO_DIRECTION direction)
{
    impl_->dioConfig(direction);
}

uint8_t USBTemp::dioRead() { return impl_->dioRead(); }
void USBTemp::dioSet(const uint8_t value) { impl_->dioSet(value); }
void USBTemp::dioBitConfig(const uint8_t bit, DIO_DIRECTION direction)
{
    impl_->dioBitConfig(bit, direction);
}

bool USBTemp::dioBitRead(const uint8_t bit) { return impl_->dioBitRead(bit); }
void USBTemp::dioBitSet(const uint8_t bit, const bool value)
{
    impl_->dioBitSet(bit, value);
}

USBTemp::SENSOR_TYPE USBTemp::getSensorType(CHANNEL channel)
{
    return impl_->getSensorType(channel);
}

void USBTemp::setSensorType(CHANNEL channel, SENSOR_TYPE type)
{
    impl_->setSensorType(channel, type);
}

USBTemp::THERMOCOUPLE_TYPE USBTemp::getThermocoupleType(CHANNEL channel)
{
    return impl_->getThermocoupleType(channel);
}

void USBTemp::setThermocoupleType(CHANNEL channel, THERMOCOUPLE_TYPE type)
{
    impl_->setThermocoupleType(channel, type);
}

USBTemp::CONNECTION USBTemp::getSensorConnection(CHANNEL channel)
{
    return impl_->getSensorConnection(channel);
}

void USBTemp::setSensorConnection(CHANNEL channel, CONNECTION connection)
{
    impl_->setSensorConnection(channel, connection);
}

USBTemp::SEMICONDUCTOR_TYPE USBTemp::getSemiconductorType(CHANNEL channel)
{
    return impl_->getSemiconductorType(channel);
}

void USBTemp::setSemiconductorType(CHANNEL channel, SEMICONDUCTOR_TYPE type)
{
    impl_->setSemiconductorType(channel, type);
}

USBTemp::GAIN USBTemp::getGain(CHANNEL channel)
{
    return impl_->getGain(channel);
}

void USBTemp::setGain(CHANNEL channel, GAIN gain)
{
    impl_->setGain(channel, gain);
}

USBTemp::FILTER_FREQUENCY USBTemp::getFilterFrequency(CHANNEL channel)
{
    return impl_->getFilterFrequency(channel);
}

void USBTemp::setFilterFrequency(CHANNEL channel, FILTER_FREQUENCY frequency)
{
    impl_->setFilterFrequency(channel, frequency);
}

USBTemp::EXCITATION USBTemp::getExcitation(CHANNEL channel)
{
    return impl_->getExcitation(channel);
}

void USBTemp::setExcitation(CHANNEL channel, EXCITATION exitation)
{
    impl_->setExcitation(channel, exitation);
}

float USBTemp::getVRef(CHANNEL channel) { return impl_->getVRef(channel); }
void USBTemp::setVRef(CHANNEL channel, const float value)
{
    impl_->setVRef(channel, value);
}

float USBTemp::getI_10uA(CHANNEL channel) { return impl_->getI_10uA(channel); }
void USBTemp::setI_10uA(CHANNEL channel, const float value)
{
    impl_->setI_10uA(channel, value);
}

float USBTemp::getI_210uA(CHANNEL channel)
{
    return impl_->getI_210uA(channel);
}

void USBTemp::setI_210uA(CHANNEL channel, const float value)
{
    impl_->setI_210uA(channel, value);
}

float USBTemp::getI_10uA_3Wire(CHANNEL channel)
{
    return impl_->getI_10uA_3Wire(channel);
}

void USBTemp::setI_10uA_3Wire(CHANNEL channel, const float value)
{
    impl_->setI_10uA_3Wire(channel, value);
}

float USBTemp::getV_10uA(CHANNEL channel) { return impl_->getV_10uA(channel); }
void USBTemp::setV_10uA(CHANNEL channel, const float value)
{
    impl_->setV_10uA(channel, value);
}

float USBTemp::getV_210uA(CHANNEL channel)
{
    return impl_->getV_210uA(channel);
}

void USBTemp::setV_210uA(CHANNEL channel, const float value)
{
    impl_->setV_210uA(channel, value);
}

float USBTemp::getV_10uA_3Wire(CHANNEL channel)
{
    return impl_->getV_10uA_3Wire(channel);
}

void USBTemp::setV_10uA_3Wire(CHANNEL channel, const float value)
{
    impl_->setV_10uA_3Wire(channel, value);
}

void USBTemp::getCoefficients(CHANNEL channel, float *coeffs)
{
    impl_->getCoefficients(channel, coeffs);
}

void USBTemp::setCoefficients(CHANNEL channel, const float *coeffs)
{
    impl_->setCoefficients(channel, coeffs);
}

float USBTemp::getTemperature(CHANNEL channel, UNIT unit)
{
    return impl_->getTemperature(channel, unit);
}

void USBTemp::scanTemperatures(CHANNEL first, CHANNEL last, UNIT unit,
			       float *values)
{
    impl_->scanTemperatures(first, last, unit, values);
}

/*

size_t USBTemp::readMemory(enum USBTEMP_MEMORY_TYPE type, uint16_t base,
			   void *data, size_t n)
{
	return impl_->readMemory(type, base, data, n);
}

size_t USBTemp::writeMemory(enum USBTEMP_MEMORY_TYPE type, uint16_t base,
			    void *data, size_t n)
{
	return impl_->writeMemory(type, base, data, n);
}

*/

void USBTemp::blinkLED() { impl_->blinkLED(); }
void USBTemp::resetUSB() { impl_->resetUSB(); }
uint8_t USBTemp::getStatus() { return impl_->getStatus(); }
void USBTemp::calibrate() { impl_->calibrate(); }
uint8_t USBTemp::getBurnoutStatus() { return impl_->getBurnoutStatus(); }
/*

void USBTemp::prepareDownload(enum USBTEMP_MEMORY_TYPE type)
{
	impl_->prepareDownload(type);
}

void USBTemp::writeCode(uint32_t base, void *data, size_t n)
{
	impl_->writeCode(base, data, n);
}

void USBTemp::readCode(uint32_t base, void *data, size_t n)
{
	impl_->readCode(base, data, n);
}

void USBTemp::writeSerial(char *data, size_t n)
{
	impl_->writeSerial(data, n);
}

*/

} // namespace CALF
