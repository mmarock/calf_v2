

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../header/PIDLock.hh"

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <cstdio>

////////////////////////////////////////////////////////////
//		class PIDLock Impl
////////////////////////////////////////////////////////////

class PIDLock::Impl
{
    int fd_;

    public:
    static const char *LOCK_PATH;

    Impl();
    ~Impl();

    bool aqquire() noexcept;
    void release() noexcept;
};

const char *PIDLock::Impl::LOCK_PATH = "/tmp/calf_pidlock.lock";

PIDLock::Impl::Impl() : fd_(-1) {}
PIDLock::Impl::~Impl() { release(); }
bool PIDLock::Impl::aqquire() noexcept
{
    // open file
    if ((fd_ = open(PIDLock::Impl::LOCK_PATH, O_WRONLY | O_CREAT | O_EXCL)) <
	0) {
	perror("open()");
	return false;
    }
    auto p = getpid();
    dprintf(fd_, "%u", p);
    return true;
}

void PIDLock::Impl::release() noexcept
{
    if (fd_ > 0) {
	close(fd_);
	unlink(LOCK_PATH);
	fd_ = -1;
    }
}

////////////////////////////////////////////////////////////
//		PIDLock declaration
////////////////////////////////////////////////////////////

PIDLock::Impl &PIDLock::getImpl() noexcept
{
    static Impl i;
    return i;
}

bool PIDLock::aqquire() noexcept { return getImpl().aqquire(); }
void PIDLock::release() noexcept { getImpl().release(); }
