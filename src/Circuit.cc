

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../header/Circuit.hh"

#include "../header/Detector.hh"
#include "../header/FillGroup.hh"
#include "../header/FillReport.hh"
#include "../header/FillTime.hh"
#include "../header/MainVent.hh"
#include "../header/NotificationSystem.hh"
#include "../header/RRDManager.hh"
#include "../header/Scheduler.hh"
#include "../header/USBEnvironment.hh"
#include "../header/ValveVent.hh"
#include "../header/logger.hh"
#include "../header/make_unique.hpp"
#include "config.h"

#include <algorithm>
#include <fstream>
#include <stdexcept>
#include <thread>

#include <cassert>
#include <cmath>
#include <cstring>

#include <linux/hiddev.h>

#include <json/json.h>

namespace CALF
{
////////////////////////////////////////////////////////////////////////////////
//		Exception definitions
////////////////////////////////////////////////////////////////////////////////

CircuitError::CircuitError(const std::string &msg)
    : msg_(std::string("CircuitError: ") + msg)
{
}
CircuitError::CircuitError(const char *msg) : msg_(msg) {}
const char *CircuitError::what() const noexcept { return msg_.c_str(); }
/**
 * @brief a circuit element error; An element is missing or you tried to add the
 * same ID more than once
 */
struct ElementError : public CircuitError {
    enum TYPE : uint8_t { MAINVENT, VALVEVENT, DETECTOR, USBTEMP };

    enum CAUSE : uint8_t { NOT_FOUND, ALREADY_ADDED };

    ElementError(CAUSE c, TYPE t, int id)
	: CircuitError(std::string("ElementError: ") + cause(c) + type(t) +
		       "[ id=" + std::to_string(id) + "]")
    {
    }

    const char *cause(CAUSE c)
    {
	switch (c) {
	case CAUSE::ALREADY_ADDED:
	    return "already added";
	case CAUSE::NOT_FOUND:
	    return "not found";
	};
    }

    const char *type(TYPE t)
    {
	switch (t) {
	case TYPE::MAINVENT:
	    return "main_vent";
	case TYPE::VALVEVENT:
	    return "valve_vent";
	case TYPE::DETECTOR:
	    return "detector";
	case TYPE::USBTEMP:
	    return "usb_temp";
	};
    }
};

/**
 * @brief Something went wrong at loading a config file
 */
struct ConfigFileError : public CircuitError {
    enum class ERROR {
	FILE_NOT_FOUND,
	CIRCUIT_NOT_IN_CONFIG,
	CONFIG_PARSING_FAILED
    };

    explicit ConfigFileError(ERROR err)
	: CircuitError(std::string("ConfigFileError: ") + errorStr(err))
    {
    }

    private:
    static const char *errorStr(ERROR err)
    {
	switch (err) {
	case ERROR::FILE_NOT_FOUND:
	    return "file not found";
	case ERROR::CIRCUIT_NOT_IN_CONFIG:
	    return "circuit not in config";
	case ERROR::CONFIG_PARSING_FAILED:
	    return "parsing config file failed";
	};
    }
};

struct IllFormedConfigFileMember : public CircuitError {
    enum class TYPE { MAIN_VENT, VALVE_VENT, DETECTOR, CIRCUITS };

    const char *typeStr(TYPE t)
    {
	switch (t) {
	case TYPE::MAIN_VENT:
	    return "MainVent";
	case TYPE::VALVE_VENT:
	    return "ValveVent";
	case TYPE::DETECTOR:
	    return "Detector";
	case TYPE::CIRCUITS:
	    return "Circuits";
	};
    }

    explicit IllFormedConfigFileMember(TYPE t)
	: CircuitError(std::string("IllFormedConfigFileMember: ") + typeStr(t) +
		       ": is no array")
    {
    }
};

struct JsonError : public CircuitError {
    explicit JsonError(Json::Exception &ex) : CircuitError(ex.what()) {}
};

struct NoSensor : public CircuitError {
    enum class SENSOR { INLET, OUTLET };

    NoSensor(int detector, SENSOR sensor_type)
	: CircuitError(std::string("NoSensor: detector_id: ") +
		       std::to_string(detector) + ", type: " +
		       ((sensor_type == SENSOR::OUTLET) ? "outlet" : "inlet"))
    {
    }
};

struct CircuitBadTemperature : public CircuitError {
    CircuitBadTemperature(int sensor, float resistance)
	: CircuitError(std::string("BadTemperature: sensor: ") +
		       std::to_string(sensor) + ", resistance: " +
		       std::to_string(resistance))
    {
    }
};

////////////////////////////////////////////////////////////////////////////////
//		log definitions
////////////////////////////////////////////////////////////////////////////////

namespace log
{
static void close_main_vent(int main_vent)
{
    logger::log(LOG_LEVEL::INFO, "main_vent[%d] closed", main_vent);
}

static void detector_filled(int detector)
{
    logger::log(LOG_LEVEL::INFO, "detector[%d] filled", detector);
}

static void detector_is_not_active(int d_id)
{
    logger::log(LOG_LEVEL::NOTICE, "detector[%d] not active", d_id);
}

static void detector_outlet_ok(int d, const Temperature &outlet_temp,
			       const Temperature &outlet_trigger)
{
    logger::log(LOG_LEVEL::INFO, "detector[%d].outlet is ok (%.01lf/%.01lf)", d,
		outlet_temp.inCelsius(), outlet_trigger.inCelsius());
}

static void detector_timeout(int detector)
{
    logger::log(LOG_LEVEL::WARNING, "detector[%d] timeout", detector);
}

static void detector_warmup(int detector)
{
    logger::log(LOG_LEVEL::WARNING, "detector[%d] warmup", detector);
}

static void faulty_temp_data(int vvent)
{
    logger::log(LOG_LEVEL::WARNING, "valve_vent[%d] faulty temperature data",
		vvent);
}

static void faulty_temp_data(int detector, bool have_inlet, bool have_outlet)
{
    logger::log(LOG_LEVEL::WARNING,
		"detector[%d] faulty temperature data, please check %s",
		detector, (have_inlet && have_outlet)
			      ? "inlet and outlet"
			      : (have_inlet) ? "inlet" : "outlet");
}

static void filled(const FillGroup &grp, const std::chrono::minutes &mins)
{
    logger::log(LOG_LEVEL::INFO,
		"FillGroup[%d] fill stop after %d std::chrono::minutes",
		grp.getID(), mins.count());
}

static void open_main_vent(int main_vent)
{
    logger::log(LOG_LEVEL::INFO, "main_vent[%d] opened", main_vent);
}

static void preliminary_warmup(int detector, const Temperature &inlet_temp)
{
    logger::log(LOG_LEVEL::WARNING,
		"detector[%d] warmup before filling (%.01lf)", detector,
		inlet_temp.inCelsius());
}

static void prepare_filling(int detector)
{
    logger::log(LOG_LEVEL::INFO, "detector[%d] prepare for filling", detector);
}

static void prepare_ventilation(int vvent)
{
    logger::log(LOG_LEVEL::INFO, "valve_vent[%d] prepare for ventilation",
		vvent);
}

static void report_filling(int d_id, bool is_opened)
{
    logger::log(LOG_LEVEL::NOTICE, "detector[%d] %s", d_id,
		(is_opened) ? "opened" : "closed");
}

static void report_ventilation(int vvent_id, bool is_opened)
{
    logger::log(LOG_LEVEL::NOTICE, "valve_vent[%d] %s", vvent_id,
		(is_opened) ? "opened" : "closed");
}

static void try_filling(const FillGroup &grp)
{
    logger::log(LOG_LEVEL::INFO, "FillGroup[%d] fill start", grp.getID());
}

static void ventilation_sensor_ok(int d, const Temperature &sensor_temp,
				  const Temperature &sensor_trigger)
{
    logger::log(LOG_LEVEL::INFO, "ventilation[%d].sensor is ok (%.01lf/%.01lf)",
		d, sensor_temp.inCelsius(), sensor_trigger.inCelsius());
}

} // namespace log

template <class T>
typename std::vector<T>::iterator find_by_key(std::vector<T> &cont,
					      const int key)
{
    return find_if(cont.begin(), cont.end(),
		   [&](const T &el) { return (key == el.id()); });
}

template <class T>
typename std::vector<T>::const_iterator find_by_key(const std::vector<T> &cont,
						    const int key)
{
    return find_if(cont.begin(), cont.end(),
		   [&](const T &el) { return (key == el.id()); });
}

template <class T>
typename std::vector<T>::iterator erase_by_key(std::vector<T> &cont, int key)
{
    auto it = find_by_key(cont, key);
    assert(it != cont.end());
    return cont.erase(it);
}

////////////////////////////////////////////////////////////////////////////////
//		Circuit::Impl declaration and definitions
////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The Circuit implimentation class
 * @see Impl Idiom
 * @internal
 */
class Circuit::Impl
{
    const int id_;

    std::mutex cfg_mtx_;
    std::vector<ValveVent> valve_vents_;
    std::vector<MainVent> main_vents_;
    std::vector<Detector> detectors_;

    NotificationSystem notification_system_;
    RRDManager rrd_manager_;

    static Temperature default_trigger_temp;

    public:
    Impl(int id, Circuit &circuit);

    Impl(const Impl &) = delete;
    Impl &operator=(const Impl &) = delete;

    static Circuit fromConfigFile(int id);
    void toConfigFile();

    Json::Value toJson() const;

    static const char *circuit_config_path() noexcept;
    void updateRRDPtr(Circuit *c);

    void reload();
    void clear();
    void dump();

    int id() const noexcept;
    const std::vector<MainVent> &getMainVents() const noexcept;
    const std::vector<ValveVent> &getValveVents() const noexcept;
    const std::vector<Detector> &getDetectors() const noexcept;
    const NotificationSystem &getNotificationSystem() const noexcept;

    MainVent &findMainVent(int main_vent_id);
    const MainVent &findMainVent(int main_vent_id) const;
    bool isMainVentRegistered(int main_vent_id) const;

    ValveVent &findValveVent(int valve_vent_id);
    const ValveVent &findValveVent(int valve_vent_id) const;
    bool isValveVentRegistered(int valve_vent_id) const;

    Detector &findDetector(int detector_id);
    const Detector &findDetector(int detector_id) const;
    bool isDetectorRegistered(int detector_id) const;

    int pairMainVent(int vent_usb, uint8_t vent_ch);
    void splitMainVent(int mvent_id);

    int pairValveVent(int vent_usb, uint8_t vent_ch);
    void addSensor(int vvent_id, int sensor_usb, uint8_t sensor_ch);
    void splitValveVent(int vvent_id);

    int pairDetector(int vent_usb, uint8_t vent_ch);
    void addInlet(int detector, int sensor_usb, uint8_t sensor_ch);
    void addOutlet(int detector, int sensor_usb, uint8_t sensor_ch);
    void splitDetector(int d_id);

    void setDetectorActivity(int id, bool is_active);

    void openMainVents();
    void closeMainVents();

    std::chrono::minutes fillTimeout(const std::vector<int> &ids) const;
    std::chrono::minutes fillTimeout(int detector_id) const;

    std::chrono::minutes maxVentilationTimeout() const;
    std::chrono::minutes maxDetectorTimeout(int detector_id) const;
    std::chrono::minutes maxDetectorTimeout(const std::vector<int> &ids) const;

    Temperature getDetectorInletTemperature(int detector_id);
    Temperature getDetectorOutletTemperature(int detector_id);

    bool fill(const volatile bool &cancel, const FillGroup &grp);

    bool handleVentilation(const volatile bool &cancel_prep, FillReport &msg);
    void handleVentilationThread(ValveVent vvent,
				 const volatile bool &cancel_fill,
				 bool &vent_sucessfull, FillReport &report);

    bool handleDetectors(const FillGroup &grp, const volatile bool &cancel,
			 FillReport &msg);
    void handleDetectorThread(const volatile bool &cancel,
			      bool &fill_sucessfull, int detector_id,
			      FillReport &msg);

    static void from_config(Circuit::Impl &circuit);

    private:
    template <class T>
    static int new_id(const typename std::vector<T> &container)
    {
	int new_id(1);

	while (std::find_if(container.begin(), container.end(),
			    [&new_id](const T &t) {
				return t.id() == new_id;
			    }) != container.end()) {
	    ++new_id;
	}

	return new_id;
    }
};

Temperature Circuit::Impl::default_trigger_temp =
    Temperature::fromCelsius(-190);

Circuit::Impl::Impl(int id, Circuit &circuit)
    : id_(id), notification_system_(NotificationSystem::fromConfigFile()),
      rrd_manager_(&circuit)
{
}

void Circuit::Impl::from_config(Circuit::Impl &impl)
{
    std::ifstream file(circuit_config_path(), std::ios::in);
    if (!file.is_open()) {
	// file error
	throw ConfigFileError(ConfigFileError::ERROR::FILE_NOT_FOUND);
    }
    Json::Reader reader;

    Json::Value root(Json::objectValue);
    if (!reader.parse(file, root, true)) {
	throw ConfigFileError(ConfigFileError::ERROR::CONFIG_PARSING_FAILED);
    }

    file.close();

    if (!root["circuits"].isArray()) {
	throw ConfigFileError(ConfigFileError::ERROR::CONFIG_PARSING_FAILED);
    }

    auto &circuits = root["circuits"];

    if (!circuits.isArray()) {
	throw IllFormedConfigFileMember(
	    IllFormedConfigFileMember::TYPE::CIRCUITS);
    }

    auto it = std::find_if(
	circuits.begin(), circuits.end(),
	[&](const Json::Value &val) { return val["id"].asInt() == impl.id(); });

    if (it == circuits.end() || !it->isObject()) {
	throw ConfigFileError(ConfigFileError::ERROR::CIRCUIT_NOT_IN_CONFIG);
    }

    auto &circuit = *it;

    std::vector<Detector> detectors;
    std::vector<MainVent> main_vents;
    std::vector<ValveVent> valve_vents;

    try {
	if (!circuit["main_vents"].isArray()) {
	    throw IllFormedConfigFileMember(
		IllFormedConfigFileMember::TYPE::MAIN_VENT);
	}

	if (!circuit["valve_vents"].isArray()) {
	    throw IllFormedConfigFileMember(
		IllFormedConfigFileMember::TYPE::VALVE_VENT);
	}

	if (!circuit["detectors"].isArray()) {
	    throw IllFormedConfigFileMember(
		IllFormedConfigFileMember::TYPE::DETECTOR);
	}

	for (const auto &m : circuit["main_vents"]) {
	    main_vents.emplace_back(MainVent::fromJson(m));
	}

	for (const auto &m : circuit["valve_vents"]) {
	    valve_vents.emplace_back(ValveVent::fromJson(m));
	}

	for (const auto &d : circuit["detectors"]) {
	    detectors.emplace_back(Detector::fromJson(d));
	}

    } catch (Json::Exception &ex) {
	throw JsonError(ex);
    } catch (DetectorError &ex) {
	throw CircuitError(ex.what());
    } catch (MainVentError &ex) {
	throw CircuitError(ex.what());
    } catch (ValveVentError &ex) {
	throw CircuitError(ex.what());
    }

    std::lock_guard<std::mutex> lk(impl.cfg_mtx_);

    for (auto &d : impl.detectors_) {
	if (d.isActive()) {
	    impl.rrd_manager_.unsubscribe(d.id());
	}
    }

    // is noexcept
    std::swap(impl.detectors_, detectors);
    std::swap(impl.main_vents_, main_vents);
    std::swap(impl.valve_vents_, valve_vents);

    for (auto &d : impl.detectors_) {
	if (d.isActive()) {
	    impl.rrd_manager_.subscribe(d.id(), d.hasInlet(), d.hasOutlet());
	}
    }
}

Circuit Circuit::Impl::fromConfigFile(int id)
{
    // erstellte neuen circuit
    Circuit circuit(id);

    // parse default path
    // construct all needed data
    // tausche Inhalt aus
    from_config(*(circuit.impl_));

    return circuit;
}

void Circuit::Impl::toConfigFile()
{
    assert(false && "implement Circuit::Impl::toConfigFile()");
}

Json::Value Circuit::Impl::toJson() const
{
    try {
	Json::Value root(Json::objectValue);

	root["id"] = id();

	Json::Value valves(Json::arrayValue);
	for (const auto &v : valve_vents_) {
	    valves.append(v.toJson());
	}

	Json::Value mains(Json::arrayValue);
	for (const auto &m : main_vents_) {
	    mains.append(m.toJson());
	}

	Json::Value detectors(Json::arrayValue);
	for (const auto &d : detectors_) {
	    detectors.append(d.toJson());
	}

	root["valve_vents"] = valves;
	root["main_vents"] = mains;
	root["detectors"] = detectors;

	root["notification_system"] = notification_system_.toJson();

	return root;

    } catch (Json::Exception &ex) {
	throw JsonError(ex);
    }
}

const char *Circuit::Impl::circuit_config_path() noexcept
{
    return CIRCUIT_CONFIG_DIR "/circuit.json";
}

/**
 * @brief replace the saved pointer from Circuit declaration in the
 * implimentation
 *
 * @todo This function is needed because RRDManager needs detector data to
 * request temperatures
 * 		and therefore it gets a pointer to the used Circuit. This is
 * errorprone
 * 		and should be changed. One way could be to pass just detectors_
 * vector.
 * 		This vector is fixed in memory ( since Impl must not be copied
 * or
 * moved )
 *
 * @param c the circuit handle
 */
void Circuit::Impl::updateRRDPtr(Circuit *c)
{
    rrd_manager_.updateCircuitPtr(c);
}

void Circuit::Impl::reload()
{
    // parse default path
    // construct all needed data
    // tausche inhalt
    from_config(*this);
}

void Circuit::Impl::clear()
{
    std::lock_guard<std::mutex> lk(cfg_mtx_);
    while (!detectors_.empty()) {
	auto &d = detectors_.back();
	rrd_manager_.unsubscribe(d.id());
	detectors_.pop_back();
    }
    valve_vents_.clear();
    main_vents_.clear();
}

void Circuit::Impl::dump()
{
    logger::log(LOG_LEVEL::NOTICE, "[ circuit ] dump");
}

int Circuit::Impl::id() const noexcept { return id_; }
const std::vector<MainVent> &Circuit::Impl::getMainVents() const noexcept
{
    return main_vents_;
}
const std::vector<ValveVent> &Circuit::Impl::getValveVents() const noexcept
{
    return valve_vents_;
}
const std::vector<Detector> &Circuit::Impl::getDetectors() const noexcept
{
    return detectors_;
}
const NotificationSystem &Circuit::Impl::getNotificationSystem() const noexcept
{
    return notification_system_;
}

MainVent &Circuit::Impl::findMainVent(int main_vent_id)
{
    auto it = find_by_key(main_vents_, main_vent_id);
    if (it == main_vents_.end()) {
	throw ElementError(ElementError::NOT_FOUND, ElementError::MAINVENT,
			   main_vent_id);
    }
    return *it;
}

const MainVent &Circuit::Impl::findMainVent(int main_vent_id) const
{
    auto it = find_by_key(main_vents_, main_vent_id);
    if (it == main_vents_.end()) {
	throw ElementError(ElementError::NOT_FOUND, ElementError::MAINVENT,
			   main_vent_id);
    }
    return *it;
}

bool Circuit::Impl::isMainVentRegistered(int main_vent_id) const
{
    return (find_by_key(main_vents_, main_vent_id) != main_vents_.end());
}

ValveVent &Circuit::Impl::findValveVent(int valve_vent_id)
{
    auto it = find_by_key(valve_vents_, valve_vent_id);
    if (it == valve_vents_.end()) {
	throw ElementError(ElementError::NOT_FOUND, ElementError::VALVEVENT,
			   valve_vent_id);
    }
    return *it;
}

const ValveVent &Circuit::Impl::findValveVent(int valve_vent_id) const
{
    auto it = find_by_key(valve_vents_, valve_vent_id);
    if (it == valve_vents_.end()) {
	throw ElementError(ElementError::NOT_FOUND, ElementError::VALVEVENT,
			   valve_vent_id);
    }
    return *it;
}

bool Circuit::Impl::isValveVentRegistered(int valve_vent_id) const
{
    return (find_by_key(valve_vents_, valve_vent_id) != valve_vents_.end());
}

Detector &Circuit::Impl::findDetector(int detector_id)
{
    auto it = find_by_key(detectors_, detector_id);
    if (it == detectors_.end()) {
	throw ElementError(ElementError::NOT_FOUND, ElementError::DETECTOR,
			   detector_id);
    }
    return *it;
}

const Detector &Circuit::Impl::findDetector(int detector_id) const
{
    auto it = find_by_key(detectors_, detector_id);
    if (it == detectors_.end()) {
	throw ElementError(ElementError::NOT_FOUND, ElementError::DETECTOR,
			   detector_id);
    }
    return *it;
}

bool Circuit::Impl::isDetectorRegistered(int detector_id) const
{
    return (find_by_key(detectors_, detector_id) != detectors_.end());
}

int Circuit::Impl::pairMainVent(int vent_usb, uint8_t ch)
{
    int id = new_id(main_vents_);

    std::lock_guard<std::mutex> lk(cfg_mtx_);

    if (!USBEnvironment::haveUSB(vent_usb)) {
	throw ElementError(ElementError::NOT_FOUND, ElementError::USBTEMP,
			   vent_usb);
    }

    main_vents_.emplace_back(id, vent_usb, ch);
    return id;
}

void Circuit::Impl::splitMainVent(int mvent_id)
{
    std::lock_guard<std::mutex> lk(cfg_mtx_);

    if (!isMainVentRegistered(mvent_id)) {
	throw ElementError(ElementError::NOT_FOUND, ElementError::MAINVENT,
			   mvent_id);
    }

    erase_by_key(main_vents_, mvent_id);
}

int Circuit::Impl::pairValveVent(int vent_usb, uint8_t vent_ch)
{
    int id = new_id(valve_vents_);

    std::lock_guard<std::mutex> lk(cfg_mtx_);

    if (!USBEnvironment::haveUSB(vent_usb)) {
	throw ElementError(ElementError::NOT_FOUND, ElementError::USBTEMP,
			   vent_usb);
    }

    valve_vents_.emplace_back(id, vent_usb, vent_ch);
    return id;
}

void Circuit::Impl::addSensor(int vvent_id, int sensor_usb, uint8_t sensor_ch)
{
    auto &v = findValveVent(vvent_id);

    v.setSensor(sensor_usb, sensor_ch, default_trigger_temp);
}

void Circuit::Impl::splitValveVent(int vvent_id)
{
    std::lock_guard<std::mutex> lk(cfg_mtx_);

    if (!isValveVentRegistered(vvent_id)) {
	throw ElementError(ElementError::NOT_FOUND, ElementError::VALVEVENT,
			   vvent_id);
    }

    erase_by_key(valve_vents_, vvent_id);
}

int Circuit::Impl::pairDetector(int vent_usb, uint8_t vent_ch)
{
    int id = new_id(detectors_);

    std::lock_guard<std::mutex> lk(cfg_mtx_);

    if (!USBEnvironment::haveUSB(vent_usb)) {
	throw ElementError(ElementError::NOT_FOUND, ElementError::USBTEMP,
			   vent_usb);
    }

    detectors_.emplace_back(id, vent_usb, vent_ch);
    return id;
}

void Circuit::Impl::addInlet(int detector, int sensor_usb, uint8_t sensor_ch)
{
    auto &d = findDetector(detector);

    d.setInlet(sensor_usb, sensor_ch, default_trigger_temp);

    if (rrd_manager_.isSubscribed(detector)) {
	rrd_manager_.unsubscribe(detector);
    }
    rrd_manager_.subscribe(detector, d.hasInlet(), d.hasOutlet());
}

void Circuit::Impl::addOutlet(int detector, int sensor_usb, uint8_t sensor_ch)
{
    auto &d = findDetector(detector);

    d.setOutlet(sensor_usb, sensor_ch, default_trigger_temp);

    if (rrd_manager_.isSubscribed(detector)) {
	rrd_manager_.unsubscribe(detector);
    }
    rrd_manager_.subscribe(detector, d.hasInlet(), d.hasOutlet());
}

void Circuit::Impl::splitDetector(int detector_id)
{
    std::lock_guard<std::mutex> lk(cfg_mtx_);
    if (!isDetectorRegistered(detector_id)) {
	ElementError(ElementError::NOT_FOUND, ElementError::DETECTOR,
		     detector_id);
    }

    auto it = find_if(detectors_.begin(), detectors_.end(),
		      [&](const Detector &t) { return t.id() == detector_id; });

    detectors_.erase(it);
    rrd_manager_.unsubscribe(detector_id);
}

void Circuit::Impl::setDetectorActivity(int detector_id, bool is_active)
{
    std::lock_guard<std::mutex> lk(cfg_mtx_);

    auto &detector = findDetector(detector_id);

    if (detector.isActive() == is_active) {
	return;
    }

    detector.setActive(is_active);

    if (is_active) {
	rrd_manager_.subscribe(detector_id, detector.hasInlet(),
			       detector.hasOutlet());
    } else if (detector.hasInlet() || detector.hasOutlet()) {
	rrd_manager_.unsubscribe(detector_id);
    }
}

void Circuit::Impl::openMainVents()
{
    for (auto &mv : main_vents_) {
	log::open_main_vent(mv.id());
	USBEnvironment::open(mv.getVentUSB(), mv.getVentChannel());
	// TODO catch exception
    }
}

void Circuit::Impl::closeMainVents()
{
    for (auto &mv : main_vents_) {
	log::close_main_vent(mv.id());
	USBEnvironment::close(mv.getVentUSB(), mv.getVentChannel());
	// TODO catch exception
    }
}

std::chrono::minutes
Circuit::Impl::fillTimeout(const std::vector<int> &ids) const
{
    return maxVentilationTimeout() + maxDetectorTimeout(ids);
}

std::chrono::minutes Circuit::Impl::fillTimeout(int id) const
{
    return maxVentilationTimeout() + maxDetectorTimeout(id);
}

std::chrono::minutes Circuit::Impl::maxVentilationTimeout() const
{
    using Element = const ValveVent &;

    auto it = max_element(valve_vents_.begin(), valve_vents_.end(),
			  [](Element a, Element b) {
			      return (a.getAllowedTimeDuration().second <
				      b.getAllowedTimeDuration().second);
			  });
    return std::chrono::duration_cast<std::chrono::minutes>(
	it->getAllowedTimeDuration().second);
}

std::chrono::minutes Circuit::Impl::maxDetectorTimeout(int detector_id) const
{
    auto it =
	find_if(detectors_.begin(), detectors_.end(),
		[&](const Detector &d) { return (d.id() == detector_id); });

    if (it == detectors_.end()) {
	throw ElementError(ElementError::NOT_FOUND, ElementError::DETECTOR,
			   detector_id);
    }
    return std::chrono::duration_cast<std::chrono::minutes>(
	it->getAllowedTimeDuration().second);
}

std::chrono::minutes
Circuit::Impl::maxDetectorTimeout(const std::vector<int> &ids) const
{
    std::chrono::minutes max_timeout(30);
    for (auto &obj : detectors_) {
	if (find(ids.begin(), ids.end(), obj.id()) == ids.end()) {
	    throw ElementError(ElementError::NOT_FOUND, ElementError::DETECTOR,
			       obj.id());
	}
	if ((obj.getAllowedTimeDuration().second > max_timeout)) {
	    max_timeout = std::chrono::duration_cast<std::chrono::minutes>(
		obj.getAllowedTimeDuration().second);
	}
    }
    return max_timeout;
}

Temperature Circuit::Impl::getDetectorInletTemperature(int detector_id)
{
    auto &detector = findDetector(detector_id);
    try {
	if (detector.hasInlet()) {
	    return USBEnvironment::temperature(detector.getInletUSB(),
					       detector.getInletChannel());
	}
    } catch (const USBEnvironmentError &ex) {
	throw CircuitError(ex.what());
    }
    throw NoSensor(detector_id, NoSensor::SENSOR::INLET);
}

Temperature Circuit::Impl::getDetectorOutletTemperature(int detector_id)
{
    auto &detector = findDetector(detector_id);
    try {
	if (detector.hasOutlet()) {
	    return USBEnvironment::temperature(detector.getOutletUSB(),
					       detector.getOutletChannel());
	}
    } catch (const CircuitError &ex) {
	throw CircuitError(ex.what());
    }
    throw NoSensor(detector_id, NoSensor::SENSOR::OUTLET);
}

bool Circuit::Impl::fill(const volatile bool &cancel, const FillGroup &grp)
{
    auto now(std::chrono::system_clock::now());
    log::try_filling(grp);

    FillReport report;
    const int grp_id(grp.getID());

    report.addLine(grp_id, EVENT::GROUP_BEGIN);

    auto &detectors = grp.getDetectors();
    bool succeeded(false);
    if (find_if(detectors.begin(), detectors.end(), [&](int d) {
	    return findDetector(d).isActive();
	}) != detectors.end()) {
	openMainVents();

	report.addLine(grp_id, EVENT::VENTILATION_BEGIN);
	succeeded = handleVentilation(cancel, report);

	if (succeeded) {
	    report.addLine(grp_id, EVENT::DETECTOR_BEGIN);
	}
	succeeded &= handleDetectors(grp, cancel, report);

	closeMainVents();

	if (cancel) {
	    report.addLine(grp_id, EVENT::USER_FILL_CANCEL);
	} else if (succeeded) {
	    report.addLine(grp_id, EVENT::GROUP_SUCESSFULL);
	} else {
	    report.addLine(grp_id, EVENT::GROUP_FAILED);
	}
    } else {
	report.addLine(grp_id, EVENT::GROUP_NO_ACTIVE_DETECTOR);
    }

    notification_system_.sendNotifications(report);

    log::filled(grp, std::chrono::duration_cast<std::chrono::minutes>(
			 std::chrono::system_clock::now() - now));
    return succeeded;
}

bool Circuit::Impl::handleVentilation(const volatile bool &cancel_prep,
				      FillReport &msg)
{
    std::vector<std::thread> vent_thrs_;

    bool ventilation_sucessfull(true);
    for (auto &vvent : valve_vents_) {
	vent_thrs_.emplace_back(&Circuit::Impl::handleVentilationThread, this,
				vvent, std::cref(cancel_prep),
				std::ref(ventilation_sucessfull),
				std::ref(msg));
    }
    for (std::thread &thr : vent_thrs_) {
	thr.join();
    }
    return ventilation_sucessfull;
}

void Circuit::Impl::handleVentilationThread(ValveVent vvent,
					    const volatile bool &cancel_prep,
					    bool &ventilation_sucessfull,
					    FillReport &msg)
{
    log::prepare_ventilation(vvent.id());

    auto duration(vvent.getAllowedTimeDuration());

    bool has_sensor = vvent.haveSensor();
    Temperature sensor_temp;
    Temperature sensor_trigger;
    if (has_sensor) {
	try {
	    sensor_temp = USBEnvironment::temperature(vvent.getSensorUSB(),
						      vvent.getSensorChannel());
	} catch (USBEnvironmentError &ex) {
	    ventilation_sucessfull = false;
	    log::faulty_temp_data(vvent.id());
	    return;
	}
	sensor_trigger = vvent.getTrigger();
    }

    USBEnvironment::open(vvent.getVentUSB(), vvent.getVentChannel());
    log::report_ventilation(vvent.id(), true);

    enum class OUTCOME {
	TEMPERATURE_FAILURE,
	CANCELLED,
	TIMEOUT,
	SENSOR_TEMPERATURE_DROP
    } outcome(OUTCOME::TIMEOUT);

    auto now = std::chrono::system_clock::now();
    auto min_vent_tp(now + duration.first);
    auto timeout_tp(now + duration.second);
    while ((now = std::chrono::system_clock::now()) < timeout_tp) {
	if (cancel_prep) {
	    outcome = OUTCOME::CANCELLED;
	    goto cleanup_handle_ventilation_thread;
	}

	if ((now >= min_vent_tp) && has_sensor &&
	    (sensor_trigger > sensor_temp)) {
	    outcome = OUTCOME::SENSOR_TEMPERATURE_DROP;
	    goto cleanup_handle_ventilation_thread;
	}

	std::this_thread::sleep_for(seconds(10));
	if (has_sensor) {
	    try {
		sensor_temp = USBEnvironment::temperature(
		    vvent.getSensorUSB(), vvent.getSensorChannel());
	    } catch (CircuitError &ex) {
		outcome = OUTCOME::TEMPERATURE_FAILURE;
		goto cleanup_handle_ventilation_thread;
	    }
	}
    }

cleanup_handle_ventilation_thread:

    USBEnvironment::close(vvent.getVentUSB(), vvent.getVentChannel());
    log::report_ventilation(vvent.id(), false);

    switch (outcome) {
    case OUTCOME::SENSOR_TEMPERATURE_DROP:
	log::ventilation_sensor_ok(vvent.id(), sensor_temp, sensor_trigger);
    case OUTCOME::CANCELLED:
	break;

    case OUTCOME::TIMEOUT:
	if (has_sensor) {
	    msg.addLine(vvent.id(), EVENT::VENTILATION_TIMEOUT);
	}
	break;
    case OUTCOME::TEMPERATURE_FAILURE:
	ventilation_sucessfull = false;
	log::faulty_temp_data(vvent.id());
	break;
    };
}

bool Circuit::Impl::handleDetectors(const FillGroup &grp,
				    const volatile bool &cancel,
				    FillReport &msg)
{
    std::vector<std::thread> fill_thrs;

    bool filling_sucessfull(true);

    for (auto &id : grp.getDetectors()) {
	fill_thrs.emplace_back(&Circuit::Impl::handleDetectorThread, this,
			       std::cref(cancel), std::ref(filling_sucessfull),
			       id, std::ref(msg));
    }
    for (std::thread &thr : fill_thrs) {
	thr.join();
    }
    return filling_sucessfull;
}

void Circuit::Impl::handleDetectorThread(const volatile bool &cancel,
					 bool &fill_sucessfull, int detector_id,
					 FillReport &msg)
{
    log::prepare_filling(detector_id);

    Detector &detector = findDetector(detector_id);
    if (!detector.isActive()) {
	log::detector_is_not_active(detector_id);
	msg.addLine(detector_id, EVENT::DETECTOR_INACTIVE);
	return;
    }

    const bool has_inlet(detector.hasInlet());
    int inlet_usb(-1);
    uint8_t inlet_channel(0);
    Temperature inlet_temp, inlet_trigger;

    const bool has_outlet(detector.hasOutlet());
    int outlet_usb(-1);
    uint8_t outlet_channel(0);
    Temperature outlet_temp, outlet_trigger;

    try {
	if (has_inlet) {
	    inlet_usb = detector.getInletUSB();
	    inlet_channel = detector.getInletChannel();
	    inlet_trigger = detector.getInletTrigger();
	    inlet_temp = USBEnvironment::temperature(inlet_usb, inlet_channel);
	}
	if (has_outlet) {
	    outlet_usb = detector.getOutletUSB();
	    outlet_channel = detector.getOutletChannel();
	    outlet_trigger = detector.getOutletTrigger();
	    outlet_temp =
		USBEnvironment::temperature(outlet_usb, outlet_channel);
	}
    } catch (CircuitError &ex) {
	fill_sucessfull = false;
	log::faulty_temp_data(detector_id, has_inlet, has_outlet);
	return;
    }

    // warmup before vent is open?
    if (has_inlet && (inlet_temp > inlet_trigger)) {
	msg.addLine(detector_id, EVENT::DETECTOR_WARMUP);
	log::preliminary_warmup(detector_id, inlet_temp);
	fill_sucessfull = false;
	return;
    }

    USBEnvironment::open(detector.getVentUSB(), detector.getVentChannel());
    log::report_filling(detector_id, true);

    enum class OUTCOME {
	TIMEOUT,
	CANCELLED,
	INLET_WARMUP,
	OUTLET_DROP,
	TEMPERATURE_FAILURE
    } outcome(OUTCOME::TIMEOUT);

    const auto duration = detector.getAllowedTimeDuration();
    auto now = std::chrono::system_clock::now();
    const auto min_vent_tp = now + duration.first;
    const auto timeout_tp = now + duration.second;

    while ((now = std::chrono::system_clock::now()) < timeout_tp) {
	if (cancel) {
	    outcome = OUTCOME::CANCELLED;
	    goto cleanup_handle_detector_thread;
	}

	if ((now >= min_vent_tp) && has_inlet && (inlet_trigger < inlet_temp)) {
	    outcome = OUTCOME::INLET_WARMUP;
	    goto cleanup_handle_detector_thread;
	}

	if ((now >= min_vent_tp) && has_outlet &&
	    (outlet_trigger > outlet_temp)) {
	    outcome = OUTCOME::OUTLET_DROP;
	    log::detector_outlet_ok(detector_id, outlet_temp, outlet_trigger);
	    goto cleanup_handle_detector_thread;
	}

	std::this_thread::sleep_for(seconds(10));

	try {
	    if (has_inlet) {
		inlet_temp =
		    USBEnvironment::temperature(inlet_usb, inlet_channel);
	    }
	    if (has_outlet) {
		outlet_temp =
		    USBEnvironment::temperature(outlet_usb, outlet_channel);
	    }
	} catch (USBEnvironmentError &ex) {
	    outcome = OUTCOME::TEMPERATURE_FAILURE;
	    goto cleanup_handle_detector_thread;
	}
    }

cleanup_handle_detector_thread:

    USBEnvironment::close(detector.getVentUSB(), detector.getVentChannel());
    log::report_filling(detector_id, false);

    switch (outcome) {
    case OUTCOME::TIMEOUT:
	if (has_outlet) {
	    msg.addLine(detector_id, EVENT::DETECTOR_TIMEOUT);
	    log::detector_timeout(detector_id);
	}
	break;

    case OUTCOME::INLET_WARMUP:
	msg.addLine(detector_id, EVENT::DETECTOR_WARMUP);
	log::detector_warmup(detector_id);
	break;

    case OUTCOME::OUTLET_DROP:
	log::detector_filled(detector_id);
    case OUTCOME::CANCELLED:
	break;

    case OUTCOME::TEMPERATURE_FAILURE:
	log::faulty_temp_data(detector_id);
	fill_sucessfull = false;
	break;
    };
}

//////////////////////////////
//	class Circuit	    //
//////////////////////////////

Circuit::Circuit(int id) : impl_(new Impl(id, *this)) {}
Circuit::~Circuit() {}
Circuit::Circuit(Circuit &&rhs) : impl_(std::forward<Circuit>(rhs).impl_)
{
    logger::log(LOG_LEVEL::DEBUG, __PRETTY_FUNCTION__);
    impl_->updateRRDPtr(this);
}

Circuit &Circuit::operator=(Circuit &&rhs)
{
    logger::log(LOG_LEVEL::DEBUG, __PRETTY_FUNCTION__);
    impl_ = std::forward<Circuit>(rhs).impl_;
    impl_->updateRRDPtr(this);
    return *this;
}

Circuit Circuit::fromConfigFile(int id) { return Impl::fromConfigFile(id); }
/**
 * @brief Dump the loaded config into Json
 *
 * @return This is a json node
 */
Json::Value Circuit::toJson() const
{
    assert(impl_ && "impl is nullptr");
    return impl_->toJson();
}

/**
 * @brief reload the circuit (including the NotificationSystem)
 * @todo Es ist eine Weile her, schaue hier auf jeden Fall nochmal rein
 */
void Circuit::reload()
{
    assert(impl_ && "impl is nullptr");
    impl_->reload();
}

/**
 * @brief clear the circuit
 */
void Circuit::clear()
{
    assert(impl_ && "impl is nullptr");
    impl_->clear();
}

/**
 * @brief dump the config into syslog
 *
 * @todo write that function
 */
void Circuit::dump()
{
    assert(impl_ && "impl is nullptr");
    impl_->dump();
}

/**
 * @brief get the circuit ID
 *
 * @return circuit_id_
 */
int Circuit::id() const noexcept
{
    assert(impl_ && "impl is nullptr");
    return impl_->id();
}

/**
 * @brief get the main vent vector (RO)
 *
 * @return const ref to vector of main vents
 */
const std::vector<MainVent> &Circuit::getMainVents() const noexcept
{
    assert(impl_ && "impl is nullptr");
    return impl_->getMainVents();
}
const std::vector<ValveVent> &Circuit::getValveVents() const noexcept
{
    assert(impl_ && "impl is nullptr");
    return impl_->getValveVents();
}
const std::vector<Detector> &Circuit::getDetectors() const noexcept
{
    assert(impl_ && "impl is nullptr");
    return impl_->getDetectors();
}
const NotificationSystem &Circuit::getNotificationSystem() const noexcept
{
    assert(impl_ && "impl is nullptr");
    return impl_->getNotificationSystem();
}

int Circuit::pairMainVent(int vent_usb, uint8_t vent_ch)
{
    assert(impl_ && "impl is nullptr");
    return impl_->pairMainVent(vent_usb, vent_ch);
}

int Circuit::pairValveVent(int vent_usb, uint8_t vent_ch)
{
    assert(impl_ && "impl is nullptr");
    return impl_->pairValveVent(vent_usb, vent_ch);
}

void Circuit::addSensor(int vvent_id, int sensor_usb, uint8_t sensor_ch)
{
    assert(impl_ && "impl is nullptr");
    impl_->addSensor(vvent_id, sensor_usb, sensor_ch);
}

int Circuit::pairDetector(int vent_usb, uint8_t vent_ch)
{
    assert(impl_ && "impl is nullptr");
    return impl_->pairDetector(vent_usb, vent_ch);
}

void Circuit::addInlet(int id, int sensor_usb, uint8_t sensor_ch)
{
    assert(impl_ && "impl is nullptr");
    impl_->addInlet(id, sensor_usb, sensor_ch);
}

void Circuit::addOutlet(int id, int sensor_usb, uint8_t sensor_ch)
{
    assert(impl_ && "impl is nullptr");
    impl_->addOutlet(id, sensor_usb, sensor_ch);
}

/**
 * @brief remove the main vent
 *
 * @param mvent_id the main vent id to remove
 */
void Circuit::splitMainVent(int mvent_id)
{
    assert(impl_ && "impl is nullptr");
    impl_->splitMainVent(mvent_id);
}

/**
 * @brief remove the valve vent
 *
 * @param vvent_id the valve vent id to remove
 */
void Circuit::splitValveVent(int vvent_id)
{
    assert(impl_ && "impl is nullptr");
    impl_->splitValveVent(vvent_id);
}

/**
 * @brief remove the detector from circuit
 *
 * @param d_id the detector ID
 */
void Circuit::splitDetector(int d_id)
{
    assert(impl_ && "impl is nullptr");
    impl_->splitDetector(d_id);
}

/**
 * @brief set the detector activity
 *
 * @param detector_id the ID
 * @param is_active enable = true, disable = false
 */
void Circuit::setDetectorActivity(int detector_id, bool is_active)
{
    assert(impl_ && "impl is nullptr");
    impl_->setDetectorActivity(detector_id, is_active);
}

/**
 * @see Circuit::getDetectorOutletTemperature
 */
Temperature Circuit::getDetectorInletTemperature(int detector_id)
{
    assert(impl_ && "impl is nullptr");
    return impl_->getDetectorInletTemperature(detector_id);
}

/**
 * @brief get an outlet temperature
 *
 * This is a convenience function:
 *
 * - Query the detector id
 * - Get to know the sensor usb id and channel
 * - Query the temperature
 * - Return it
 *
 * @param detector_id the DETECTOR id (not a channel or sensor id!)
 *
 * @return temperature
 *
 * @throws CircuitError
 */
Temperature Circuit::getDetectorOutletTemperature(int detector_id)
{
    assert(impl_ && "impl is nullptr");
    return impl_->getDetectorOutletTemperature(detector_id);
}

/**
 * @brief Fill a set of detectors provided by a given FillGroup
 *
 *	This function blocks, until the filling has happened
 *	or a timeout occured. This function has to be called by
 *	a seperate thread. In Calf, this is done by a Scheduler member.
 *
 * @param cancel a boolean which cancels the fill threads
 * @param grp provided fill group
 *
 * @return true if it filled, else false
 */
bool Circuit::fill(const volatile bool &cancel, const FillGroup &grp)
{
    assert(impl_ && "impl is nullptr");
    return impl_->fill(cancel, grp);
}

/**
 * @brief get the min timeout time of one cryostat
 *
 * @param detector_id the detector ID
 *
 * @return time in minutes
 */
std::chrono::minutes Circuit::fillTimeout(int detector_id) const
{
    assert(impl_ && "impl is nullptr");
    return impl_->fillTimeout(detector_id);
}

/**
 * @brief get the min timeout time of a set of cryostats
 *
 * @param ids the detector IDs
 *
 * @return time in minutes
 */
std::chrono::minutes Circuit::fillTimeout(const std::vector<int> &ids) const
{
    assert(impl_ && "impl is nullptr");
    return impl_->fillTimeout(ids);
}

} // namespace CALF
