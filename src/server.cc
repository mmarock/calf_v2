

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../header/Circuit.hh"
#include "../header/DescriptorGuard.hh"
#include "../header/PIDLock.hh"
#include "../header/RTDCalculator.hh"
#include "../header/Scheduler.hh"
#include "../header/ThreadedConnection.hh"
#include "../header/USBEnvironment.hh"
#include "../header/logger.hh"
#include "../header/make_unique.hpp"

#include <algorithm>

#include <cassert>
#include <cstring>

#include <getopt.h>
#include <signal.h>
#include <sys/epoll.h>
#include <sys/signalfd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

/**
 * @mainpage
 *
 * @section SYNOPSIS
 *	This software handles automated filling
 *	of cryostats. You have to supply a set of
 *	working configurations.
 *
 * @section USAGE
 * 	calfd starts the server, keep it in the background: calfd &
 * 	calf-cli gets some data from the server
 *
 * @section LICENSE
 *
 */

using namespace CALF;

namespace CALF
{
namespace log
{
static void notice(const char *msg)
{
    logger::log(LOG_LEVEL::NOTICE, "[ main ] %s", msg);
}
}
}

static int handle_signal_event(const struct epoll_event &ev, Circuit &circuit,
			       Scheduler &scheduler)
{
    struct signalfd_siginfo info;
    read(ev.data.fd, &info, sizeof(info));
    switch (info.ssi_signo) {
    case SIGINT:
	logger::log(LOG_LEVEL::DEBUG, "[ main ] Go foo yourself");
	return 1;
    case SIGTERM:
    case SIGSTOP:
	log::notice(" [ main ] try a shutdown");
	return 1;
    case SIGHUP:
	try {
	    circuit.reload();
	    scheduler.reload();
	    USBEnvironment::reload();
	} catch (const std::exception &ex) {
	    logger::log(LOG_LEVEL::WARNING, "[ main ] %s", ex.what());
	}
	// do refresh
	break;
    default:
	log::notice("[ main ] caught unknown signal");
	break;
    }
    return 0;
}

static void handle_connection_accept(
    std::vector<ThreadedConnection> &connections, const DescriptorGuard &sockfd,
    const DescriptorGuard &efd, Scheduler &scheduler, Circuit &circuit)
{
    int new_connection = accept(sockfd.fd(), nullptr, nullptr);
    connections.emplace_back(new_connection, circuit, scheduler);
    struct epoll_event event;
    event.data.fd = new_connection;
    event.events = EPOLLIN;
    epoll_ctl(efd.fd(), EPOLL_CTL_ADD, new_connection, &event);
    logger::log(LOG_LEVEL::NOTICE, "[connection %d]: new connection",
		new_connection);
}

static void
handle_connection_event(std::vector<ThreadedConnection> &connections,
			const DescriptorGuard &efd,
			const struct epoll_event &ev)
{
    // wahrscheinlich eine Verbindung
    auto it = std::find_if(
	connections.begin(), connections.end(),
	[&](const ThreadedConnection &c) { return c.fd() == ev.data.fd; });
    assert(it != connections.end() && "Unknown fd");
    if (it->readFromFd() <= 0) {
	// ensure, that file descriptor is closed
	it->closeFd();

	// test, if the closed descriptor is because of an error
	logger::log(LOG_LEVEL::NOTICE, "[connection %d]: %s", it->fd(),
		    (it->closeRequested()) ? "close requested by client"
					   : "error occured, close");
	epoll_ctl(efd.fd(), EPOLL_CTL_DEL, ev.data.fd, nullptr);

	// erase the whole connection
	connections.erase(it);
    }
}

void print_help(const struct option *options, const char **descriptions)
{
    printf("Aufruf: calfd [OPTION] --circuit [ID]\nOptions:\n");

    const char **description = descriptions;
    while (options && options->name && description) {
	printf(" -%c,  --%-20s%-30s\n", options->val, options->name,
	       *description);

	++options;
	++description;
    }
}

static int handle_program_options(int argc, char **argv, bool &start_empty,
				  int &circuit_id)
{
    (void)circuit_id;

    struct option options[] = {{"help", no_argument, 0, 'h'},
			       {"verbosity", optional_argument, 0, 'v'},
			       {"circuit", optional_argument, 0, 'c'},
			       {"start_empty", no_argument, 0, 's'},
			       {"debug", no_argument, 0, 'D'},
			       {0, 0, 0, 0}};
    const char *descriptions[] = {
	"print this string",
	"increase (or set) the verbosity",
	"load the configuration with given id",
	"explicitely load nothing",
	"do not daemonize, set verbosity to debug and start empty",
	nullptr};

    static_assert(sizeof(options) / sizeof(struct option) ==
		      sizeof(descriptions) / sizeof(char *),
		  "Jedes Argument braucht eine Beschreibung");

    int index(0);
    int c(0);
    while ((c = getopt_long(argc, argv, "hv::dc::sD", options, &index)) > 0) {
	switch (c) {
	case '?':
	case ':':
	case 'h':
	    print_help(options, descriptions);
	    return 1;
	case 'v':
	    assert(0 && "impl that shit!");
	case 'c':
	    assert(0 && "impl that shit");
	case 's':
	    start_empty = true;
	    break;
	case 'D':
	    logger::setLogLevel(LOG_LEVEL::DEBUG);
	    start_empty = true;
	    break;
	};
    }
    return 0;
}

/**
 *
 * @brief here does the magic happen
 */
int main(int argc, char **argv)
{
    int circuit_id(1); //<-- default value
    bool start_empty(false);

    /**
     * Do all signal stuff right at the beginning, because:
     * the signal mask is copied to every new thread.
     * If an object ( looking at you: Circuit ) creates a thread
     * and afterwards the signal mask is changed, the thread signal
     * mask remains the same! Because I want to catch signals with
     *signalfd(),
     * threads must not catch signals!
     **/

    sigset_t mask;
    sigfillset(&mask);
    sigprocmask(SIG_SETMASK, &mask, nullptr);

    logger::setLogLevel(
	LOG_LEVEL::DEBUG); //<-- remove in release or set to higher level

    if (handle_program_options(argc, argv, start_empty, circuit_id)) {
	return 0;
    }

    if (!PIDLock::aqquire()) {
	logger::log(LOG_LEVEL::ALERT, "[ main ] pid lock not aqquired");
	return -1;
    }

    try {
	RTDCalculatorFactory::loadRTDCalculator();
    } catch (const RTDCalculatorError &ex) {
	logger::log(LOG_LEVEL::ALERT, "[ main ] loadRTDCalculator() failed: %s",
		    ex.what());
	return -1;
    }

    // if (!start_empty && USBEnvironment::loadUSBEnvironment()) {
    try {
	USBEnvironment::loadUSBEnvironment();
    } catch (const USBEnvironmentError &ex) {
	logger::log(LOG_LEVEL::ALERT,
		    "[ main ] USBEnvironment::loadUSBEnvironment() failed: %s",
		    ex.what());
	return -1;
    }

    // lade optionen, nehme einstellungen vor
    //
    // erstelle circuit objekt
    // erstelle scheduler objekt
    // warte auf Signale und verarbeite diese
    // warte auf Verbindungen und verarbeite diese

    // baue so um, dass die Erzeugung der Objekte noexcept ist
    // und die Configuration separat geladen werden kann
    // auto circuit = Circuit::fromConfigFile(0);
    // auto scheduler = Scheduler::fromConfigFile(circuit);

    std::unique_ptr<Circuit> circuit(nullptr);
    std::unique_ptr<Scheduler> scheduler(nullptr);

    try {
	if (start_empty) {
	    circuit = custom::make_unique<Circuit>(circuit_id);
	    scheduler = custom::make_unique<Scheduler>(circuit_id);
	} else {
	    circuit = custom::make_unique<Circuit>(
		Circuit::fromConfigFile(circuit_id));
	    scheduler = custom::make_unique<Scheduler>(
		Scheduler::fromConfigFile(circuit_id));
	}
    } catch (const CircuitError &ex) {
	logger::log(LOG_LEVEL::NOTICE, "[ main ] %s, create empty circuit",
		    ex.what());
	circuit = custom::make_unique<Circuit>(circuit_id);
	scheduler = custom::make_unique<Scheduler>(circuit_id);
    } catch (const SchedulerError &ex) {
	logger::log(LOG_LEVEL::NOTICE, "[ main ] %s, create empty scheduler",
		    ex.what());
	scheduler = custom::make_unique<Scheduler>(circuit_id);
    } catch (const std::exception &ex) {
	logger::log(LOG_LEVEL::ALERT, "[ main ] %s", ex.what());
	return -1;
    }

    assert(circuit && "circuit must not be NULL");
    assert(scheduler && "scheduler must not be NULL");

    DescriptorGuard efd(epoll_create1(0));
    if (efd.fd() < 0) {
	logger::log(LOG_LEVEL::ALERT, "epoll_create1(): %s", strerror(errno));
	return -1;
    }

    DescriptorGuard sigfd(signalfd(-1, &mask, 0));
    if (sigfd.fd() < 0) {
	logger::log(LOG_LEVEL::ALERT, "signalfd(): %s", strerror(errno));
	return -1;
    }

    DescriptorGuard sockfd(socket(AF_UNIX, SOCK_STREAM, 0));
    if (sockfd.fd() < 0) {
	logger::log(LOG_LEVEL::ALERT, "socket(): %s", strerror(errno));
	return -1;
    }

    struct sockaddr_un addr {
	AF_UNIX, "/tmp/calf.socket"
    };
    if (bind(sockfd.fd(), (const struct sockaddr *)&addr,
	     sizeof(struct sockaddr_un)) < 0) {
	logger::log(LOG_LEVEL::ALERT, "bind(): %s", strerror(errno));
	return -1;
    }

    if (listen(sockfd.fd(), 10) < 0) {
	logger::log(LOG_LEVEL::ALERT, "listen(): %s", strerror(errno));
	unlink(addr.sun_path);
	return -1;
    }

    struct epoll_event evs[10];
    memset(evs, 0, sizeof(evs));

    // add epoll fd
    evs[0].data.fd = efd.fd();
    evs[0].events = EPOLLERR;
    epoll_ctl(efd.fd(), EPOLL_CTL_ADD, efd.fd(), evs);

    // add signalfd
    evs[0].data.fd = sigfd.fd();
    evs[0].events = EPOLLIN;
    epoll_ctl(efd.fd(), EPOLL_CTL_ADD, sigfd.fd(), evs);

    // add sockfd
    evs[0].data.fd = sockfd.fd();
    evs[0].events = EPOLLIN;
    epoll_ctl(efd.fd(), EPOLL_CTL_ADD, sockfd.fd(), evs);

    std::vector<ThreadedConnection> connections;

    int incoming_events(0);
    auto next_group = scheduler->next();
    while ((incoming_events = epoll_wait(
		efd.fd(), evs, 10, next_group.timeout_in_milliseconds)) >= 0 &&
	   !ThreadedConnection::globalStopRequested()) {
	logger::log(LOG_LEVEL::DEBUG, "[ main ] returned from epoll instance");

	// Kein timeout, starte einen Füllversuch
	if (!incoming_events) {
	    // launch detached fill thread
	    scheduler->fillGroup(*circuit, next_group.group_id);
	    next_group = scheduler->next();
	    continue;
	}

	for (int i = 0; i < incoming_events; ++i) {
	    const auto &ev = evs[i];
	    if (ev.data.fd == sigfd.fd() && ev.events & EPOLLIN) {
		// wenn ein Signal ankam
		if (handle_signal_event(ev, *circuit, *scheduler)) {
		    goto server_cleanup;
		}

	    } else if (ev.data.fd == efd.fd()) {
		// wenn epoll einen Fehler macht
		logger::log(LOG_LEVEL::ALERT, "[ main ] epoll "
					      "instance error, "
					      "stop execution");
		goto server_cleanup;

	    } else if (ev.data.fd == sockfd.fd() && ev.events & EPOLLIN) {
		// akzeptiere eine Verbindung
		handle_connection_accept(connections, sockfd, efd, *scheduler,
					 *circuit);
	    } else {
		// lese oder schließe eine Verbindung
		handle_connection_event(connections, efd, ev);
	    }
	}
    }

server_cleanup:

    logger::log(LOG_LEVEL::INFO, "[ main ] number of pending connections: %d",
		connections.size());

    connections.clear();
    connections.shrink_to_fit();

    unlink(addr.sun_path);

    return 0;
}
