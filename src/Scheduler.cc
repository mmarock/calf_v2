

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../header/Scheduler.hh"

#include "../header/Circuit.hh"
#include "../header/FillGroup.hh"
#include "../header/FillTime.hh"
#include "../header/logger.hh"
#include "config.h"

#include <algorithm>
#include <atomic>
#include <condition_variable>
#include <fstream>
#include <mutex>
#include <thread>
#include <vector>

#include <json/json.h>

namespace CALF
{
////////////////////////////////////////////////////////////
//		log namespace
////////////////////////////////////////////////////////////

namespace log
{
static void function_call(const char *funct)
{
    logger::log(LOG_LEVEL::DEBUG, "[ scheduler ] function_call: %s", funct);
}

static void group_started(int group)
{
    logger::log(LOG_LEVEL::INFO, "[ scheduler ] FillGroup: id: %d started",
		group);
}

static void group_stopped(int group)
{
    logger::log(LOG_LEVEL::INFO, "[ scheduler ] FillGroup: id: %d stopped",
		group);
}

static void filling_failed(const CircuitError &ex)
{
    logger::log(LOG_LEVEL::WARNING, "[ scheduler ] Circuit::fill() failed: %s",
		ex.what());
}

static void next_offset(const std::chrono::minutes &min)
{
    logger::log(LOG_LEVEL::DEBUG, "[ scheduler ] next offset in %dmin",
		min.count());
}

static void no_next_offset()
{
    logger::log(LOG_LEVEL::DEBUG, "[ scheduler ] no next offset, just wait");
}

static void try_reload()
{
    logger::log(LOG_LEVEL::NOTICE, "[ scheduler ] try reload");
}

static void not_in_config(int circuit_id)
{
    logger::log(LOG_LEVEL::NOTICE,
		"[ scheduler ] config circuit[id=%d] not found", circuit_id);
}

} // log namespace

////////////////////////////////////////////////////////////
//		exception definitions
////////////////////////////////////////////////////////////

SchedulerError::SchedulerError(const char *msg)
    : msg_(std::string("SchedulerError: ") + msg)
{
}
SchedulerError::SchedulerError(const std::string &msg)
    : msg_(std::string("SchedulerError: ") + msg)
{
}

const char *SchedulerError::what() const noexcept { return msg_.c_str(); }
struct GroupNotFound : public SchedulerError {
    explicit GroupNotFound(int id)
	: SchedulerError(std::string("GroupNotFound: id: ") +
			 std::to_string(id))
    {
    }
};

struct SchedulerNoConfigFound : public SchedulerError {
    explicit SchedulerNoConfigFound(const char *file)
	: SchedulerError(std::string("NoConfigFound: ") + file)
    {
    }
};

struct SchedulerConfigParserError : public SchedulerError {
    SchedulerConfigParserError(const char *file, Json::Reader &reader)
	: SchedulerError(std::string("ConfigError: file: ") + file + ": " +
			 reader.getFormattedErrorMessages())
    {
    }
};

struct SchedulerMissingObject : public SchedulerError {
    SchedulerMissingObject(const char *obj)
	: SchedulerError(std::string("MissingObject: obj: ") + obj)
    {
    }
};

////////////////////////////////////////////////////////////
//		Scheduler::Impl definitions
////////////////////////////////////////////////////////////

class Scheduler::Impl
{
    int id_;
    std::vector<FillGroup> groups_;

    std::mutex group_thr_mtx_;
    std::condition_variable group_thr_cdt_;
    volatile bool cancel_group_thr_;
    bool group_thr_active_;

    public:
    explicit Impl(int id);
    ~Impl();

    bool haveGroup(int group_id) const noexcept;
    int addGroup();
    void rmGroup(int grp);
    const std::vector<FillGroup> &groups() const noexcept;

    void addFillTime(int grp, int hour, int minute);

    void addDetector(int grp, int detector, minutes timeout);
    void updateDetector(int detector, minutes timeout);
    void rmDetector(int detector);	  // rm from all grps
    void rmDetector(int grp, int detector); // rm from one grp

    bool cancelFilling() noexcept;
    void fillGroup(Circuit &circ, int grp);
    /*
	    bool isPaused() const noexcept;
	    void pause();
	    void resume();
    */
    struct NextGroupData next() const noexcept;

    void reload();
    void clear() noexcept;
    void dump() noexcept;

    static Scheduler fromJson(const Json::Value &val);
    Json::Value toJson() const;

    static Scheduler fromConfigFile(int c);
    void toConfigFile() const;

    private:
    void groupThread(Circuit &circuit, FillGroup grp); // make a copy

    static const char *defaultConfigFile();
};

using namespace std;

Scheduler::Impl::Impl(int id)
    : id_(id), cancel_group_thr_(false), group_thr_active_(false)
{
}

Scheduler::Impl::~Impl() {}
bool Scheduler::Impl::haveGroup(int group_id) const noexcept
{
    return find_if(groups_.begin(), groups_.end(), [&](const FillGroup &gr) {
	       return group_id = gr.getID();
	   }) != groups_.end();
}

int Scheduler::Impl::addGroup()
{
    int new_id = 0;
    while (find_if(groups_.begin(), groups_.end(), [&](const FillGroup &grp) {
	       return grp.getID() == new_id;
	   }) != groups_.end()) {
	++new_id;
    }
    groups_.emplace_back(new_id);
    return new_id;
}

void Scheduler::Impl::rmGroup(int grp)
{
    auto it =
	find_if(groups_.begin(), groups_.end(),
		[&](const FillGroup &group) { return grp == group.getID(); });

    if (it == groups_.end()) {
	throw GroupNotFound(grp);
    }
    groups_.erase(it);
}

const std::vector<FillGroup> &Scheduler::Impl::groups() const noexcept
{
    return groups_;
}

void Scheduler::Impl::addFillTime(int grp, int hour, int minute)
{
    auto it = find_if(groups_.begin(), groups_.end(),
		      [&](const FillGroup &fg) { return grp == fg.getID(); });

    if (it == groups_.end()) {
	throw GroupNotFound(grp);
    }

    it->addFillTime(hour, minute);
}

void Scheduler::Impl::addDetector(int grp, int detector, minutes timeout)
{
    auto it = find_if(groups_.begin(), groups_.end(),
		      [&](const FillGroup &fg) { return grp == fg.getID(); });

    if (it == groups_.end()) {
	throw GroupNotFound(grp);
    }

    auto &detectors = it->getDetectors();
    if (find(detectors.begin(), detectors.end(), detector) != detectors.end()) {
	return;
    }

    // TODO: check if grp interferes with the other groups by a
    // given
    // time-frame
    it->addDetector(detector, timeout);
}

void Scheduler::Impl::rmDetector(int detector)
{
    {
	for (auto &grp : groups_) {
	    if (grp.haveDetector(detector)) {
		grp.removeDetector(detector);
	    }
	}
    }
}

void Scheduler::Impl::rmDetector(int group, int detector)
{
    auto it =
	find_if(groups_.begin(), groups_.end(),
		[&](const FillGroup &grp) { return group == grp.getID(); });
    if (it == groups_.end()) {
	throw GroupNotFound(group);
    }
    it->removeDetector(detector);
}

bool Scheduler::Impl::cancelFilling() noexcept
{
    unique_lock<std::mutex> lk(group_thr_mtx_);
    if (!group_thr_active_) {
	return true;
    }
    cancel_group_thr_ = true;

    return group_thr_cdt_.wait_for(lk, std::chrono::seconds(10), [&]() {
	return group_thr_active_ == false;
    });
}

void Scheduler::Impl::reload()
{
    log::function_call(__PRETTY_FUNCTION__);

    ifstream file(defaultConfigFile(), std::ios::in);
    if (!file.is_open()) {
	// file error
	throw SchedulerNoConfigFound(defaultConfigFile());
    }

    Json::Reader reader;
    Json::Value root = Json::Value(Json::ValueType::objectValue);
    if (!reader.parse(file, root)) {
	// parse error
	file.close();
	throw SchedulerConfigParserError(defaultConfigFile(), reader);
    }
    file.close();

    log::try_reload();

    auto it = find_if(root["schedulers"].begin(), root["schedulers"].end(),
		      [&](const Json::Value &value) {
			  return (!value["circuit_id"].isNull() &&
				  (value["circuit_id"].asInt() == id_));
		      });

    if (it == root["schedulers"].end()) {
	log::not_in_config(id_);
	throw SchedulerNoConfigFound("circuit not found");
    }

    auto &circuit_value = *it;

    // reload groups
    vector<FillGroup> groups;
    for (auto &grp : circuit_value["groups"]) {
	groups.push_back(FillGroup::fromJson(grp));
    }

    // everything worked, now swap the groups
    swap(groups_, groups);
}

void Scheduler::Impl::clear() noexcept { groups_.clear(); }
void Scheduler::Impl::dump() noexcept {}
void Scheduler::Impl::fillGroup(Circuit &circ, int group)
{
    auto it =
	find_if(groups_.begin(), groups_.end(),
		[&](const FillGroup &grp) { return group == grp.getID(); });
    if (it == groups_.end()) {
	throw GroupNotFound(group);
    }

    thread group_thread(&Scheduler::Impl::groupThread, this, std::ref(circ),
			std::ref(*it));
    group_thread.detach();
}

Scheduler::NextGroupData Scheduler::Impl::next() const noexcept
{
    auto group_it = min_element(groups_.begin(), groups_.end(),
				[](const FillGroup &fg1, const FillGroup &fg2) {
				    return fg1.nextOffset() < fg2.nextOffset();
				});

    NextGroupData data{-1, -1};

    if (group_it != groups_.end()) {
	auto offset = group_it->nextOffset();
	log::next_offset(offset);
	data.group_id = group_it->getID();
	data.timeout_in_milliseconds =
	    std::chrono::duration_cast<std::chrono::milliseconds>(offset)
		.count();

    } else {
	log::no_next_offset();
    }

    return data;
}

Scheduler Scheduler::Impl::fromJson(const Json::Value &val)
{
    Scheduler scheduler(val["circuit_id"].asInt());

    if (!val["groups"].isArray()) {
	throw SchedulerMissingObject("groups");
    }

    for (const auto &grp : val["groups"]) {
	scheduler.impl_->groups_.emplace_back(FillGroup::fromJson(grp));
    }

    return scheduler;
}

Json::Value Scheduler::Impl::toJson() const
{
    Json::Value val(Json::objectValue);
    for (const auto &grp : groups_) {
	val["groups"].append(grp.toJson());
    }
    return val;
}

Scheduler Scheduler::Impl::fromConfigFile(int circuit)
{
    ifstream file(defaultConfigFile(), ios::in);
    if (!file.is_open()) {
	// file error
	throw SchedulerNoConfigFound(defaultConfigFile());
    }

    Json::Reader reader;
    Json::Value root = Json::Value(Json::ValueType::objectValue);
    if (!reader.parse(file, root)) {
	// parse error
	throw SchedulerConfigParserError(defaultConfigFile(), reader);
    }
    if (!root.isObject()) {
	throw SchedulerError("fromConfigFile: need object");
    }
    if (!root["schedulers"].isArray()) {
	throw SchedulerError("fromConfigFile: 'schedulers' = array not found");
    }

    auto it = find_if(root["schedulers"].begin(), root["schedulers"].end(),
		      [&](Json::Value &v) {
			  return v["circuit_id"].isInt() &&
				 (v["circuit_id"] == circuit);
		      });

    if (it == root["schedulers"].end()) {
	throw SchedulerError(string("fromConfigFile: cirucit not found, id: ") +
			     to_string(circuit));
    }

    return fromJson(*it);
}

void Scheduler::Impl::toConfigFile() const
{
    const char *path = defaultConfigFile();
    std::fstream file(path, std::ios::in);
    Json::Reader reader;
    Json::Value root = Json::Value(Json::ValueType::objectValue);

    if (!file.is_open()) {
	throw SchedulerNoConfigFound(path);
    }

    if (!reader.parse(file, root)) {
	throw SchedulerConfigParserError(path, reader);
    }
    file.close();

    Json::Value &schedulers = root["schedulers"];

    auto it = find_if(schedulers.begin(), schedulers.end(),
		      [&](Json::Value &scheduler) {
			  return (scheduler["circuit_id"].asInt() == id_);
		      });
    if (it != schedulers.end()) {
	Json::Value val;
	schedulers.removeIndex(std::distance(schedulers.begin(), it), &val);
    }

    schedulers.append(toJson());

    file.open(path, std::ios::out);
    Json::StyledStreamWriter writer;

    writer.write(file, root);
    file.close();
}

void Scheduler::Impl::groupThread(Circuit &circuit, const FillGroup grp)
{
    log::group_started(grp.getID());
    {
	lock_guard<std::mutex> lk(group_thr_mtx_);
	group_thr_active_ = true;
    }
    try {
	circuit.fill(cancel_group_thr_, grp);
    } catch (CircuitError &ex) {
	log::filling_failed(ex);
    }
    {
	lock_guard<std::mutex> lk(group_thr_mtx_);
	group_thr_active_ = false;
	cancel_group_thr_ = false;
    }
    if (cancel_group_thr_) {
	group_thr_cdt_.notify_one();
    }
    log::group_stopped(grp.getID());
}

const char *Scheduler::Impl::defaultConfigFile()
{
    return SCHEDULER_CONFIG_DIR "/scheduler.json";
}

////////////////////////////////////////////////////////////
//		Scheduler definitions
////////////////////////////////////////////////////////////

Scheduler::Scheduler(int circuit_id) : impl_(new Impl(circuit_id)) {}
Scheduler::~Scheduler() {}
Scheduler::Scheduler(Scheduler &&rhs) : impl_(std::move(rhs.impl_)) {}
Scheduler &Scheduler::operator=(Scheduler &&rhs)
{
    impl_ = std::move(rhs.impl_);
    return *this;
}

bool Scheduler::haveGroup(int group_id) const noexcept
{
    return impl_->haveGroup(group_id);
}

int Scheduler::addGroup() { return impl_->addGroup(); }
void Scheduler::rmGroup(int id) { impl_->rmGroup(id); }
const std::vector<FillGroup> &Scheduler::groups() const noexcept
{
    return impl_->groups();
}

void Scheduler::addFillTime(int grp, int h, int min)
{
    impl_->addFillTime(grp, h, min);
}

void Scheduler::addDetector(int grp, int detector, minutes timeout)
{
    impl_->addDetector(grp, detector, timeout);
}

void Scheduler::rmDetector(int d) { impl_->rmDetector(d); }
void Scheduler::rmDetector(int grp, int d) { impl_->rmDetector(grp, d); }
bool Scheduler::cancelFilling() noexcept { return impl_->cancelFilling(); }
void Scheduler::reload() { impl_->reload(); }
void Scheduler::clear() noexcept { impl_->clear(); }
void Scheduler::dump() noexcept { impl_->dump(); }
void Scheduler::fillGroup(Circuit &circ, int grp)
{
    impl_->fillGroup(circ, grp);
}

Scheduler::NextGroupData Scheduler::next() const noexcept
{
    return impl_->next();
}

Scheduler Scheduler::fromJson(const Json::Value &val)
{
    return Scheduler::Impl::fromJson(val);
}

Json::Value Scheduler::toJson() const { return impl_->toJson(); }
Scheduler Scheduler::fromConfigFile(int circuit)
{
    return Scheduler::Impl::fromConfigFile(circuit);
}

void Scheduler::toConfigFile() const { impl_->toConfigFile(); }
} // namespace CALF
