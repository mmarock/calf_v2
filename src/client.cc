

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>

#include <thread>

#include <sys/socket.h>
#include <sys/un.h>
#include <sys/unistd.h>

#include <iostream>
#include <string>

#include "../header/DescriptorGuard.hh"
#include "../header/ObjectSerialisation.hh"
#include "../header/Requests.hh"

#include <json/json.h>

using namespace CALF;

int main()
{
    Request::Ptr echo(new EchoRequest("Hallo Welt!"));

    Archive ar;

    ObjectSerialisation::serialise(echo, ar);

    echo = Request::Ptr(
	new NetDumpRequest(true, false, false, false, false, false));

    ObjectSerialisation::serialise(echo, ar);

    echo = Request::Ptr(new QuitRequest());

    ObjectSerialisation::serialise(echo, ar);

    DescriptorGuard fd(socket(AF_UNIX, SOCK_STREAM, 0));
    assert(fd.fd() > 0 && "socket() returned -1");

    struct sockaddr_un addr = {AF_UNIX, "/tmp/calf.socket"};
    int c(-1);
    if ((c = connect(fd.fd(), (struct sockaddr *)&addr, sizeof(addr))) < 0) {
	perror("connect()");
	return -1;
    }

    ar.exportToFD(fd.fd());

    std::cerr << "\nJetzt lese\n";

    std::vector<uint8_t> total;
    uint8_t buf[256];
    c = 0;
    while ((c = read(fd.fd(), buf, sizeof(buf))) > 0) {
	for (int i = 0; i < c; ++i) {
	    if (buf[i] == '\x04') {
		goto received_message;
	    } else {
		total.push_back(buf[i]);
	    }
	}
    }

received_message:

    try {
	Json::Value root(Json::objectValue);
	Json::Reader reader;

	reader.parse((const char *)&total.front(), (const char *)&total.back(),
		     root);

	Json::StyledStreamWriter writer;

	writer.write(std::cout, root);

    } catch (std::exception &ex) {
	std::cerr << "error caught: " << ex.what();
    }

    std::this_thread::sleep_for(std::chrono::seconds(3));

    return 0;
}
