

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>

#include "../header/Circuit.hh"
#include "../header/Detector.hh"
#include "../header/MainVent.hh"
#include "../header/NotificationSystem.hh"
#include "../header/ObjectSerialisation.hh"
#include "../header/RTDCalculator.hh"
#include "../header/Requests.hh"
#include "../header/USBEnvironment.hh"
#include "../header/ValveVent.hh"
#include "../header/logger.hh"

#include <unistd.h>
#include <cassert>

#include <json/json.h>

namespace CALF
{
////////////////////////////////////////////////////////////
//		log namespace
////////////////////////////////////////////////////////////

namespace log
{
template <class Exception>
static void serialisation_exception(const Exception &ex)
{
    logger::log(LOG_LEVEL::ALERT, "ObjectSerialisation: %s", ex.what());
}
}

////////////////////////////////////////////////////////////
//		Archive definition
////////////////////////////////////////////////////////////

class Archive::Impl
{
    Json::Value objects_;

    friend struct ObjectSerialisation;

    public:
    Impl();

    ~Impl();

    void clear();

    static bool importable(const Bytes &external_buffer) noexcept;
    void import(Bytes &b);
    void importByFD(int fd);

    Bytes exportBytes() noexcept;

    // gebe Objekt Typ des nächsten Elements an
    OBJECT extractable() noexcept;

    int exportToFD(int fd);

    Json::Value extractNextObject();
};

Archive::Impl::Impl() : objects_(Json::ValueType::arrayValue) {}
Archive::Impl::~Impl() {}
void Archive::Impl::clear() { objects_.clear(); }
bool Archive::Impl::importable(const Bytes &b) noexcept
{
    return std::find(b.begin(), b.end(), '\x04') != b.end();
}

void Archive::Impl::import(Bytes &b)
{
    auto it = std::find(b.begin(), b.end(), '\x04');

    try {
	Bytes buf;
	buf.insert(buf.end(), it + 1, b.end());
	b.erase(it, b.end());
	std::swap(b, buf);

	/*
	std::cerr << std::string("Bearbeite nun folgenden buffer:")
		  << std::string((const char *)buf.data(), buf.size())
		  << "ENDE\n";
	*/

	Json::Reader reader;
	Json::Value tmp(Json::arrayValue);
	if (!reader.parse((const char *)&buf.front(), (const char *)&buf.back(),
			  tmp, false)) {
	    // data is not good, discard
	    /**
	     * @todo wtf ich habe hier einfach gestoppt...
	     */
	}

	for (auto &val : tmp) {
	    objects_.append(val);
	}

    } catch (Json::Exception &ex) {
	log::serialisation_exception(ex);
    } catch (std::exception &ex) {
	log::serialisation_exception(ex);
    }
}

void Archive::Impl::importByFD(int fd)
{
    Bytes b;

    uint8_t buffer[256];
    constexpr ssize_t buffer_len = sizeof(buffer);
    int c(-1);
    // if c == buffer, the last read returned less bytes than could be
    // stored
    while (c < buffer_len && (c = read(fd, buffer, buffer_len)) > 0) {
	// c: was wirklich gelesen wurde
	for (ssize_t i = 0; i < c; ++i) {
	    b.push_back(buffer[i]);
	    if (buffer[i] == '\x04') {
		goto import_by_fd_found_eot;
	    }
	}
    }

    if (c <= 0) {
	/**
	 * @todo schreibe hier eine sinnvolle Fehlermeldung, read <=
	 * deutet auf einen Verbindungsfehler hin
	 */
	return;
    }

import_by_fd_found_eot:

    import(b);
}

Bytes Archive::Impl::exportBytes() noexcept
{
    Bytes b;
    try {
	Json::FastWriter w;
	auto str = w.write(objects_);
	b.insert(b.end(), str.begin(), str.end());
	objects_.clear();
    } catch (Json::Exception &ex) {
	log::serialisation_exception(ex);
    }

    b.push_back('\x04');
    return b;
}

Archive::OBJECT Archive::Impl::extractable() noexcept
{
    // TODO JSON Exceptions!!!
    if (objects_.size() == 0) {
	return OBJECT::NONE;
    }

    try {
	auto first = objects_[0];
	assert(first.isObject() && "first object is no Json::Object");

	auto type = first["type"];
	if (type.isString()) {
	    if (!type.asString().compare("CIRCUIT")) {
		return OBJECT::CIRCUIT;
	    } else if (!type.asString().compare("MAIN_VENT")) {
		return OBJECT::MAIN_VENT;
	    } else if (!type.asString().compare("REQUEST")) {
		return OBJECT::REQUEST;
	    }
	}
    } catch (Json::Exception &ex) {
	log::serialisation_exception(ex);
    }
    return OBJECT::NONE;
}

int Archive::Impl::exportToFD(int fd)
{
    auto bytes = exportBytes();

    size_t written(0);
    int c(0);
    while ((c = write(fd, bytes.data() + written, bytes.size() - written)) >
	   0) {
	written += c;
    }
    return (c < 0) ? c : written;
}

Json::Value Archive::Impl::extractNextObject()
{
    // TODO JSON Exceptions!!!
    Json::Value buf;
    objects_.removeIndex(0, &buf);
    return buf;
}

Archive::Archive() : impl_(new Impl()) {}
Archive::~Archive() {}
void Archive::clear() { impl_->clear(); }
bool Archive::importable(const Bytes &b) noexcept
{
    return Impl::importable(b);
}

void Archive::import(Bytes &b) { impl_->import(b); }
void Archive::importByFD(int fd) { impl_->importByFD(fd); }
Bytes Archive::exportBytes() noexcept { return impl_->exportBytes(); }
int Archive::exportToFD(int fd) { return impl_->exportToFD(fd); }
Archive::OBJECT Archive::extractable() noexcept { return impl_->extractable(); }
////////////////////////////////////////////////////////////
//		ObjectSerialisation definition
////////////////////////////////////////////////////////////

int ObjectSerialisation::serialise(const Request::Ptr &r, Archive &ar) noexcept
{
    // TODO JSON Exceptions!!!
    assert(r && "r is NULL");

    try {
	Json::Value request = r->toJson();
	request["type"] = "REQUEST";

	ar.impl_->objects_.append(request);

	return ar.impl_->objects_.size();
    } catch (Json::Exception &ex) {
	log::serialisation_exception(ex);
	return -1;
    }
}

Request::Ptr ObjectSerialisation::constructRequest(Archive &ar)
{
    try {
	/*
	std::cerr << "constructRequest(), habe eine Queue von x = "
		  << ar.impl_->objects_.size() << std::endl;
	*/
	// is Json::Value
	auto request = ar.impl_->extractNextObject();

	if (!request.isObject() || request["type"].compare("REQUEST") ||
	    !request["request_type"].isString()) {
	    return nullptr;
	}

	return Request::fromJson(request);
    } catch (Json::Exception &ex) {
	log::serialisation_exception(ex);
	return nullptr;
    }
}

int ObjectSerialisation::serialise(const Circuit &circuit, Archive &ar) noexcept
{
    try {
	Json::Value val(Json::objectValue);
	val["type"] = "CIRCUIT";

	val["data"] = circuit.toJson();

	ar.impl_->objects_.append(val);
	return ar.impl_->objects_.size();
    } catch (Json::Exception &ex) {
	log::serialisation_exception(ex);
	return -1;
    }
}

int ObjectSerialisation::serialise(STATIC s, Archive &ar) noexcept
{
    try {
	Json::Value val(Json::objectValue);

	switch (s) {
	case STATIC::USBENVIRONMENT:
	    val = USBEnvironment::toJson();
	    val["type"] = "USBENVIRONMENT";
	    break;
	case STATIC::RTDCALCULATOR:
	    val = RTDCalculatorFactory::toJson();
	    val["type"] = "RTDCALCULATOR";
	    break;
	};
	ar.impl_->objects_.append(val);
	return ar.impl_->objects_.size();

    } catch (Json::Exception &ex) {
	log::serialisation_exception(ex);
	return -1;
    }
}

} // namespace CALF
