

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../header/USBEnvironment.hh"

#include <algorithm>
#include <fstream>
#include <mutex>
#include <vector>

#include <cassert>

#include "../header/RTDCalculator.hh"
#include "../header/Temperature.hh"
#include "../header/logger.hh"
#include "config.h"

#include <linux/hiddev.h>

#include <json/json.h>
#include "../header/USBTemp.hh"

namespace CALF
{
////////////////////////////////////////////////////////////
//		log namespace
////////////////////////////////////////////////////////////

namespace log
{
void function_call(const char *name)
{
    logger::log(LOG_LEVEL::DEBUG, "[ usbenvironment ] %s", name);
}

void try_associate(const char *serial, int id)
{
    logger::log(LOG_LEVEL::DEBUG, "[ usbenvironment ] associate(dev:%s, id:%d)",
		serial, id);
}

void usb_error(const char *error)
{
    logger::log(LOG_LEVEL::ALERT, "[ usbenvironment ] %s", error);
}

void system_error(const char *error)
{
    logger::log(LOG_LEVEL::ALERT, "[ usbenvironment ] %s", error);
}

} // namespace log

////////////////////////////////////////////////////////////
//		USBEnvironmentError declaration
////////////////////////////////////////////////////////////

USBEnvironmentError::USBEnvironmentError(const char *msg)
    : msg_(std::string("USBEnvironmentError: ") + msg)
{
}
USBEnvironmentError::USBEnvironmentError(const std::string &msg)
    : msg_(std::string("USBEnvironmentError: ") + msg)
{
}

class USBError : public USBEnvironmentError
{
    public:
    explicit USBError(int id)
	: USBEnvironmentError(std::string("USBError: id:") +
			      std::to_string(id) + ": not found")
    {
    }
    USBError(const USBTempError &ex) : USBEnvironmentError(ex.what()) {}
};

class FileError : public USBEnvironmentError
{
    public:
    enum ERROR : uint8_t { OPEN_FAILED, PARSING_FAILED, JSON_INCOMPLETE };

    const char *error(ERROR e)
    {
	switch (e) {
	case ERROR::OPEN_FAILED:
	    return "open failed";
	case ERROR::PARSING_FAILED:
	    return "parsing failed";
	case ERROR::JSON_INCOMPLETE:
	    return "json incomplete: ";
	};
    }

    FileError(ERROR err)
	: USBEnvironmentError(std::string("FileError: ") + error(err))
    {
    }
    FileError(const Json::Exception &ex) : USBEnvironmentError(ex.what()) {}
};

class SensorError : public USBEnvironmentError
{
    public:
    SensorError(const RTDCalculatorError &ex) : USBEnvironmentError(ex.what())
    {
    }
};

const char *USBEnvironmentError::what() const noexcept { return msg_.c_str(); }
////////////////////////////////////////////////////////////
//		USBEnvironment::Impl declaration
////////////////////////////////////////////////////////////

class USBEnvironment::Impl
{
    struct USBDev {
	int id;
	USBTemp usb;
	std::mutex mtx;

	USBDev(int usb, const std::string &serial) : id(usb), usb(serial) {}
	USBDev(USBDev &&rhs) : id(rhs.id), usb(std::forward<USBTemp>(rhs.usb))
	{
	}
	USBDev &operator=(USBDev &&rhs)
	{
	    id = rhs.id;
	    usb = std::move(rhs.usb);
	    return *this;
	}
    };

    RTDCalculator calc_;

    const std::vector<USBTemp> usb_temps_;

    std::mutex devs_mtx_;
    std::vector<USBDev> devs_;

    public:
    Impl();
    ~Impl();

    bool haveUSB(int id) noexcept;

    void usePT100Sensor() { calc_ = RTDCalculatorFactory::getRTDCalc("pt100"); }
    void usePT1000Sensor()
    {
	calc_ = RTDCalculatorFactory::getRTDCalc("pt1000");
    }

    void associate(const std::string &serial, int usb_id);

    void clear();
    void reload();
    void loadUSBEnvironment();

    void open(int usb, uint8_t channel);
    void close(int usb, uint8_t channel);

    Temperature temperature(int usb, uint8_t channel);

    Json::Value toJson() const;

    static const char *default_path() noexcept
    {
	return USB_ENVIRONMENT_CONFIG_DIR "/usb_environment.json";
    }

}; // class USBEnvironment::Impl

static std::vector<USBTemp> get_usb_temps()
{
    std::vector<USBTemp> data;
    try {
	for (const auto &device : USBTemp::findDevices()) {
	    data.emplace_back(device.device);
	}

    } catch (const USBTempError &ex) {
	log::usb_error(ex.what());
    } catch (const std::exception &ex) {
	log::system_error(ex.what());
    }
    return data;
}

USBEnvironment::Impl::Impl() : calc_(), usb_temps_(get_usb_temps())
{
    try {
	calc_ = RTDCalculatorFactory::getRTDCalc("pt100");
    } catch (const RTDCalculatorError &ex) {
	throw SensorError(ex);
    }
}

USBEnvironment::Impl::~Impl() {}
bool USBEnvironment::Impl::haveUSB(int id) noexcept
{
    return std::find_if(devs_.begin(), devs_.end(),
			[&](const struct USBDev &d) { return d.id == id; }) !=
	   devs_.end();
}

void USBEnvironment::Impl::associate(const std::string &serial, int usb)
{
    log::try_associate(serial.c_str(), usb);
    try {
	if (std::find_if(devs_.begin(), devs_.end(),
			 [&](const struct USBDev &d) -> bool {
			     return d.id == usb;
			 }) != devs_.end()) {
	    throw USBEnvironmentError("id already used");
	}

	auto it = std::find_if(usb_temps_.begin(), usb_temps_.end(),
			       [&](const USBTemp &temp) {
				   return temp.getSerialNumber() == serial;
			       });
	if (it == usb_temps_.end()) {
	    throw USBEnvironmentError("usb device not found");
	}

	USBDev dev(usb, it->getDevice());

	dev.usb.dioConfig(USBTemp::DIO_DIRECTION::OUT);
	dev.usb.dioSet('\xff'); // set all to true

	for (uint8_t ch = USBTemp::CHANNEL_0; ch <= USBTemp::CHANNEL_7; ch++) {
	    dev.usb.setSensorType(static_cast<USBTemp::CHANNEL>(ch),
				  USBTemp::SENSOR_TYPE::THERMISTOR);
	    dev.usb.setSensorConnection(
		static_cast<USBTemp::CHANNEL>(ch),
		USBTemp::CONNECTION::TWO_WIRE_TWO_SENSOR);
	}

	std::lock_guard<std::mutex> lk(devs_mtx_);
	devs_.emplace_back(std::move(dev));

    } catch (const std::exception &ex) {
	throw USBEnvironmentError(ex.what());
    }
}

void USBEnvironment::Impl::clear()
{
    std::lock_guard<std::mutex> lk(devs_mtx_);
    devs_.clear();
}

void USBEnvironment::Impl::reload()
{
    // std::vector<USBDev> devs;

    assert(false && "implement USBEnvironment::reload()");
    // std::swap(devs_,devs);
}

void USBEnvironment::Impl::loadUSBEnvironment()
{
    log::function_call(__PRETTY_FUNCTION__);

    std::ifstream file(default_path(), std::ios::in);
    if (!file.is_open()) {
	throw FileError(FileError::OPEN_FAILED);
    }
    Json::Reader reader;

    Json::Value root(Json::objectValue);
    if (!reader.parse(file, root, true)) {
	throw FileError(FileError::PARSING_FAILED);
    }

    file.close();

    try {
	for (const auto &dev : root) {
	    associate(dev["serial"].asString(), dev["id"].asInt());
	}

    } catch (const USBTempError &ex) {
	throw USBError(ex);
    } catch (const Json::Exception &ex) {
	throw FileError(ex);
    }
}

void USBEnvironment::Impl::open(int usb, uint8_t ch)
{
    assert(ch < 8 && "invalid channel");
    auto it = std::find_if(
	devs_.begin(), devs_.end(),
	[&](const struct USBDev &dev) -> bool { return dev.id == usb; });

    if (it == devs_.end()) {
	throw USBError(usb);
    }

    try {
	std::lock_guard<std::mutex> lk(it->mtx);
	it->usb.dioBitSet(ch, false);
    } catch (const USBTempError &ex) {
	throw USBError(ex);
    }
}

void USBEnvironment::Impl::close(int usb, uint8_t ch)
{
    assert(ch < 8 && "invalid channel");
    auto it = std::find_if(
	devs_.begin(), devs_.end(),
	[&](const struct USBDev &dev) -> bool { return dev.id == usb; });

    if (it == devs_.end()) {
	throw USBError(usb);
    }

    try {
	std::lock_guard<std::mutex> lk(it->mtx);
	it->usb.dioBitSet(ch, true);
    } catch (const USBTempError &ex) {
	throw USBError(ex);
    }
}

Temperature USBEnvironment::Impl::temperature(int usb, uint8_t ch)
{
    assert(ch < 8 && "invalid channel");
    auto it = std::find_if(
	devs_.begin(), devs_.end(),
	[&](const struct USBDev &dev) -> bool { return dev.id == usb; });

    if (it == devs_.end()) {
	throw USBError(usb);
    }

    try {
	std::lock_guard<std::mutex> lk(it->mtx);
	float temp =
	    it->usb.getTemperature((USBTemp::CHANNEL)ch, USBTemp::UNIT::RAW);
	return calc_(temp);
    } catch (const USBTempError &ex) {
	throw USBError(ex);
    } catch (const RTDCalculatorError &ex) {
	throw SensorError(ex);
    }
}

Json::Value USBEnvironment::Impl::toJson() const
{
    log::function_call(__PRETTY_FUNCTION__);

    try {
	Json::Value root(Json::objectValue);

	Json::Value devices(Json::arrayValue);
	for (const auto &dev : devs_) {
	    Json::Value val(Json::objectValue);

	    val["serial"] = dev.usb.getSerialNumber();
	    val["id"] = dev.id;
	    // TODO sensor type

	    devices.append(val);
	}

	root["devices"] = devices;
	return root;

    } catch (const Json::Exception &ex) {
	throw USBEnvironmentError(ex.what());
    }
}

////////////////////////////////////////////////////////////
//		USBEnvironment declaration
////////////////////////////////////////////////////////////

USBEnvironment::Impl &USBEnvironment::getImpl()
{
    static Impl i;
    return i;
}

bool USBEnvironment::haveUSB(int id) noexcept { return getImpl().haveUSB(id); }
void USBEnvironment::usePT100Sensor() { getImpl().usePT100Sensor(); }
void USBEnvironment::usePT1000Sensor() { getImpl().usePT1000Sensor(); }
void USBEnvironment::associate(const std::string &serial, int usb_id)
{
    getImpl().associate(serial, usb_id);
}

void USBEnvironment::loadUSBEnvironment() { getImpl().loadUSBEnvironment(); }
void USBEnvironment::clear() { getImpl().clear(); }
void USBEnvironment::reload() { getImpl().reload(); }
void USBEnvironment::open(int id, uint8_t ch) { getImpl().open(id, ch); }
void USBEnvironment::close(int id, uint8_t ch) { getImpl().close(id, ch); }
Temperature USBEnvironment::temperature(int usb, uint8_t ch)
{
    return getImpl().temperature(usb, ch);
}

Json::Value USBEnvironment::toJson() { return getImpl().toJson(); }
} // namespace CALF
