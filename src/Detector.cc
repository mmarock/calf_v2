

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../header/Detector.hh"

#include <cassert>

#include <json/json.h>

namespace CALF
{
////////////////////////////////////////////////////////////
//		DetectorError definitions
////////////////////////////////////////////////////////////

DetectorError::DetectorError(const std::string &msg)
    : msg_(std::string("DetectorError: ") + msg)
{
}

const char *DetectorError::what() const noexcept { return msg_.c_str(); }
////////////////////////////////////////////////////////////
//		Detector definitions
////////////////////////////////////////////////////////////

using seconds = std::chrono::seconds;

Detector::Detector(int id, int vent_usb, uint8_t vent_channel)
    : id_(id), vent_usb_(vent_usb), vent_channel_(vent_channel), inlet_usb_(-1),
      outlet_usb_(-1), is_active_(true),
      allowed_time_duration_(std::make_pair(seconds(360), seconds(1800)))
{
    assert(vent_channel_ < 8 && "channel must be < 8");
}

int Detector::id() const noexcept { return id_; }
int Detector::getVentUSB() const { return vent_usb_; }
uint8_t Detector::getVentChannel() const { return vent_channel_; }
void Detector::setInlet(int usb, uint8_t ch, const Temperature &t) noexcept
{
    assert(ch < 8 && "channel must be < 8");
    inlet_usb_ = usb;
    inlet_ch_ = ch;
    inlet_trigger_temp_ = t;
}

bool Detector::hasInlet() const { return inlet_usb_ != -1; }
int Detector::getInletUSB() const { return inlet_usb_; }
uint8_t Detector::getInletChannel() const { return inlet_ch_; }
void Detector::setOutlet(int usb, uint8_t ch, const Temperature &t) noexcept
{
    assert(ch < 8 && "channel must be < 8");
    outlet_usb_ = usb;
    outlet_ch_ = ch;
    outlet_trigger_temp_ = t;
}

bool Detector::hasOutlet() const { return outlet_usb_ != -1; }
int Detector::getOutletUSB() const { return outlet_usb_; }
uint8_t Detector::getOutletChannel() const { return outlet_ch_; }
const Temperature &Detector::getInletTrigger() const
{
    return inlet_trigger_temp_;
}

void Detector::setInletTrigger(const Temperature &T)
{
    inlet_trigger_temp_ = T;
}

const Temperature &Detector::getOutletTrigger() const
{
    return outlet_trigger_temp_;
}

void Detector::setOutletTrigger(const Temperature &T)
{
    outlet_trigger_temp_ = T;
}

bool Detector::isActive() const { return is_active_; }
bool Detector::setActive(bool state)
{
    std::swap(state, is_active_);
    return is_active_;
}

const std::pair<seconds, seconds> Detector::getAllowedTimeDuration() const
{
    return allowed_time_duration_;
}

void Detector::setAllowedTimeDuration(seconds min, seconds max)
{
    assert(min < max);
    allowed_time_duration_ = {min, max};
}

std::chrono::time_point<std::chrono::system_clock> Detector::touch()
{
    auto prev_last_changed = last_changed_;
    last_changed_ = std::chrono::system_clock::now();
    return prev_last_changed;
}

Json::Value Detector::toJson() const
{
    try {
	Json::Value val(Json::objectValue);

	val["id"] = id_;
	val["vent_usb"] = vent_usb_;
	val["vent_channel"] = vent_channel_;
	val["is_active"] = is_active_;

	val["inlet_usb"] = inlet_usb_;
	val["inlet_channel"] = inlet_ch_;
	val["inlet_trigger"] = inlet_trigger_temp_.inCelsius();

	val["outlet_usb"] = outlet_usb_;
	val["outlet_channel"] = outlet_ch_;
	val["outlet_trigger"] = outlet_trigger_temp_.inCelsius();

	val["allowed_time_duration_min"] =
	    (int)allowed_time_duration_.first.count();
	val["allowed_time_duration_max"] =
	    (int)allowed_time_duration_.second.count();

	return val;

    } catch (const Json::Exception &ex) {
	throw DetectorError(ex.what());
    }
}

Detector Detector::fromJson(const Json::Value &val)
{
    try {
	const auto &id = val["id"];
	const auto &vent_usb = val["vent_usb"];
	const auto &vent_channel = val["vent_channel"];
	const auto &is_active = val["is_active"];

	const auto &inlet_usb = val["inlet_usb"];
	const auto &inlet_channel = val["inlet_channel"];
	const auto inlet_trigger = val["inlet_trigger"];

	const auto &outlet_usb = val["outlet_usb"];
	const auto &outlet_channel = val["outlet_channel"];
	const auto outlet_trigger = val["outlet_trigger"];

	const auto &allowed_time_duration_min =
	    val["allowed_time_duration_min"];
	const auto &allowed_time_duration_max =
	    val["allowed_time_duration_max"];

	Detector d(id.asInt(), vent_usb.asInt(), vent_channel.asUInt());

	d.setInlet(inlet_usb.asInt(), inlet_channel.asUInt(),
		   Temperature::fromCelsius(inlet_trigger.asDouble()));
	d.setOutlet(outlet_usb.asInt(), outlet_channel.asUInt(),
		    Temperature::fromCelsius(outlet_trigger.asDouble()));

	d.setAllowedTimeDuration(seconds(allowed_time_duration_min.asUInt()),
				 seconds(allowed_time_duration_max.asUInt()));

	d.setActive(is_active.asBool());

	return d;

    } catch (const Json::Exception &ex) {
	throw DetectorError(ex.what());
    }
}

} // namespace CALF
