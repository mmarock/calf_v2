

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../header/NotificationSystem.hh"

#include "../header/FillReport.hh"
#include "config.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <regex>

#include <cassert>

#include <libesmtp.h>

#include <json/json.h>

using namespace std;

namespace CALF
{
////////////////////////////////////////////////////////////
//		Exceptions
////////////////////////////////////////////////////////////

NotificationSystemError::NotificationSystemError(const char *msg)
    : msg_(string("NotificationSystemError: ") + msg)
{
}
NotificationSystemError::NotificationSystemError(const string &msg)
    : msg_(string("NotificationSystemError: ") + move(msg))
{
}

const char *NotificationSystemError::what() const noexcept
{
    return msg_.c_str();
}

struct libESMTPError : public NotificationSystemError {
    explicit libESMTPError(const std::string &what_arg);
    explicit libESMTPError(const char *what_arg);
};

struct ConfigError : public NotificationSystemError {
    explicit ConfigError(const std::string &what_arg);
    explicit ConfigError(const char *what_arg);
};

libESMTPError::libESMTPError(const string &what_arg)
    : NotificationSystemError(string("libESMTPError: ") + what_arg)
{
}

libESMTPError::libESMTPError(const char *what_arg)
    : NotificationSystemError(string("libESMTPError: ") + what_arg)
{
}

ConfigError::ConfigError(const string &what_arg)
    : NotificationSystemError(string("ConfigError: ") + what_arg)
{
}

ConfigError::ConfigError(const char *what_arg)
    : NotificationSystemError(string("ConfigError: ") + what_arg)
{
}

struct NoSysMissingObject : public NotificationSystemError {
    explicit NoSysMissingObject(const char *obj)
	: NotificationSystemError(string("NoSysMissingObject: ") + obj)
    {
    }
};

struct NoSysFileNotFound : public NotificationSystemError {
    explicit NoSysFileNotFound()
	: NotificationSystemError(string("NoSysFileNotFound: ") +
				  NotificationSystem::getDefaultConfigPath())
    {
    }
};

struct NoSysParserError : public NotificationSystemError {
    explicit NoSysParserError(Json::Reader &reader)
	: NotificationSystemError(string("NoSysParserError: ") +
				  reader.getFormattedErrorMessages())
    {
    }
};

////////////////////////////////////////////////////////////
//		NotificationSystem definitions
////////////////////////////////////////////////////////////

NotificationSystem NotificationSystem::fromConfigFile()
{
    using namespace Json;

    ifstream file(getDefaultConfigPath());
    if (!file.is_open()) {
	throw NoSysFileNotFound();
    }

    Value document(ValueType::objectValue);
    Reader reader;
    if (!reader.parse(file, document)) {
	throw NoSysParserError(reader);
    }

    return NotificationSystem::fromJson(document);
}

NotificationSystem::NotificationSystem(const string &server,
				       vector<Recipient> &&recipients,
				       const string &reverse_path)
    : server_(server), reverse_path_(reverse_path),
      recipients_(move(recipients))
{
}

void NotificationSystem::addRecipient(const string &mail, bool verbose_msg)
{
    auto obj =
	find_if(recipients_.begin(), recipients_.end(),
		[&](const Recipient &r) -> bool { return (r.first == mail); });
    if (obj == recipients_.end()) {
	recipients_.emplace_back(mail, verbose_msg);
    } else {
	obj->second = verbose_msg;
    }
}

void NotificationSystem::removeRecipient(const string &mail)
{
    auto obj =
	find_if(recipients_.begin(), recipients_.end(),
		[&](const Recipient &r) -> bool { return (r.first == mail); });
    if (obj != recipients_.end()) {
	recipients_.erase(obj);
    } else {
	throw runtime_error("FillReportMail::removeRecipient(): ref not found");
    }
}

vector<NotificationSystem::Recipient>::iterator NotificationSystem::begin()
{
    return recipients_.begin();
}

vector<NotificationSystem::Recipient>::iterator NotificationSystem::end()
{
    return recipients_.end();
}

vector<NotificationSystem::Recipient>::const_iterator
NotificationSystem::cbegin() const
{
    return recipients_.cbegin();
}

vector<NotificationSystem::Recipient>::const_iterator
NotificationSystem::cend() const
{
    return recipients_.cend();
}

vector<NotificationSystem::Recipient>::reverse_iterator
NotificationSystem::rbegin()
{
    return recipients_.rbegin();
}

vector<NotificationSystem::Recipient>::reverse_iterator
NotificationSystem::rend()
{
    return recipients_.rend();
}

vector<NotificationSystem::Recipient>::const_reverse_iterator
NotificationSystem::crbegin() const
{
    return recipients_.crbegin();
}

vector<NotificationSystem::Recipient>::const_reverse_iterator
NotificationSystem::crend() const
{
    return recipients_.crend();
}

bool NotificationSystem::sendNotifications(FillReport &note)
{
    if (recipients_.empty()) {
	return false;
    }

    string msg;
    if (needsVerboseEMail()) {
	msg = note.createLongFillReport();
	sendEMail(msg, true);
    }

    if (needsNonVerboseEMail()) {
	msg = note.createShortFillReport();
	sendEMail(msg, false);
    }

    return true;
}

void NotificationSystem::reloadConfigFile()
{
    using namespace Json;

    ifstream file(getDefaultConfigPath());
    if (!file.is_open()) {
	throw NoSysFileNotFound();
    }

    Value document(ValueType::objectValue);
    Reader reader;
    if (!reader.parse(file, document)) {
	file.close();
	throw NoSysParserError(reader);
    }
    file.close();

    // is open, now fetch values
    reload(document);
}

NotificationSystem NotificationSystem::fromJson(const Json::Value &val)
{
    assert(val.isObject());
    auto &server = val["smtp_server"];
    if (!server.isString()) {
	throw NoSysMissingObject("smtp_server");
    }
    auto &reverse_path = val["reverse_path"];

    vector<Recipient> recipients;
    for (auto &rec : val["recipients"]) {
	if (!rec.isString()) {
	    throw NoSysMissingObject("recipient");
	}
	recipients.emplace_back(rec.asString(), true);
    }
    for (auto &rec : val["short_recipients"]) {
	if (!rec.isString()) {
	    throw NoSysMissingObject("recipient");
	}
	recipients.emplace_back(rec.asString(), false);
    }
    if (recipients.empty()) {
	throw ConfigError("no recipients found");
    }
    return (reverse_path.isString())
	       ? NotificationSystem(server.asString(), move(recipients),
				    reverse_path.asString())
	       : NotificationSystem(server.asString(), move(recipients));
}

Json::Value NotificationSystem::toJson() const
{
    Json::Value nsys(Json::ValueType::objectValue);

    nsys["server"] = server_;
    nsys["reverse_path"] = reverse_path_;
    auto &recipients =
	(nsys["recipients"] = Json::Value(Json::ValueType::arrayValue));
    for (auto &recipient : recipients_) {
	Json::Value entry(Json::ValueType::objectValue);
	entry["recipient"] = recipient.first;
	entry["verbose"] = recipient.second;
	recipients.append(entry);
    }
    return nsys;
}

void NotificationSystem::reload(const Json::Value &document)
{
    if (!document["smtp_server"].isString()) {
	throw NoSysMissingObject("smtp_server");
    }
    string smtp_server = document["smtp_server"].asString();

    if (!document["recipients"].isArray() ||
	!document["short_recipients"].isArray()) {
	throw NoSysMissingObject("recipients/short_recipients");
    }
    string reverse_path = (document["reverse_path"].isString())
			      ? document["reverse_path"].asString()
			      : getDefaultReversePath();

    vector<Recipient> recipients;
    for (auto &rec : document["recipients"]) {
	if (!rec.isString()) {
	    throw NoSysMissingObject("recipient");
	}
	recipients.emplace_back(rec.asString(), true);
    }
    for (auto &rec : document["short_recipients"]) {
	if (!rec.isString()) {
	    throw NoSysMissingObject("recipient");
	}
	recipients.emplace_back(rec.asString(), false);
    }
    if (recipients_.empty()) {
	throw ConfigError("no recipients found");
    }

    // these are noexcept
    swap(smtp_server, server_);
    swap(reverse_path, reverse_path_);
    swap(recipients, recipients_);
}

const char *NotificationSystem::getDefaultConfigPath()
{
    return NOTIFICATION_CONFIG_DIR "/notification.config";
}

const string &NotificationSystem::getDefaultReversePath()
{
    static const string s("\"CALF Fuellanlage\" <noreply@ikp.uni-koeln.de>");
    return s;
}

bool NotificationSystem::needsVerboseEMail() const
{
    return (find_if(recipients_.begin(), recipients_.end(),
		    [](const Recipient &r) { return r.second; }) !=
	    recipients_.end());
}

bool NotificationSystem::needsNonVerboseEMail() const
{
    return (find_if_not(recipients_.begin(), recipients_.end(),
			[](const Recipient &r) { return r.second; }) !=
	    recipients_.end());
}

bool set_recipient(smtp_message_t msg, const NotificationSystem::Recipient &r)
{
    return (smtp_add_recipient(msg, r.first.c_str()));
}

bool add_to_header(smtp_message_t msg, const NotificationSystem::Recipient &r)
{
    static const regex name_regex("^\"(.*)\" <", regex_constants::basic);
    static const regex mail_regex(" <(.*)>$", regex_constants::basic);

    smatch name_match;
    smatch mail_match;

    regex_search(r.first, name_match, name_regex);
    regex_search(r.first, mail_match, mail_regex);

    if (name_match.size() != 2 || mail_match.size() != 2) {
	return false;
    }

    string name(name_match[1]);
    string mail(mail_match[1]);

    return (smtp_set_header(msg, "To", name.c_str(), mail.c_str()));
}

void NotificationSystem::sendEMail(const string &msg_str,
				   bool send_verbose) const
{
    smtp_session_t session = smtp_create_session();
    if (!session) {
	char buf[128];
	throw libESMTPError(string("smtp_create_session failed: ") +
			    smtp_strerror(smtp_errno(), buf, 128));
    }

    if (!smtp_set_server(session, server_.c_str())) {
	smtp_destroy_session(session);
	char buf[128];
	throw libESMTPError(string("smtp_set_server failed: ") +
			    smtp_strerror(smtp_errno(), buf, 128));
    }

    smtp_message_t msg = smtp_add_message(session);
    if (!msg) {
	smtp_destroy_session(session);
	throw libESMTPError("smtp_add_message failed");
    }

    if (!smtp_set_reverse_path(msg, reverse_path_.c_str())) {
	smtp_destroy_session(session);
	throw libESMTPError("smtp_set_reverse_path failed");
    }

    for (auto &rec : recipients_) {
	if (rec.second != send_verbose) {
	    continue;
	}
	if (!add_to_header(msg, rec)) {
	    smtp_destroy_session(session);
	    throw runtime_error("NotificationSystem::sendEmail: "
				"add_to_header failed");
	}
	if (!set_recipient(msg, rec)) {
	    smtp_destroy_session(session);
	    char buf[128];
	    throw libESMTPError(string("set_recipient failed: ") +
				smtp_strerror(smtp_errno(), buf, 128));
	}
    }

    // the smtp_message_str is a macro which demands a void pointer!
    if (!smtp_set_message_str(msg, (void *)msg_str.c_str())) {
	smtp_destroy_session(session);
	throw libESMTPError("smtp_set_message_str failed");
    }

    if (!smtp_start_session(session)) {
	smtp_destroy_session(session);
	char buf[128];
	throw libESMTPError(string("smtp_start_session failed: ") +
			    smtp_strerror(smtp_errno(), buf, 128));
    }

    smtp_destroy_session(session);
}

} // namespace CALF
