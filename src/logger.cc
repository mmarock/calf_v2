

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../header/logger.hh"

#include <syslog.h>
#include <cstdarg>
#include <cstdlib>

namespace CALF
{
////////////////////////////////////////////////////////////
//		init static member variables
////////////////////////////////////////////////////////////
LOG_LEVEL logger::log_level_ = LOG_LEVEL::NOTICE;
bool logger::logger_initialized_ = false;

void logger::log(LOG_LEVEL lvl, const char *fmt, ...)
{
    if (!logger::logger_initialized_) {
	initLogger();
    }
    if (lvl <= log_level_) {
	va_list ap;
	va_start(ap, fmt);
	vsyslog(getSyslogLevel(lvl), fmt, ap);
	va_end(ap);
    }
}

void logger::setLogLevel(LOG_LEVEL lvl) { log_level_ = lvl; }
LOG_LEVEL logger::getLogLevel() { return log_level_; }
void logger::initLogger()
{
    openlog("calf", 0, LOG_DAEMON);
    atexit(closelog);

    logger::logger_initialized_ = true;
}

int logger::getSyslogLevel(LOG_LEVEL lvl)
{
    switch (lvl) {
    case LOG_LEVEL::EMERG:
	return LOG_EMERG;
    case LOG_LEVEL::ALERT:
	return LOG_ALERT;
    case LOG_LEVEL::CRIT:
	return LOG_CRIT;
    case LOG_LEVEL::ERR:
	return LOG_ERR;
    case LOG_LEVEL::WARNING:
	return LOG_WARNING;
    case LOG_LEVEL::NOTICE:
	return LOG_NOTICE;
    case LOG_LEVEL::INFO:
	return LOG_INFO;
    case LOG_LEVEL::DEBUG:
	return LOG_DEBUG;
    }
}
}
