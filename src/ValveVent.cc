

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../header/ValveVent.hh"

#include <cassert>

#include <json/json.h>

using namespace std;

namespace CALF
{
////////////////////////////////////////////////////////////
//		ValveVentError definitions
////////////////////////////////////////////////////////////

ValveVentError::ValveVentError(const std::string &msg)
    : msg_(std::string("ValveVentError: ") + msg)
{
}

const char *ValveVentError::what() const noexcept { return msg_.c_str(); }
////////////////////////////////////////////////////////////
//		ValveVent definitions
////////////////////////////////////////////////////////////

ValveVent::ValveVent(int id, int vent_usb, uint8_t vent_channel)
    : id_(id), vent_usb_(vent_usb), vent_channel_(vent_channel),
      sensor_usb_(-1), allowed_time_duration_{seconds(10), seconds(600)}
{
}

int ValveVent::id() const noexcept { return id_; }
int ValveVent::getVentUSB() const noexcept { return vent_usb_; }
uint8_t ValveVent::getVentChannel() const noexcept { return vent_channel_; }
void ValveVent::setSensor(int usb, uint8_t ch, const Temperature &t) noexcept
{
    sensor_usb_ = usb;
    sensor_channel_ = ch;
    trigger_temp_ = t;
}

bool ValveVent::haveSensor() const noexcept { return (sensor_usb_ != -1); }
int ValveVent::getSensorUSB() const noexcept
{
    assert(sensor_usb_ != -1 && "Use a hasSensor() if you are not sure!");
    return sensor_usb_;
}

uint8_t ValveVent::getSensorChannel() const noexcept { return sensor_channel_; }
const Temperature &ValveVent::getTrigger() const noexcept
{
    return trigger_temp_;
}

void ValveVent::setTrigger(const Temperature &T) noexcept { trigger_temp_ = T; }
const ValveVent::duration_range ValveVent::getAllowedTimeDuration() const
    noexcept
{
    return allowed_time_duration_;
}

void ValveVent::setAllowedTimeDuration(seconds s1, seconds s2) noexcept
{
    assert(s1 < s2);

    allowed_time_duration_ = {s1, s2};
}

Json::Value ValveVent::toJson() const
{
    try {
	Json::Value val(Json::objectValue);

	val["id"] = id_;
	val["vent_usb"] = vent_usb_;
	val["vent_channel"] = vent_channel_;

	val["sensor_usb"] = sensor_usb_;
	val["sensor_channel"] = sensor_channel_;
	val["sensor_trigger"] = trigger_temp_.inCelsius();

	val["allowed_time_duration_min"] =
	    (int)allowed_time_duration_.first.count();
	val["allowed_time_duration_max"] =
	    (int)allowed_time_duration_.second.count();

	return val;

    } catch (const Json::Exception &ex) {
	throw ValveVentError(ex.what());
    }
}

ValveVent ValveVent::fromJson(const Json::Value &val)
{
    try {
	const auto &id = val["id"];
	const auto &vent_usb = val["vent_usb"];
	const auto &vent_channel = val["vent_channel"];

	const auto &sensor_usb = val["sensor_usb"];
	const auto &sensor_channel = val["sensor_channel"];
	const auto sensor_trigger = val["sensor_trigger"];

	const auto &allowed_time_duration_min =
	    val["allowed_time_duration_min"];
	const auto &allowed_time_duration_max =
	    val["allowed_time_duration_max"];

	ValveVent vv(id.asInt(), vent_usb.asInt(), vent_channel.asUInt());

	vv.setSensor(sensor_usb.asInt(), sensor_channel.asUInt(),
		     Temperature::fromCelsius(sensor_trigger.asDouble()));

	vv.setAllowedTimeDuration(seconds(allowed_time_duration_min.asUInt()),
				  seconds(allowed_time_duration_max.asUInt()));

	return vv;

    } catch (const Json::Exception &ex) {
	throw ValveVentError(ex.what());
    }
}

} // namespace CALF
