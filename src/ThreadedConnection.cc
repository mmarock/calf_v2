

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../header/ThreadedConnection.hh"
#include "../header/Circuit.hh"
#include "../header/ObjectSerialisation.hh"
#include "../header/Requests.hh"
#include "../header/Scheduler.hh"
#include "../header/logger.hh"

#include <algorithm>
#include <array>
#include <mutex>
#include <thread>
#include <vector>

#include <unistd.h>
#include <cassert>
#include <cstring>

namespace CALF
{
////////////////////////////////////////////////////////////
//		log namespace
////////////////////////////////////////////////////////////

namespace log
{
static void function_call(const char *function)
{
    logger::log(LOG_LEVEL::DEBUG, "[connection]: function_call: %s", function);
}

static void read_error(int fd, int err)
{
    logger::log(LOG_LEVEL::DEBUG, "[connection %d]: read(): %s", fd,
		strerror(err));
}

static void read_bytes(int fd, int c)
{
    logger::log(LOG_LEVEL::DEBUG, "[connection %d]: bytes received = %d", fd,
		c);
}

static void close_connection(int fd)
{
    logger::log(LOG_LEVEL::INFO, "[connection %d]: close connection", fd);
}

static void handle_connection(int fd)
{
    logger::log(LOG_LEVEL::INFO, "[connection %d]: handle connection", fd);
}

static void nothing_to_do(int fd)
{
    logger::log(LOG_LEVEL::DEBUG, "[connection %d]: nothing_to_do", fd);
}
}

////////////////////////////////////////////////////////////
//		ThreadedConnection::Impl definition
////////////////////////////////////////////////////////////

class ThreadedConnection::Impl
{
    int fd_;

    Circuit &circuit_;
    Scheduler &scheduler_;

    std::mutex buffer_mtx_;
    Bytes buffer_;

    volatile bool stop_thread_;
    bool close_requested_;
    std::thread thread_;

    static bool global_stop_;

    public:
    Impl(int fd, Circuit &c, Scheduler &s);
    ~Impl();

    Impl(const Impl &rhs) = delete;
    Impl &operator=(const Impl &rhs) = delete;

    static bool globalStopRequested() noexcept;

    bool closeRequested() const noexcept;
    int fd() const noexcept;

    int readFromFd(); // read new data
    void closeFd();   // close the socket, do this to ensure correct behaviour

    private:
    void worker();
};

bool ThreadedConnection::Impl::global_stop_ = false;

////////////////////////////////////////////////////////////
//		ThreadedConnection::Impl declaration
////////////////////////////////////////////////////////////

ThreadedConnection::Impl::Impl(int fd, Circuit &c, Scheduler &s)
    : fd_(fd), circuit_(c), scheduler_(s), stop_thread_(false),
      close_requested_(false), thread_(&ThreadedConnection::Impl::worker, this)
{
}

ThreadedConnection::Impl::~Impl() { Impl::closeFd(); }
bool ThreadedConnection::Impl::globalStopRequested() noexcept
{
    return global_stop_;
}

bool ThreadedConnection::Impl::closeRequested() const noexcept
{
    return close_requested_;
}

int ThreadedConnection::Impl::fd() const noexcept { return fd_; }
int ThreadedConnection::Impl::readFromFd()
{
    log::function_call(__PRETTY_FUNCTION__);
    assert(!stop_thread_ && "No one will read that");
    uint8_t buffer[1024];
    auto c = read(fd_, buffer, sizeof(buffer));
    if (c <= 0) {
	log::read_error(fd_, errno);
	return c;
    }
    std::lock_guard<std::mutex> lk(buffer_mtx_);
    for (int i = 0; i < c; ++i) {
	buffer_.push_back(buffer[i]);
    }
    log::read_bytes(fd_, c);
    return c;
}

void ThreadedConnection::Impl::closeFd()
{
    if (!stop_thread_) {
	log::close_connection(fd_);
	stop_thread_ = true;
	thread_.join();
	if (fd_ > 0) {
	    close(fd_);
	}
    }
}

void ThreadedConnection::Impl::worker()
{
    log::handle_connection(fd_);
    while (!stop_thread_ && !close_requested_) {
	std::this_thread::sleep_for(std::chrono::seconds(1));

	if (!Archive::importable(buffer_)) {
	    log::nothing_to_do(fd_);
	    continue;
	}

	Archive ar;
	{
	    std::lock_guard<std::mutex> lk(buffer_mtx_);
	    ar.import(buffer_);
	}

	// Archive respond;
	Archive::OBJECT obj = Archive::OBJECT::NONE;
	Archive response;
	while ((obj = ar.extractable()) == Archive::OBJECT::REQUEST) {
	    Request::Ptr rptr = ObjectSerialisation::constructRequest(ar);
	    if (!rptr) {
		logger::log(LOG_LEVEL::NOTICE,
			    "[connection fd%d]: unknown object "
			    "resulted in nullptr",
			    fd_);
		continue;
	    }

	    switch (rptr->type()) {
	    case Request::TYPE::ECHO:
		ObjectSerialisation::serialise(
		    Request::Ptr(new EchoRequest(
			dynamic_cast<const EchoRequest &>(*rptr).msg())),
		    response);
		logger::log(
		    LOG_LEVEL::INFO, "[connection fd:%d]: echo: %s", fd_,
		    dynamic_cast<const EchoRequest &>(*rptr).msg().c_str());
		break;
	    case Request::TYPE::NETDUMP:
		if (dynamic_cast<const NetDumpRequest &>(*rptr).circuit()) {
		    ObjectSerialisation::serialise(circuit_, response);
		    ObjectSerialisation::serialise(
			ObjectSerialisation::STATIC::USBENVIRONMENT, response);
		}
		break;
	    case Request::TYPE::ENABLE:
		circuit_.setDetectorActivity(
		    dynamic_cast<const DetectorEnableRequest &>(*rptr)
			.detector(),
		    dynamic_cast<const DetectorEnableRequest &>(*rptr)
			.enable());
		ObjectSerialisation::serialise(circuit_, response);
		break;
	    case Request::TYPE::QUIT:
		close_requested_ = true;
		break;
	    case Request::TYPE::SYSTEM:
		switch (dynamic_cast<const SystemRequest &>(*rptr).cmd()) {
		case SystemRequest::CMD::DUMP_TO_SYSLOG:
		    circuit_.dump();
		    scheduler_.dump();
		    break;
		case SystemRequest::CMD::RELOAD:
		    circuit_.reload();
		    scheduler_.reload();
		    break;
		case SystemRequest::CMD::STOP:
		    global_stop_ = true;
		    close_requested_ = true;
		    break;
		};
		break;
	    };
	}

	response.exportToFD(fd_);

	ar.clear();
    }
}

////////////////////////////////////////////////////////////
//		ThreadedConnection declaration
////////////////////////////////////////////////////////////

ThreadedConnection::ThreadedConnection(int fd, Circuit &c, Scheduler &s)
    : impl_(new Impl(fd, c, s))
{
}

ThreadedConnection::~ThreadedConnection() {}
ThreadedConnection::ThreadedConnection(ThreadedConnection &&rhs)
    : impl_(std::forward<ThreadedConnection>(rhs).impl_)
{
}

ThreadedConnection &ThreadedConnection::operator=(ThreadedConnection &&rhs)
{
    impl_ = std::forward<ThreadedConnection>(rhs).impl_;
    return *this;
}

bool ThreadedConnection::globalStopRequested() noexcept
{
    return Impl::globalStopRequested();
}

bool ThreadedConnection::closeRequested() const noexcept
{
    return impl_->closeRequested();
}

int ThreadedConnection::fd() const noexcept { return impl_->fd(); }
int ThreadedConnection::readFromFd()
{
    return impl_->readFromFd();
} // read new data

void ThreadedConnection::closeFd()
{
    impl_->closeFd();
} // close the socket, do this to ensure correct behaviour

} // namespace CALF
