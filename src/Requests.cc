

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../header/Requests.hh"

#include <cassert>

#include <json/json.h>

namespace CALF
{
////////////////////////////////////////////////////////////
//		Request definition
////////////////////////////////////////////////////////////

Request::Request() {}
Request::~Request() {}
Request::TYPE Request::type() const noexcept
{
    assert(0 && "Big fucking Nope");
    return TYPE::QUIT;
}

Json::Value Request::toJson() const
{
    assert(0 && "Big fucking Nope");
    Json::Value val(Json::objectValue);

    val["error"] = "This is the basic version of virtual "
		   "Request::toJson(). Should NEVER happen";

    return val;
}

Request::Ptr Request::fromJson(const Json::Value &val)
{
    auto name = val["request_type"].asString();
    if (!name.compare(Request::type(Request::TYPE::ECHO))) {
	return Request::Ptr(new EchoRequest(val["echo_msg"].asString()));
    } else if (!name.compare(Request::type(Request::TYPE::ENABLE))) {
	return Request::Ptr(new DetectorEnableRequest(
	    val["enable_detector_id"].asInt(), val["enable_enable"].asBool()));
    } else if (!name.compare(Request::type(Request::TYPE::NETDUMP))) {
	return Request::Ptr(new NetDumpRequest(
	    val["netdump_circuit"].asBool(), val["netdump_scheduler"].asBool(),
	    val["netdump_main_vents"].asBool(),
	    val["netdump_valve_vents"].asBool(),
	    val["netdump_detectors"].asBool(),
	    val["netdump_temperatures"].asBool()));
    } else if (!name.compare(Request::type(Request::TYPE::QUIT))) {
	return Request::Ptr(new QuitRequest());
    } else if (!name.compare(Request::type(Request::TYPE::SYSTEM))) {
	Request::Ptr ptr(nullptr);
	auto str = val["system_cmd"].asString();
	if (!str.compare(
		SystemRequest::cmd(SystemRequest::CMD::DUMP_TO_SYSLOG))) {
	    ptr.reset(new SystemRequest(SystemRequest::CMD::DUMP_TO_SYSLOG));
	} else if (!str.compare(
		       SystemRequest::cmd(SystemRequest::CMD::RELOAD))) {
	    ptr.reset(new SystemRequest(SystemRequest::CMD::RELOAD));
	} else if (!str.compare(SystemRequest::cmd(SystemRequest::CMD::STOP))) {
	    ptr.reset(new SystemRequest(SystemRequest::CMD::STOP));
	}
	return ptr;
    } else {
	return Request::Ptr(nullptr);
    }
}

const char *Request::type(TYPE t) noexcept
{
    switch (t) {
    case TYPE::ECHO:
	return "ECHO";
    case TYPE::QUIT:
	return "QUIT";
    case TYPE::NETDUMP:
	return "NETDUMP";
    case TYPE::ENABLE:
	return "ENABLE";
    case TYPE::SYSTEM:
	return "SYSTEM";
    };
}

EchoRequest::EchoRequest(std::string msg) : msg_(msg) {}
EchoRequest::~EchoRequest() {}
Json::Value EchoRequest::toJson() const
{
    Json::Value val(Json::objectValue);

    val["request_type"] = Request::type(TYPE::ECHO);
    val["echo_msg"] = msg_;

    return val;
}

const std::string &EchoRequest::msg() const noexcept { return msg_; }
Request::TYPE EchoRequest::type() const noexcept { return TYPE::ECHO; }
QuitRequest::QuitRequest() {}
QuitRequest::~QuitRequest() {}
QuitRequest::TYPE QuitRequest::type() const noexcept { return TYPE::QUIT; }
Json::Value QuitRequest::toJson() const
{
    Json::Value val(Json::objectValue);

    val["request_type"] = Request::type(TYPE::QUIT);

    return val;
}

NetDumpRequest::NetDumpRequest(bool circuit, bool scheduler, bool main_vents,
			       bool detectors, bool valve_vents,
			       bool temperatures)
    : circuit_(circuit), scheduler_(scheduler), main_vents_(main_vents),
      detectors_(detectors), valve_vents_(valve_vents),
      temperatures_(temperatures)
{
}

NetDumpRequest::~NetDumpRequest() {}
bool NetDumpRequest::circuit() const noexcept { return circuit_; }
bool NetDumpRequest::scheduler() const noexcept { return scheduler_; }
bool NetDumpRequest::main_vents() const noexcept { return main_vents_; }
bool NetDumpRequest::detectors() const noexcept { return detectors_; }
bool NetDumpRequest::valve_vents() const noexcept { return valve_vents_; }
bool NetDumpRequest::temperatures() const noexcept { return temperatures_; }
Json::Value NetDumpRequest::toJson() const
{
    Json::Value val(Json::objectValue);

    val["request_type"] = Request::type(TYPE::NETDUMP);

    val["netdump_circuit"] = circuit_;
    val["netdump_scheduler"] = scheduler_;
    val["netdump_main_vents"] = main_vents_;
    val["netdump_detectors"] = detectors_;
    val["netdump_valve_vents"] = valve_vents_;
    val["netdump_temperatures"] = temperatures_;

    return val;
}

NetDumpRequest::TYPE NetDumpRequest::type() const noexcept
{
    return TYPE::NETDUMP;
}

DetectorEnableRequest::DetectorEnableRequest(int detector_id, bool enable)
    : detector_id_(detector_id), enable_(enable)
{
}

DetectorEnableRequest::~DetectorEnableRequest() {}
Json::Value DetectorEnableRequest::toJson() const
{
    Json::Value val(Json::objectValue);

    val["request_type"] = Request::type(TYPE::ENABLE);

    val["enable_detector_id"] = detector_id_;
    val["enable_enable_"] = enable_;

    return val;
}

int DetectorEnableRequest::detector() const noexcept { return detector_id_; }
bool DetectorEnableRequest::enable() const noexcept { return enable_; }
Request::TYPE DetectorEnableRequest::type() const noexcept
{
    return TYPE::ENABLE;
}

SystemRequest::SystemRequest(CMD cmd) : cmd_(cmd) {}
SystemRequest::CMD SystemRequest::cmd() const noexcept { return cmd_; }
Request::TYPE SystemRequest::type() const noexcept
{
    return Request::TYPE::SYSTEM;
}

Json::Value SystemRequest::toJson() const
{
    Json::Value val(Json::objectValue);

    val["request_type"] = Request::type(TYPE::SYSTEM);

    val["system_cmd"] = SystemRequest::cmd(cmd_);

    return val;
}

const char *SystemRequest::cmd(CMD c) noexcept
{
    switch (c) {
    case CMD::DUMP_TO_SYSLOG:
	return "dump_to_syslog";
    case CMD::RELOAD:
	return "reload";
    case CMD::STOP:
	return "stop";
    }
}

} // namespace CALF
