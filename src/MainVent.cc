

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../header/MainVent.hh"

#include <cassert>

#include <json/json.h>

namespace CALF
{
////////////////////////////////////////////////////////////
//		MainVentError definitions
////////////////////////////////////////////////////////////

MainVentError::MainVentError(const std::string &msg)
    : msg_(std::string("MainVentError: ") + msg)
{
}

const char *MainVentError::what() const noexcept { return msg_.c_str(); }
////////////////////////////////////////////////////////////
//		MainVent definitions
////////////////////////////////////////////////////////////

MainVent::MainVent(int id, int vent_usb, uint8_t vent_channel)
    : id_(id), vent_usb_(vent_usb), vent_channel_(vent_channel)
{
}

int MainVent::id() const noexcept { return id_; }
int MainVent::getVentUSB() const noexcept { return vent_usb_; }
uint8_t MainVent::getVentChannel() const noexcept { return vent_channel_; }
Json::Value MainVent::toJson() const
{
    try {
	Json::Value val(Json::objectValue);

	val["id"] = id_;
	val["vent_usb"] = vent_usb_;
	val["vent_channel"] = vent_channel_;

	return val;

    } catch (const Json::Exception &ex) {
	throw MainVentError(ex.what());
    }
}

MainVent MainVent::fromJson(const Json::Value &val)
{
    try {
	const auto &id = val["id"];
	const auto &vent_usb = val["vent_usb"];
	const auto &vent_channel = val["vent_channel"];

	return MainVent(id.asInt(), vent_usb.asInt(), vent_channel.asUInt());

    } catch (const Json::Exception &ex) {
	throw MainVentError(ex.what());
    }
}

} // namespace CALF
