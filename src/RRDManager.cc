

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../header/RRDManager.hh"

#include "../header/Circuit.hh"
#include "../header/RoundRobinArchive.hh"
#include "../header/logger.hh"
#include "config.h"

#include <cassert>

#include <chrono>
#include <functional>
#include <map>
#include <mutex>
#include <thread>

namespace CALF
{
////////////////////////////////////////////////////////////
//		Exceptions
////////////////////////////////////////////////////////////

class RRDManagerError : public std::runtime_error
{
    public:
    enum class TYPE {
	SUBSCRIBE,
	UNSUBSCRIBE

    };
    explicit RRDManagerError(int detector_id, const TYPE &t);

    static const char *type(const TYPE &t)
    {
	switch (t) {
	case TYPE::SUBSCRIBE:
	    return "subscribe";
	case TYPE::UNSUBSCRIBE:
	    return "unsubscribe";
	};
    }
};

RRDManagerError::RRDManagerError(int d, const TYPE &t)
    : std::runtime_error(std::string("RRDManagerError: detector:") +
			 std::to_string(d) + ", type:" +
			 RRDManagerError::type(t) + " failed")
{
}

////////////////////////////////////////////////////////////
//		log namespace
////////////////////////////////////////////////////////////

namespace log
{
void round_robin_error(int id, const std::runtime_error &ex)
{
    logger::log(LOG_LEVEL::CRIT, "[ RRDManager ] detector:%d, what:%s", id,
		ex.what());
}

void circuit_error(int id, const CircuitError &ex)
{
    logger::log(LOG_LEVEL::CRIT, "[ RRDManager ] detector:%d, what:%s", id,
		ex.what());
}
}

/**
 * @ brief RRDManager impl idiom
 * @internal
 */
class RRDManager::Impl
{
    std::mutex mtx_;
    volatile bool resume_work_;

    Circuit *circuit_;

    enum DETECTOR_INFO { has_inlet, has_outlet, has_both };

    struct task {
	DETECTOR_INFO info_;
	RoundRobinArchive ar_;

	task(DETECTOR_INFO i, RoundRobinArchive &&ar)
	    : info_(i), ar_(std::move(ar))
	{
	}
    };

    std::map<int, task> tasks_;

    std::thread t_;

    public:
    explicit Impl(Circuit *c);
    void updateCircuitPtr(Circuit *c);
    // quit and join the thread
    ~Impl();

    bool isSubscribed(int detector_id);

    void subscribe(int id, bool has_inlet, bool has_outlet);
    void unsubscribe(int id);

    private:
    static const char *getDefaultLogDir();

    void logTemperatures(int id, task &t);
    void worker();
};

////////////////////////////////////////////////////////////
//		RRDManager::Impl definition
////////////////////////////////////////////////////////////

RRDManager::Impl::Impl(Circuit *c)
    : resume_work_(true), circuit_(c), t_(&Impl::worker, this)
{
    assert(c && "circuit c must not be null");
}

void RRDManager::Impl::updateCircuitPtr(Circuit *c)
{
    assert(c && "must not be NULL");
    logger::log(LOG_LEVEL::DEBUG, "[ RRDManager ] update circuit ptr");
    std::lock_guard<std::mutex> lk(mtx_);
    circuit_ = c;
}

RRDManager::Impl::~Impl()
{
    {
	std::lock_guard<std::mutex> lk(mtx_);
	resume_work_ = false;
	tasks_.clear(); // <-- if the RRD manager is destroyed AFTER the
			// circuit, it wil not look for temps without
			// tasks...
    }
    t_.join();
}

bool RRDManager::Impl::isSubscribed(int detector_id)
{
    assert(circuit_ && "circuit c must not be null");
    std::lock_guard<std::mutex> lk(mtx_);
    return (tasks_.find(detector_id) != tasks_.end());
}

void RRDManager::Impl::subscribe(int id, bool has_inlet, bool has_outlet)
{
    assert(circuit_ && "circuit c must not be null");
    assert(has_inlet || has_outlet && "both are false, makes no sense");
    logger::log(LOG_LEVEL::INFO,
		"[ RRDManager ] subscribe(id=%d, has_inlet=%s, has_outlet=%s)",
		id, (has_inlet) ? "true" : "false",
		(has_outlet) ? "true" : "false");

    std::lock_guard<std::mutex> lk(mtx_);

    DETECTOR_INFO detector_info;
    if (has_inlet && has_outlet) {
	detector_info = DETECTOR_INFO::has_both;
    } else if (has_inlet) {
	detector_info = DETECTOR_INFO::has_inlet;
    } else {
	detector_info = DETECTOR_INFO::has_outlet;
    }

    auto emplace_succeeded = tasks_.emplace(
	id, task(detector_info, RoundRobinArchive(id, getDefaultLogDir())));
    if (!emplace_succeeded.second) {
	throw RRDManagerError(id, RRDManagerError::TYPE::SUBSCRIBE);
    }
}

void RRDManager::Impl::unsubscribe(int id)
{
    assert(circuit_ && "circuit c must not be null");

    logger::log(
	LOG_LEVEL::INFO,
	"[ RRDManager ] unsubscribe(id=%d, has_inlet=%s, has_outlet=%s)", id,
	(has_inlet) ? "true" : "false", (has_outlet) ? "true" : "false");

    std::lock_guard<std::mutex> lk(mtx_);

    auto it = tasks_.find(id);
    if (it == tasks_.end()) {
	throw RRDManagerError(id, RRDManagerError::TYPE::UNSUBSCRIBE);
    }
    tasks_.erase(it);
}

const char *RRDManager::Impl::getDefaultLogDir() { return RRD_DIRECTORY; }
void RRDManager::Impl::logTemperatures(int id, task &t)
{
    assert(circuit_ && "circuit must not be NULL");
    try {
	double inlet_temp = 0;
	double outlet_temp = 0;
	switch (t.info_) {
	case has_inlet:
	    inlet_temp = circuit_->getDetectorInletTemperature(id).inCelsius();
	    t.ar_.updateInlet(inlet_temp);
	    break;
	case has_outlet:
	    outlet_temp =
		circuit_->getDetectorOutletTemperature(id).inCelsius();
	    t.ar_.updateOutlet(outlet_temp);
	    break;
	case has_both:
	    inlet_temp = circuit_->getDetectorInletTemperature(id).inCelsius();
	    outlet_temp =
		circuit_->getDetectorOutletTemperature(id).inCelsius();
	    t.ar_.updateBoth(inlet_temp, outlet_temp);
	};
    } catch (const CircuitError &ex) {
	log::circuit_error(id, ex);
    } catch (std::runtime_error &ex) {
	// RoundRobin Error
	log::round_robin_error(id, ex);
    }
}

void RRDManager::Impl::worker()
{
    assert(circuit_ && "circuit c must not be null");
    while (resume_work_) {
	{
	    std::lock_guard<std::mutex> lk(mtx_);
	    for (auto &task : tasks_) {
		logTemperatures(task.first, task.second);
	    }
	}
	std::this_thread::sleep_for(std::chrono::seconds(5));
    }
}

////////////////////////////////////////////////////////////
//		RRDManager definition
////////////////////////////////////////////////////////////

RRDManager::RRDManager(Circuit *c) : impl_(new Impl(c)) {}
void RRDManager::updateCircuitPtr(Circuit *c) { impl_->updateCircuitPtr(c); }
RRDManager::~RRDManager() {}
bool RRDManager::isSubscribed(int id) { return impl_->isSubscribed(id); }
void RRDManager::subscribe(int detector_id, bool has_inlet, bool has_outlet)
{
    impl_->subscribe(detector_id, has_inlet, has_outlet);
}

void RRDManager::unsubscribe(int detector_id)
{
    impl_->unsubscribe(detector_id);
}
}
