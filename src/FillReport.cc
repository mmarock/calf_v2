

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../header/FillReport.hh"

#include <algorithm>
#include <functional>
#include <iomanip>

namespace CALF
{
using namespace std;

////////////////////////////////////////////////////////////
//		Non member functions
////////////////////////////////////////////////////////////

const char *get_event_string(EVENT e)
{
    switch (e) {
    case EVENT::GROUP_NO_ACTIVE_DETECTOR:
	return "No active detector";
    case EVENT::VENTILATION_BEGIN:
	return "Ventilation Begin";
    case EVENT::VENTILATION_TIMEOUT:
	return "Ventilation Timeout";
    case EVENT::DETECTOR_BEGIN:
	return "Detector Begin";
    case EVENT::DETECTOR_INACTIVE:
	return "Detector Inactive";
    case EVENT::DETECTOR_WARMUP:
	return "Detector Warmup";
    case EVENT::DETECTOR_TIMEOUT:
	return "Detector Timeout";
    case EVENT::GROUP_BEGIN:
	return "FillGroup Begin";
    case EVENT::GROUP_SUCESSFULL:
    case EVENT::GROUP_FAILED:
	return "FillGroup End";
    case EVENT::USER_FILL_CANCEL:
	return "User requested fill cancellation";
    case EVENT::USER_REQUESTED_FORCE_FILL:
	return "User requested force-fill";
    case EVENT::UNKNOWN:
	return "Unknown Event";
    };
}

bool is_good_event(EVENT e)
{
    switch (e) {
    case EVENT::VENTILATION_BEGIN:
    case EVENT::GROUP_BEGIN:
    case EVENT::GROUP_SUCESSFULL:
    case EVENT::USER_FILL_CANCEL:
    case EVENT::USER_REQUESTED_FORCE_FILL:
    case EVENT::DETECTOR_BEGIN:
    case EVENT::DETECTOR_INACTIVE:
	return true;
    case EVENT::GROUP_NO_ACTIVE_DETECTOR:
    case EVENT::GROUP_FAILED:
    case EVENT::UNKNOWN:
    case EVENT::VENTILATION_TIMEOUT:
    case EVENT::DETECTOR_WARMUP:
    case EVENT::DETECTOR_TIMEOUT:
	return false;
    };
}

const char *type_string(EVENT e)
{
    switch (e) {
    case EVENT::VENTILATION_TIMEOUT:
	return "ValveVent";
    case EVENT::DETECTOR_WARMUP:
    case EVENT::DETECTOR_TIMEOUT:
    case EVENT::DETECTOR_INACTIVE:
	return "Detector";
    case EVENT::DETECTOR_BEGIN:
    case EVENT::VENTILATION_BEGIN:
    case EVENT::GROUP_BEGIN:
    case EVENT::GROUP_FAILED:
    case EVENT::GROUP_SUCESSFULL:
    case EVENT::USER_FILL_CANCEL:
    case EVENT::USER_REQUESTED_FORCE_FILL:
    case EVENT::GROUP_NO_ACTIVE_DETECTOR:
	return "Group";

    case EVENT::UNKNOWN:
	return "Unknown";
    };
}

string get_time_string(time_t time)
{
    struct tm tt;
    localtime_r(&time, &tt);
    char buf[64];
    strftime(buf, 64, "%d/%m/%Y %T ", &tt);
    return buf;
}

////////////////////////////////////////////////////////////
//		FillReportLine member
////////////////////////////////////////////////////////////

FillReportLine::FillReportLine(int i, time_t t, EVENT e)
    : id(i), time(t), event(e)
{
}

string FillReportLine::getString() const
{
    const char *type_str = type_string(event);
    const char *event_str = get_event_string(event);
    return get_time_string(time) + ((isPositive()) ? "[Success] " : "[Fail] ") +
	   event_str + " (" + type_str + " " + to_string(id) + ")\r\n";
}

bool FillReportLine::isPositive() const { return is_good_event(event); }
////////////////////////////////////////////////////////////
//		FillReport member
////////////////////////////////////////////////////////////

void FillReport::addLine(int id, EVENT e)
{
    time_t time = std::time(NULL);
    lock_guard<mutex> lk(mtx_);
    lines_.emplace_back(id, time, e);
}

void FillReport::clear()
{
    lock_guard<mutex> lk(mtx_);
    lines_.clear();
}

std::string FillReport::createLongFillReport()
{
    lock_guard<mutex> lk(mtx_);
    /*
	    sort(lines_.begin(), lines_.end(),
		 [](const FillReportLine &a, const FillReportLine &b) {
			 return (a.time <= b.time);
		 });
    */

    string result(getSubject());
    result.reserve(1024);
    for (auto &line : lines_) {
	result += line.getString();
    }
    result += "\r\n";
    return result;
}

std::string FillReport::createShortFillReport() const
{
    lock_guard<mutex> lk(mtx_);
    return getSubject();
}

bool FillReport::isPositive() const
{
    return (find_if_not(lines_.begin(), lines_.end(),
			mem_fn(&FillReportLine::isPositive)) == lines_.end());
}

const FillReportLine &FillReport::getGroupBeginLine() const
{
    auto it =
	find_if(lines_.begin(), lines_.end(), [](const FillReportLine &rp) {
	    return (rp.event == EVENT::GROUP_BEGIN);
	});
    if (it == lines_.end()) {
	throw runtime_error("FillReport::getGroupBeginLine: message not found");
    }
    return *it;
}

const FillReportLine &FillReport::getFirstNegativeLine() const
{
    auto it = find_if_not(lines_.begin(), lines_.end(),
			  mem_fn(&FillReportLine::isPositive));
    if (it == lines_.end()) {
	throw runtime_error(
	    "FillReport::getFirstNegativeLine: message not found");
    }
    return *it;
}

std::string FillReport::getSubject() const
{
    auto grp_begin = getGroupBeginLine();

    if (isPositive()) {
	return "Subject: [Success] FillGroup " + to_string(grp_begin.id) +
	       " completed\r\n\r\n";
    }

    return "Subject: [Fail] FillGroup " + to_string(grp_begin.id) +
	   " failed (" + get_event_string(getFirstNegativeLine().event) +
	   ")\r\n\r\n";
}

} // namespace CALF
