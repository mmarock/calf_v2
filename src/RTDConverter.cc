

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../header/RTDConverter.hh"

#include "../header/logger.hh"
#include "config.h"

#include <algorithm>
#include <fstream>

//#include <libconfig.h++>
#include <json/json.h>

using namespace std;

namespace CALF
{
////////////////////////////////////////////////////////////
//		log namespace
////////////////////////////////////////////////////////////

namespace log
{
static void config_path()
{
    logger::log(LOG_LEVEL::INFO, "RTDConverter: try to load config: %s",
		RTDConverter::getDefaultConfigFile());
}

static void queryTemeprature(const char *name, double resistance)
{
    logger::log(LOG_LEVEL::DEBUG, "RTDConverter: name: %s, resistance: %lf",
		name, resistance);
}
}

////////////////////////////////////////////////////////////
//		Exception definitions
////////////////////////////////////////////////////////////

RTDConverterError::RTDConverterError(const char *msg)
    : msg_(string("RTDConverterError: ") + msg)
{
}

RTDConverterError::RTDConverterError(const string &msg)
    : msg_(string("RTDConverterError: ") + move(msg))
{
}

const char *RTDConverterError::what() const noexcept { return msg_.c_str(); }
struct ResistanceOutOfRange : public RTDConverterError {
    ResistanceOutOfRange(double resistance, double resistance_limit)
	: RTDConverterError(string("ResistanceOutOfRange: ") +
			    to_string(resistance) + " exceeds " +
			    to_string(resistance_limit))
    {
    }
};

struct RTDNotFound : public RTDConverterError {
    explicit RTDNotFound(const string &name)
	: RTDConverterError(string("RTDNotFound: ") + name)
    {
    }
};

struct RTDAlreadyRegistered : public RTDConverterError {
    explicit RTDAlreadyRegistered(const string &name)
	: RTDConverterError(string("RTDAlreadyRegistered: ") + name)
    {
    }
};

struct FileNotFound : public RTDConverterError {
    explicit FileNotFound()
	: RTDConverterError(string("FileNotFound: ") +
			    RTDConverter::getDefaultConfigFile())
    {
    }
};

struct ParsingFailed : public RTDConverterError {
    explicit ParsingFailed(const string &msg)
	: RTDConverterError(string("ParsingFailed: ") + msg)
    {
    }
};

struct ConfigSettingError : public RTDConverterError {
    explicit ConfigSettingError(const string &name)
	: RTDConverterError(string("ConfigIOError: error at setting: ") + name)
    {
    }
};

RTDImpl::data_point::data_point(double r, double t)
    : resistance(r), temperature_in_K(t)
{
}

const string &RTDImpl::getName() const { return name_; }
double calculate_slope(vector<RTDImpl::data_point>::const_iterator &it,
		       vector<RTDImpl::data_point>::const_iterator &next)
{
    return (next->resistance - it->resistance) /
	   (next->temperature_in_K - it->temperature_in_K);
}

RTDImpl::RTDImpl(const string &name, const vector<data_point> &d_points)
    : name_(name)
{
    vector<data_point> interpolated;
    auto it = d_points.begin();
    auto next = it + 1;
    while (it != d_points.end() && next != d_points.end()) {
	double slope = calculate_slope(it, next);
	const double &resistance = it->resistance;
	const double &T_0 = it->temperature_in_K;

	for (double new_T = T_0; new_T < next->temperature_in_K;
	     new_T += RTDConverter::getDefaultTemperatureStep()) {
	    double new_resistance = slope * (new_T - T_0) + resistance;
	    interpolated.emplace_back(new_resistance, new_T);
	}
	it++;
	next++;
    }
    data_points_ = interpolated;
}

Temperature RTDImpl::getTemperature(double resistance) const
{
    auto it = find_if(
	data_points_.begin(), data_points_.end(),
	[&](const data_point &dp) { return (resistance <= dp.resistance); });
    log::queryTemeprature(getName().c_str(), resistance);
    if (it == data_points_.begin()) {
	throw ResistanceOutOfRange(resistance, it->resistance);
    } else if (it == data_points_.end()) {
	throw ResistanceOutOfRange(resistance, data_points_.back().resistance);
    } else {
	return Temperature(it->temperature_in_K);
    }
}

RTDCalculator::RTDCalculator(const shared_ptr<RTDImpl> &impl) : impl_(impl) {}
const string &RTDCalculator::getName() const { return impl_->getName(); }
Temperature RTDCalculator::operator()(double res) const
{
    return impl_->getTemperature(res);
}

// vector<RTDImpl::data_point> get_data_points(const libconfig::Setting &grp)
vector<RTDImpl::data_point> get_data_points(const Json::Value &grp)
{
    vector<RTDImpl::data_point> result;

    const Json::Value &data = grp["data"];

    if (!data.isArray()) {
	throw ConfigSettingError("data");
    }

    result.reserve(data.size());

    for (const auto &entry : data) {
	if (!entry.isArray() || entry.size() != 2) {
	    throw ConfigSettingError("data");
	}
	result.emplace_back(entry[0].asDouble(), entry[1].asDouble());
    }
    return result;
}

void RTDConverter::fromConfigFile()
{
    using namespace Json;

    log::config_path();
    std::ifstream file(RTDConverter::getDefaultConfigFile());

    if (!file.is_open()) {
	throw FileNotFound();
    }

    Value document(ValueType::arrayValue);

    Reader reader;
    if (!reader.parse(file, document)) {
	throw ParsingFailed(reader.getFormattedErrorMessages());
    }

    for (const auto &device : document) {
	makeRTD(device["name"].asString(), get_data_points(device));
    }
}

shared_ptr<RTDImpl>
RTDConverter::makeRTD(const string &name,
		      const vector<RTDImpl::data_point> &data_ps)
{
    auto it = findByName(name);

    auto &impls = getRTDImpls();
    if (it == impls.end()) {
	RTDImpl rtd(name, data_ps);
	impls.push_back(make_shared<RTDImpl>(rtd));
	logger::log(LOG_LEVEL::DEBUG, "RTDConverter: added calculator %s",
		    name.c_str());
	return impls.back();
    } else {
	throw RTDAlreadyRegistered(name);
    }
}

RTDCalculator RTDConverter::getCalculator(const string &name)
{
    auto it = findByName(name);
    if (it == getRTDImpls().end()) {
	throw RTDNotFound(name);
    } else {
	return RTDCalculator(*it);
    }
}

vector<shared_ptr<RTDImpl>>::iterator
RTDConverter::findByName(const string &name)
{
    auto &impls = RTDConverter::getRTDImpls();
    return find_if(
	impls.begin(), impls.end(),
	[&](const shared_ptr<RTDImpl> &dp) { return (dp->getName() == name); });
}

vector<shared_ptr<RTDImpl>> &RTDConverter::getRTDImpls()
{
    static vector<shared_ptr<RTDImpl>> static_data;
    return static_data;
}

double RTDConverter::getDefaultTemperatureStep() { return 0.5; }
const char *RTDConverter::getDefaultConfigFile()
{
    return RTD_CONVERTER_CONFIG_DIR "/converter.config";
}

} // namespace CALF
