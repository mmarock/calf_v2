

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../header/DescriptorGuard.hh"

#include <unistd.h>

namespace CALF
{
DescriptorGuard::DescriptorGuard(int fd) : fd_(fd) {}
DescriptorGuard::~DescriptorGuard()
{
    if (fd_ > 0) {
	close(fd_);
	fd_ = -1;
    }
}

int DescriptorGuard::fd() const noexcept { return fd_; }
} // namespace CALF
