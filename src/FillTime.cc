

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../header/FillTime.hh"

#include <cassert>
#include <cstring>
#include <ctime>

#include <stdexcept>

#include <json/json.h>

namespace CALF
{
FillTime::FillTime(int h, int m) : hour(h), minute(m)
{
    assert((h >= 0) && (h < 24) && (m >= 0) && (m < 60) &&
	   "input only valid times!");
}

FillTime::FillTime(const FillTime &ft) : hour(ft.hour), minute(ft.minute) {}
FillTime &FillTime::operator=(const FillTime &ft)
{
    hour = ft.hour;
    minute = ft.minute;
    return *this;
}

std::chrono::minutes FillTime::offset() const
{
    // get time_point representation

    struct tm now;
    memset(&now, 0, sizeof(now));

    time_t t = time(nullptr);

    if (!localtime_r(&t, &now)) {
	// localtime error
	throw std::runtime_error("localtime_r()");
    }

    int dhour(hour - now.tm_hour);
    int dminute(minute - now.tm_min);

    int result = dhour * 60 + dminute;
    if (result <= 0) {
	result += 60 * 24;
    }
    return std::chrono::minutes(result);
}

Json::Value FillTime::toJson() const
{
    Json::Value val(Json::objectValue);

    val["hour"] = hour;
    val["minute"] = minute;

    return val;
}

FillTime FillTime::fromJson(const Json::Value &val)
{
    return FillTime(val["hour"].asInt(), val["minute"].asInt());
}

bool operator<=(const FillTime &lhs, const FillTime &rhs) noexcept
{
    return lhs.offset() <= rhs.offset();
}

bool operator==(const FillTime &lhs, const FillTime &rhs) noexcept
{
    return (lhs.hour == rhs.hour && lhs.minute == rhs.minute);
}

} // namespace CALF
