

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file calf-cli.cc
 * @brief
 * @author Marcel Marock
 * @version 0.001
 * @date 2016-09-02
 */

#include <cctype>
#include <cstdlib>
#include <cstring>

#include <poll.h>
#include <unistd.h>

#include <sys/socket.h>
#include <sys/un.h>

#include <iostream>
#include <thread>

#include "../header/DescriptorGuard.hh"
#include "../header/ObjectSerialisation.hh"
#include "../header/Requests.hh"
#include "../header/make_unique.hpp"

/**
 * @brief Option interface
 */
struct Option {
    using Ptr = std::unique_ptr<Option>;

    Option();
    virtual ~Option() = 0;

    virtual int exec(bool &resume, std::ostream &os, CALF::Archive &oar);
    virtual bool responseNeeded() const { return false; }
    static const std::string &name();

    private:
    friend Option::Ptr parseOption(char *);
    static std::string next_token(char *buffer, char **save_ptr);
};

Option::Option() {}
Option::~Option() {}
int Option::exec(bool &resume, std::ostream &os, CALF::Archive &oar)
{
    (void)resume;
    (void)os;
    (void)oar;
    return -1;
}

const std::string &name()
{
    static std::string i("");
    return i;
}

std::string Option::next_token(char *buffer, char **save_ptr)
{
    const char *t = strtok_r(buffer, "\t \n", save_ptr);
    return (t) ? std::string(t) : std::string();
}

struct HelpOption : public Option {
    HelpOption() {}
    ~HelpOption() {}
    virtual int exec(bool &resume, std::ostream &os,
		     CALF::Archive &oar) override final
    {
	(void)resume;
	(void)oar;
	os << "\thelp: print this strings\n"
	      "\techo: echo a string (str is second parameter!)\n"
	      "\tquit: quit the client\n"
	      "\tstate: get the calfd state\n";
	return 0;
    }

    virtual bool responseNeeded() const override { return false; }
};

struct NewlineOnly : public Option {
    NewlineOnly() {}
    ~NewlineOnly() {}
    virtual int exec(bool &resume, std::ostream &os,
		     CALF::Archive &oar) override final
    {
	(void)resume;
	(void)oar;
	os << "\tgot an empty string!\n";
	return 0;
    }

    virtual bool responseNeeded() const override { return false; }
};

struct QuitClient : public Option {
    QuitClient() {}
    ~QuitClient() {}
    virtual int exec(bool &resume, std::ostream &os,
		     CALF::Archive &oar) override final
    {
	resume = false;
	os << "\tclose connection to server\n";

	CALF::ObjectSerialisation::serialise(
	    (CALF::Request::Ptr) new CALF::QuitRequest(), oar);

	return 0;
    }

    virtual bool responseNeeded() const override { return true; }
};

/**
 * @brief Get the state of remote circuit
 */
struct GetState : public Option {
    GetState() {}
    ~GetState() {}
    virtual int exec(bool &resume, std::ostream &os,
		     CALF::Archive &oar) override final
    {
	(void)resume;
	(void)os;

	return CALF::ObjectSerialisation::serialise(
	    static_cast<CALF::Request::Ptr>(new CALF::NetDumpRequest(
		true, false, false, false, false, false)),
	    oar);
    }

    virtual bool responseNeeded() const override { return true; }
};

struct Echo : public Option {
    std::string message;

    Echo(std::string &&msg) : message(std::forward<std::string>(msg)) {}
    ~Echo() {}
    virtual int exec(bool &resume, std::ostream &os,
		     CALF::Archive &oar) override final
    {
	(void)resume;
	(void)os;
	if (message.empty()) {
	    os << "\techo: empty second token! ( no message )\n";
	    return 0;
	} else {
	    os << "\techo: gonna send \"" << message << "\"" << std::endl;
	}
	return CALF::ObjectSerialisation::serialise(
	    static_cast<CALF::Request::Ptr>(new CALF::EchoRequest(message)),
	    oar);
    }

    virtual bool responseNeeded() const override { return true; }
};

Option::Ptr parseOption(char *str)
{
    char *save_ptr(nullptr); //<-- used by strtok_r in next_token
    auto name = Option::next_token(str, &save_ptr);

    if (name.empty()) {
	return custom::make_unique<NewlineOnly>();
    } else if (!name.compare("help")) {
	return custom::make_unique<HelpOption>();
    } else if (!name.compare("quit")) {
	return custom::make_unique<QuitClient>();
    } else if (!name.compare("state")) {
	return custom::make_unique<GetState>();
    } else if (!name.compare("echo")) {
	// get all appended tokens
	std::string buf;
	std::string msg;
	while (!((buf = Option::next_token(nullptr, &save_ptr)).empty())) {
	    msg.append(buf + " ");
	}
	return custom::make_unique<Echo>(std::move(msg));
    }
    return nullptr;
}

/**
 * @brief local calf client interface
 *
 * @return 0 on success, -1 else
 */
int main()
{
    CALF::DescriptorGuard sock(socket(AF_UNIX, SOCK_STREAM, 0));
    if (sock.fd() == -1) {
	perror("socket()");
	return -1;
    }

    struct sockaddr_un addr {
	AF_UNIX, "/tmp/calf.socket"
    };

    if (connect(sock.fd(), (const struct sockaddr *)&addr, sizeof(addr)) ==
	-1) {
	perror("connect()");
	return -1;
    }

    std::cout << ">> ";

    int c(0);
    char *buffer = (char *)malloc(256);
    size_t buffer_len = sizeof(buffer);
    bool proceed_work(true);

    struct pollfd pollstruct;
    pollstruct.fd = sock.fd();
    pollstruct.events = 0;

    const size_t POLL_TIMEOUT = 2000;
    while ((c = getline(&buffer, &buffer_len, stdin)) != -1) {
	// 1 line -> one cmd + args
	auto opt_ptr = parseOption(buffer);
	if (!opt_ptr) {
	    std::cout << "unknown token\n>> ";
	    continue;
	}

	pollstruct.events = POLLOUT | POLLERR;
	switch (poll(&pollstruct, 1, POLL_TIMEOUT)) {
	case 0:
	    std::cout << ">> poll timeout (2000ms)";
	    goto client_cleanup;
	case -1:
	    std::cout << "!! poll(): " << strerror(errno);
	    goto client_cleanup;
	default: {
	    CALF::Archive ar;
	    if (pollstruct.revents & POLLOUT &&
		opt_ptr->exec(proceed_work, std::cout << "<<", ar) < 0) {
		std::cerr << "!! exec(): " << strerror(errno);
		goto client_cleanup;
	    }
	    // socket is writable
	    ar.exportToFD(sock.fd());
	}
	};

	if (opt_ptr->responseNeeded()) {
	    pollstruct.events = POLLIN | POLLERR;
	    if (poll(&pollstruct, 1, POLL_TIMEOUT) < 0) {
		std::cerr << "!! poll(): " << strerror(errno) << std::endl;
		goto client_cleanup;
	    }

	    if (pollstruct.revents & POLLIN) {
		// is readable
		CALF::Archive ret;
		ret.importByFD(sock.fd());
	    }
	}

	if (!proceed_work) {
	    break;
	}

	std::cout << ">> ";
    }

client_cleanup:

    if (buffer) {
	free(buffer);
	buffer = nullptr;
    }

    if (c == -1) {
	std::cerr << ">> getline(): " << strerror(errno) << std::endl;
    }

    std::cout << "<< shutdown complete" << std::endl;
    return 0;
    // enter loop
    // commands: help status enable disable quit
    // << help
    // >> help: this output
    // >> state: get the circuit state
    // >> quit: quit this client

    // suche session
    // stelle verbindung her
    // gehe in loop, schreibe eingaben in buffer
    // solange getline hat ergebnis, verarbeite
    //
    //
    // >>	status id=3
    // << cryostat(id=3) = {vent_usb=0, vent_channel=1};
    //
    // >> enable id=3
    // << replied: OK
}
