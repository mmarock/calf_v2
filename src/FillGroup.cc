

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../header/FillGroup.hh"
#include "../header/FillTime.hh"

#include <algorithm>

#include <json/json.h>

namespace CALF
{
////////////////////////////////////////////////////////////
//		exceptions
////////////////////////////////////////////////////////////

FillGroupError::FillGroupError(const char *msg)
    : msg_(std::string("FillGroupError: ") + msg)
{
}
FillGroupError::FillGroupError(const std::string &msg)
    : msg_(std::string("FillGroupError: ") + msg)
{
}
const char *FillGroupError::what() const noexcept { return msg_.c_str(); }
struct InvalidFillTime : public FillGroupError {
    explicit InvalidFillTime(int hour, int minute)
	: FillGroupError(std::string("InvalidFillTime: hour: ") +
			 std::to_string(hour) + ", minute: " +
			 std::to_string(minute))
    {
    }
};

struct OverlappingFillTime : public FillGroupError {
    explicit OverlappingFillTime(int new_hour, int new_minute, int hour,
				 int minute)
	: FillGroupError(std::string("OverlappingFillTime: old-time: ") +
			 std::to_string(hour) + "h" + std::to_string(minute) +
			 "min, new-time: " + std::to_string(new_hour) + "h" +
			 std::to_string(new_minute) + "min")
    {
    }
};

struct DetectorNotFound : public FillGroupError {
    explicit DetectorNotFound(int detector)
	: FillGroupError(std::string("DetectorNotFound: id: ") +
			 std::to_string(detector))
    {
    }
};

struct NoFillTimesAdded : public FillGroupError {
    explicit NoFillTimesAdded(int grp)
	: FillGroupError(std::string("NoFillTimesAdded: group: ") +
			 std::to_string(grp))
    {
    }
};

////////////////////////////////////////////////////////////
//		class FillGroup
////////////////////////////////////////////////////////////

FillGroup::FillGroup(int id) : id_(id), time_frame_(20) {}
FillGroup::FillGroup(const FillGroup &rhs)
    : id_(rhs.id_), time_frame_(rhs.time_frame_), detectors_(rhs.detectors_),
      fill_times_(rhs.fill_times_)
{
}

FillGroup &FillGroup::operator=(const FillGroup &rhs)
{
    id_ = rhs.id_;
    time_frame_ = rhs.time_frame_;
    detectors_ = rhs.detectors_;
    fill_times_ = rhs.fill_times_;

    return *this;
}

void FillGroup::addFillTime(int hour, int minute)
{
    if (hour < 0 || minute < 0 || hour > 23 || minute > 59) {
	throw InvalidFillTime(hour, minute);
    }
    fill_times_.emplace_back(hour, minute);
}

std::chrono::minutes FillGroup::nextOffset() const noexcept
{
    size_t n = fill_times_.size();
    if (n == 0) {
	return std::chrono::minutes::max();
    }
    return min_element(fill_times_.begin(), fill_times_.end(),
		       [](const FillTime &ft1, const FillTime &ft2) {
			   return ft1.offset() < ft2.offset();
		       })
	->offset();
}

bool FillGroup::haveDetector(int detector) const noexcept
{
    return find(detectors_.begin(), detectors_.end(), detector) !=
	   detectors_.end();
}

void FillGroup::removeFillTime(int hour, int minute)
{
    auto it = std::find_if(fill_times_.begin(), fill_times_.end(),
			   [&](const FillTime &ft) {
			       return (hour == ft.hour && minute == ft.minute);
			   });
    if (it == fill_times_.end()) {
	throw InvalidFillTime(hour, minute);
    }
    fill_times_.erase(it);
}

void FillGroup::addDetector(int detector,
			    const std::chrono::minutes &time_frame)
{
    // check if the new time_frame invalidates the FillGroup
    std::for_each(fill_times_.begin(), fill_times_.end(), [&](FillTime &ft1) {
	std::for_each(fill_times_.begin(), fill_times_.end(),
		      [&](FillTime &ft2) {
			  if (!(ft1 == ft2) &&
			      FillGroup::overlaps(ft1, ft2, time_frame)) {
			      throw OverlappingFillTime(ft1.hour, ft1.minute,
							ft2.hour, ft2.minute);
			  }
		      });
    });

    detectors_.emplace_back(detector);
    time_frame_ = time_frame;
}

void FillGroup::removeDetector(int detector)
{
    auto it = std::find(detectors_.begin(), detectors_.end(), detector);
    if (it == detectors_.end()) {
	throw DetectorNotFound(detector);
    }
    detectors_.erase(it);
}

int FillGroup::getID() const noexcept { return id_; }
const std::vector<int> &FillGroup::getDetectors() const noexcept
{
    return detectors_;
}

const std::vector<FillTime> &FillGroup::getFillTimes() const noexcept
{
    return fill_times_;
}

bool FillGroup::interferesWith(const FillGroup &rhs) const noexcept
{
    auto max_time_frame(std::max(time_frame_, rhs.time_frame_));

    for (const FillTime &ft1 : fill_times_) {
	for (const FillTime &ft2 : rhs.fill_times_) {
	    if (!(ft1 == ft2) &&
		FillGroup::overlaps(ft1, ft2, max_time_frame)) {
		return false;
	    }
	}
    }
    return true;
}

Json::Value FillGroup::toJson() const
{
    Json::Value val(Json::objectValue);

    val["id"] = id_;
    val["time_frame"] = static_cast<Json::UInt64>(time_frame_.count());

    auto &detectors = (val["detectors"] = Json::arrayValue);
    for (int d : detectors_) {
	detectors.append(d);
    }

    auto &fill_times = (val["fill_times"] = Json::arrayValue);
    for (auto &ft : fill_times_) {
	fill_times.append(ft.toJson());
    }

    return val;
}

FillGroup FillGroup::fromJson(const Json::Value &val)
{
    FillGroup fg(val["id"].asInt());

    fg.time_frame_ = std::chrono::minutes(val["time_frame"].asUInt64());
    for (auto &detector : val["detectors"]) {
	fg.detectors_.emplace_back(detector.asInt());
    }
    for (auto &ftime : val["fill_times"]) {
	fg.fill_times_.emplace_back(FillTime::fromJson(ftime));
    }

    return fg;
}

bool FillGroup::overlaps(const FillTime &ft1_in, const FillTime &ft2_in,
			 const std::chrono::minutes &time_frame)
{
    using namespace std::chrono;
    // minutes
    const minutes ft1(ft1_in.offset());
    const minutes ft2(ft2_in.offset());
    const minutes ft1_end(ft1 + time_frame);
    const minutes ft2_end(ft2 + time_frame);

    if (ft2 >= ft1_end || ft1 >= ft2_end) {
	// separated groups
	return false;
    }
    return true;
}

} // namespace CALF
