

/**
 *   calf - scheduled filling of cryostats
 *   Copyright (C) 2016  "Marcel Marock"<mmarock@ikp.uni-koeln.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../header/RTDCalculator.hh"

#include "../header/Temperature.hh"
#include "config.h"

#include <algorithm>
#include <fstream>

#include <cassert>

#include <json/json.h>

namespace CALF
{
////////////////////////////////////////////////////////////
//		RTDCalculatorError exception
////////////////////////////////////////////////////////////

RTDCalculatorError::RTDCalculatorError(const char *msg)
    : msg_(std::string("RTDCalculatorError: ") + msg)
{
}

RTDCalculatorError::RTDCalculatorError(const std::string &msg)
    : msg_(std::string("RTDCalculatorError: ") + msg)
{
}

const char *RTDCalculatorError::what() const noexcept { return msg_.c_str(); }
class SerialisationError : public RTDCalculatorError
{
    public:
    explicit SerialisationError(const Json::Exception &ex);
};

SerialisationError::SerialisationError(const Json::Exception &ex)
    : RTDCalculatorError(std::string("JsonError: ") + ex.what())
{
}

class MalformedFile : public RTDCalculatorError
{
    public:
    MalformedFile(const Json::Exception &ex);

    enum ERROR : int { FILE_NOT_FOUND, RTD_NAME_NOT_FOUND };
    explicit MalformedFile(ERROR e, const std::string &str);

    const char *error(ERROR e) const noexcept;
};

MalformedFile::MalformedFile(const Json::Exception &ex)
    : RTDCalculatorError(std::string("Malformed File: ") + ex.what())
{
}

MalformedFile::MalformedFile(ERROR e, const std::string &str)
    : RTDCalculatorError(std::string("MalformedFile: ") + error(e) + str)
{
}

const char *MalformedFile::error(ERROR e) const noexcept
{
    switch (e) {
    case ERROR::FILE_NOT_FOUND:
	return "file not found: ";
    case ERROR::RTD_NAME_NOT_FOUND:
	return "rtd name not found: ";
    };
}

class OutOfBound : public RTDCalculatorError
{
    public:
    enum EXCESS : uint8_t { BIGGER, SMALLER };

    explicit OutOfBound(EXCESS e);

    const char *excess(EXCESS e) const noexcept;
};

OutOfBound::OutOfBound(EXCESS e)
    : RTDCalculatorError(std::string("OutOfBound: ") + excess(e))
{
}

const char *OutOfBound::excess(EXCESS e) const noexcept
{
    switch (e) {
    case EXCESS::SMALLER:
	return "smaller";
    case EXCESS::BIGGER:
	return "bigger";
    };
}
////////////////////////////////////////////////////////////
//		RTDCalculator::Impl
////////////////////////////////////////////////////////////

class RTDCalculator::Impl
{
    public:
    Impl(const std::vector<RTDData> &origin, std::string name);

    const std::string &name() const noexcept;

    Temperature operator()(double resistance) const;

    private:
    // Zeigt auf die Factory Daten, update() updatet den pointer nach dem
    // reload
    std::string name_;
    const std::vector<RTDData> &origin_;
};

RTDCalculator::Impl::Impl(const std::vector<RTDData> &origin, std::string name)
    : name_(name), origin_(origin)
{
}

const std::string &RTDCalculator::Impl::name() const noexcept { return name_; }
Temperature RTDCalculator::Impl::operator()(double resistance) const
{
    auto it = find_if(origin_.begin(), origin_.end(), [&](const RTDData &rtd) {
	return !rtd.name.compare(name_);
    });
    if (it == origin_.end()) {
	throw MalformedFile(MalformedFile::ERROR::RTD_NAME_NOT_FOUND, name_);
    }
    const auto &vals = it->vals;

    auto bigger =
	std::find_if(vals.begin(), vals.end(), [&](const FunctionVal &val) {
	    return val.resistance >= resistance;
	});

    if (bigger == vals.end()) {
	throw OutOfBound(OutOfBound::BIGGER);
    } else if (bigger == vals.begin()) {
	throw OutOfBound(OutOfBound::SMALLER);
    }

    auto prev = bigger - 1;

    return Temperature::fromCelsius((bigger->temperature + prev->temperature) /
				    2l);
}

////////////////////////////////////////////////////////////
//		RTDCalculator
////////////////////////////////////////////////////////////

RTDCalculator::RTDCalculator() : impl_(nullptr) {}
RTDCalculator::RTDCalculator(const std::vector<RTDData> &origin,
			     const std::string &name)
    : impl_(new Impl(origin, name))
{
}

RTDCalculator::~RTDCalculator() {}
RTDCalculator::RTDCalculator(RTDCalculator &&rhs)
    : impl_(std::forward<RTDCalculator>(rhs).impl_)
{
}
RTDCalculator &RTDCalculator::operator=(RTDCalculator &&rhs)
{
    impl_ = std::forward<RTDCalculator>(rhs).impl_;
    return *this;
}

const std::string &RTDCalculator::name() const noexcept
{
    assert(impl_ && "not initialised");
    return impl_->name();
}

Temperature RTDCalculator::operator()(double resistance) const
{
    assert(impl_ && "not initialised");
    return impl_->operator()(resistance);
}

////////////////////////////////////////////////////////////
//		RTDCalculatorFactory::Impl
////////////////////////////////////////////////////////////

class RTDCalculatorFactory::Impl
{
    std::vector<RTDData> rtds_;

    public:
    void loadRTDCalculator();

    void clear();
    void reload();

    RTDCalculator getRTDCalc(const char *name);

    Json::Value toJson();
};

void RTDCalculatorFactory::Impl::loadRTDCalculator()
{
    std::ifstream file(RTDCalculatorFactory::default_path());

    if (!file.is_open()) {
	throw MalformedFile(MalformedFile::FILE_NOT_FOUND, default_path());
    }

    Json::Value document(Json::ValueType::arrayValue);

    Json::Reader reader;
    if (!reader.parse(file, document)) {
	throw RTDCalculatorError(reader.getFormatedErrorMessages());
    }

    file.close();

    for (const auto &rtd : document) {
	RTDData data;
	data.name = rtd["name"].asString();
	for (const auto &d : rtd["data"]) {
	    data.vals.push_back({d[0].asDouble(), d[1].asDouble() - 273.15});
	}

	rtds_.push_back(data);
    }
}

void RTDCalculatorFactory::Impl::clear() { rtds_.clear(); }
void RTDCalculatorFactory::Impl::reload()
{
    assert(0 && "impl that shit");
    // read file
    //
    // parse
}

RTDCalculator RTDCalculatorFactory::Impl::getRTDCalc(const char *name)
{
    if (std::find_if(rtds_.begin(), rtds_.end(), [&](const RTDData &d) {
	    return !d.name.compare(name);
	}) == rtds_.end()) {
	throw MalformedFile(MalformedFile::RTD_NAME_NOT_FOUND, name);
    }

    return RTDCalculator(rtds_, name);
}

Json::Value RTDCalculatorFactory::Impl::toJson()
{
    try {
	Json::Value val(Json::arrayValue);

	for (const auto &rtd : rtds_) {
	    Json::Value r(Json::objectValue);

	    r["name"] = rtd.name;
	    r["values"] = Json::arrayValue;
	    for (const auto &value : rtd.vals) {
		Json::Value pair;
		pair["temperature"] = value.temperature;
		pair["resistance"] = value.resistance;
		r.append(pair);
	    }

	    val.append(r);
	}
	return val;

    } catch (const Json::Exception &ex) {
	throw RTDCalculatorError(ex.what());
    }
}

////////////////////////////////////////////////////////////
//		RTDCalculatorFactory
////////////////////////////////////////////////////////////

RTDCalculatorFactory::Impl &RTDCalculatorFactory::getImpl()
{
    static Impl i;
    return i;
}

void RTDCalculatorFactory::loadRTDCalculator()
{
    getImpl().loadRTDCalculator();
}

void RTDCalculatorFactory::clear() { getImpl().clear(); }
void RTDCalculatorFactory::reload() { getImpl().reload(); }
RTDCalculator RTDCalculatorFactory::getRTDCalc(const char *name)
{
    return getImpl().getRTDCalc(name);
}

Json::Value RTDCalculatorFactory::toJson() { return getImpl().toJson(); }
const char *RTDCalculatorFactory::default_path() noexcept
{
    return RTD_CONVERTER_CONFIG_DIR "/converter.config";
}

} // namespace CALF
